package com.ladestitute.bloodandgold;

import com.ladestitute.bloodandgold.client.ClientEventBusSubscriber;
import com.ladestitute.bloodandgold.registries.*;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import com.ladestitute.bloodandgold.util.events.BaGInfernalTorchHandler;
import com.ladestitute.bloodandgold.util.events.BaGDamageHandler;
import com.ladestitute.bloodandgold.util.events.materials.*;
import com.ladestitute.bloodandgold.util.events.shrines.BaGShrineOfPainHandler;
import com.ladestitute.bloodandgold.util.events.shrines.BaGShrineOfSacrificeHandler;
import com.ladestitute.bloodandgold.util.events.weapons.BaGEffectsHandler;
import com.ladestitute.bloodandgold.util.events.weapons.BaGFlailsHandler;
import com.ladestitute.bloodandgold.util.events.weapons.BaGRapiersHandler;
import com.ladestitute.bloodandgold.util.events.weapons.BaGThrownWeaponsHandler;
import com.ladestitute.bloodandgold.util.events.worldgen.BaGBiomeLoadingHandler;
import com.ladestitute.bloodandgold.util.events.worldgen.BaGOreGen;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotTypeMessage;

import static net.minecraft.item.ItemModelsProperties.register;

@SuppressWarnings("deprecation")
@Mod(BaGMain.MOD_ID)
@EventBusSubscriber(modid = BaGMain.MOD_ID, bus = Bus.MOD)
public class BaGMain
{
    public static BaGMain instance;
    public static final String NAME = "Blood & Gold";
    public static final String MOD_ID = "bloodandgold";
    public static final Logger LOGGER = LogManager.getLogger();

    public BaGMain()
    {
        instance = this;

        final IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        modEventBus.addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientEventBusSubscriber::init);

        MinecraftForge.EVENT_BUS.register(new BaGGlassWeaponsHandler());
        MinecraftForge.EVENT_BUS.register(new BaGBloodWeaponsHandler());
        MinecraftForge.EVENT_BUS.register(new BaGObsidianWeaponsHandler());
        MinecraftForge.EVENT_BUS.register(new BaGGoldWeaponsHandler());
        MinecraftForge.EVENT_BUS.register(new BaGFlailsHandler());
        MinecraftForge.EVENT_BUS.register(new BaGRapiersHandler());
        MinecraftForge.EVENT_BUS.register(new BaGShrineOfSacrificeHandler());
        MinecraftForge.EVENT_BUS.register(new BaGShrineOfPainHandler());
        MinecraftForge.EVENT_BUS.register(new BaGEffectsHandler());
        MinecraftForge.EVENT_BUS.register(new BaGKarateGiEffectHandler());
        MinecraftForge.EVENT_BUS.register(new BaGDamageHandler());
        MinecraftForge.EVENT_BUS.register(new BaGThrownWeaponsHandler());
        MinecraftForge.EVENT_BUS.register(new BaGInfernalTorchHandler());
        /* Register all of our deferred registries from our list/init classes, which get added to the IEventBus */
        // ParticleList.PARTICLES.register(modEventBus);
        SoundInit.SOUNDS.register(modEventBus);
       EffectInit.EFFECTS.register(modEventBus);
        //EffectInit.POTIONS.register(modEventBus);
       ItemInit.ITEMS.register(modEventBus);
        //FoodInit.FOOD.register(modEventBus);
       // BlockInit.BLOCKS.register(modEventBus);
       SpecialBlocksInit.BLOCKS.register(modEventBus);
        EntityTypeInit.ENTITIES.register(modEventBus);
        ContainerInit.CONTAINERS.register(modEventBus);
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, BaGConfigHandler.SPEC, "bloodandgoldconfig.toml");
      //  MinecraftForge.EVENT_BUS.addListener(EventPriority.HIGH, BaGOreGen::generateOres);
        MinecraftForge.EVENT_BUS.addListener(BaGBiomeLoadingHandler::modifyBiomes);
    }

    /*
     * This is a helper method we are using just to save a little space in clientSetup
     * It takes in the Minecraft Supplier as a parameter, which can be called from the event
     */


    /* The FMLCommonSetupEvent (FML - Forge Mod Loader) */
    private void setup(final FMLCommonSetupEvent event)
    {
        BaGOreGen.register();
    }

    // Custom ItemGroup tab
    public static final ItemGroup ARMOR_TAB = new ItemGroup("bloodgold_armor") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.OBSIDIAN_ARMOR.get());
        }
    };

    public static final ItemGroup SHOVELS_TAB = new ItemGroup("bloodgold_shovels") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.CRYSTAL_SHOVEL.get());
        }
    };

    public static final ItemGroup WEAPONS_TAB = new ItemGroup("bloodgold_weapons") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.TITANIUM_DAGGER.get());
        }
    };

    public static final ItemGroup SPELLS_TAB = new ItemGroup("bloodgold_spells") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.FIREBALL_SPELL.get());
        }
    };

    // Custom ItemGroup tab

    public static final ItemGroup MISC_TAB = new ItemGroup("bloodgold_misc") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.GLASS_SHARD.get());
        }
    };

    public static final ItemGroup SHRINES_TAB = new ItemGroup("bloodgold_shrines") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(SpecialBlocksInit.SHRINE_OF_BLOOD_UNACTIVATED.get());
        }
    };

    @SubscribeEvent
    public static void interModComms(InterModEnqueueEvent e){
        InterModComms.sendTo(CuriosApi.MODID, SlotTypeMessage.REGISTER_TYPE, () -> new SlotTypeMessage.Builder("spell")
                .size(1).icon(new ResourceLocation("curios:slot/spell")).build());
        InterModComms.sendTo(CuriosApi.MODID, SlotTypeMessage.REGISTER_TYPE, () -> new SlotTypeMessage.Builder("torch")
                .size(1).icon(new ResourceLocation("curios:slot/torch")).build());
        InterModComms.sendTo(CuriosApi.MODID, SlotTypeMessage.REGISTER_TYPE, () -> new SlotTypeMessage.Builder("ring")
                .size(1).icon(new ResourceLocation("curios:slot/ring")).build());
        InterModComms.sendTo(CuriosApi.MODID, SlotTypeMessage.REGISTER_TYPE, () -> new SlotTypeMessage.Builder("charm")
                .size(1).icon(new ResourceLocation("curios:slot/charm")).build());
    }


    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents
    {
        @SubscribeEvent
        public static void setModelProperties(FMLClientSetupEvent event) {
            register(ItemInit.GLASS_BOW.get(), new ResourceLocation("pull"), (p_239429_0_, p_239429_1_, p_239429_2_) -> {
                if (p_239429_2_ == null) {
                    return 0.0F;
                } else {
                    return p_239429_2_.getUseItem() != p_239429_0_ ? 0.0F : (float)(p_239429_0_.getUseDuration() - p_239429_2_.getUseItemRemainingTicks()) / 20.0F;
                }
            });
            register(ItemInit.GLASS_BOW.get(), new ResourceLocation("pulling"), (p_239428_0_, p_239428_1_, p_239428_2_) -> p_239428_2_ != null && p_239428_2_.isUsingItem() && p_239428_2_.getUseItem() == p_239428_0_ ? 1.0F : 0.0F);

            register(ItemInit.GLASS_CROSSBOW.get(), new ResourceLocation("pull"), (itemStack, world, livingEntity) ->
            {
                if (livingEntity == null)
                {
                    return 0.0F;
                }
                else
                {
                    return CrossbowItem.isCharged(itemStack) ? 0.0F : (float)(itemStack.getUseDuration() - livingEntity.getUseItemRemainingTicks()) / (float) CrossbowItem.getChargeDuration(itemStack);
                }
            });
            register(ItemInit.GLASS_CROSSBOW.get(), new ResourceLocation("pulling"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && livingEntity.isUsingItem() && livingEntity.getUseItem() == itemStack && !CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.GLASS_CROSSBOW.get(), new ResourceLocation("charged"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.GLASS_CROSSBOW.get(), new ResourceLocation("firework"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) && CrossbowItem.containsChargedProjectile(itemStack, Items.FIREWORK_ROCKET) ? 1.0F : 0.0F;
            });
            //
            register(ItemInit.TITANIUM_BOW.get(), new ResourceLocation("pull"), (p_239429_0_, p_239429_1_, p_239429_2_) -> {
                if (p_239429_2_ == null) {
                    return 0.0F;
                } else {
                    return p_239429_2_.getUseItem() != p_239429_0_ ? 0.0F : (float)(p_239429_0_.getUseDuration() - p_239429_2_.getUseItemRemainingTicks()) / 20.0F;
                }
            });
            register(ItemInit.TITANIUM_BOW.get(), new ResourceLocation("pulling"), (p_239428_0_, p_239428_1_, p_239428_2_) -> p_239428_2_ != null && p_239428_2_.isUsingItem() && p_239428_2_.getUseItem() == p_239428_0_ ? 1.0F : 0.0F);

            register(ItemInit.TITANIUM_CROSSBOW.get(), new ResourceLocation("pull"), (itemStack, world, livingEntity) ->
            {
                if (livingEntity == null)
                {
                    return 0.0F;
                }
                else
                {
                    return CrossbowItem.isCharged(itemStack) ? 0.0F : (float)(itemStack.getUseDuration() - livingEntity.getUseItemRemainingTicks()) / (float) CrossbowItem.getChargeDuration(itemStack);
                }
            });
            register(ItemInit.TITANIUM_CROSSBOW.get(), new ResourceLocation("pulling"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && livingEntity.isUsingItem() && livingEntity.getUseItem() == itemStack && !CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.TITANIUM_CROSSBOW.get(), new ResourceLocation("charged"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.TITANIUM_CROSSBOW.get(), new ResourceLocation("firework"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) && CrossbowItem.containsChargedProjectile(itemStack, Items.FIREWORK_ROCKET) ? 1.0F : 0.0F;
            });
            //
            register(ItemInit.BLOOD_BOW.get(), new ResourceLocation("pull"), (p_239429_0_, p_239429_1_, p_239429_2_) -> {
                if (p_239429_2_ == null) {
                    return 0.0F;
                } else {
                    return p_239429_2_.getUseItem() != p_239429_0_ ? 0.0F : (float)(p_239429_0_.getUseDuration() - p_239429_2_.getUseItemRemainingTicks()) / 20.0F;
                }
            });
            register(ItemInit.BLOOD_BOW.get(), new ResourceLocation("pulling"), (p_239428_0_, p_239428_1_, p_239428_2_) -> p_239428_2_ != null && p_239428_2_.isUsingItem() && p_239428_2_.getUseItem() == p_239428_0_ ? 1.0F : 0.0F);

            register(ItemInit.BLOOD_CROSSBOW.get(), new ResourceLocation("pull"), (itemStack, world, livingEntity) ->
            {
                if (livingEntity == null)
                {
                    return 0.0F;
                }
                else
                {
                    return CrossbowItem.isCharged(itemStack) ? 0.0F : (float)(itemStack.getUseDuration() - livingEntity.getUseItemRemainingTicks()) / (float) CrossbowItem.getChargeDuration(itemStack);
                }
            });
            register(ItemInit.BLOOD_CROSSBOW.get(), new ResourceLocation("pulling"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && livingEntity.isUsingItem() && livingEntity.getUseItem() == itemStack && !CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.BLOOD_CROSSBOW.get(), new ResourceLocation("charged"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.BLOOD_CROSSBOW.get(), new ResourceLocation("firework"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) && CrossbowItem.containsChargedProjectile(itemStack, Items.FIREWORK_ROCKET) ? 1.0F : 0.0F;
            });
            //
            register(ItemInit.OBSIDIAN_BOW.get(), new ResourceLocation("pull"), (p_239429_0_, p_239429_1_, p_239429_2_) -> {
                if (p_239429_2_ == null) {
                    return 0.0F;
                } else {
                    return p_239429_2_.getUseItem() != p_239429_0_ ? 0.0F : (float)(p_239429_0_.getUseDuration() - p_239429_2_.getUseItemRemainingTicks()) / 20.0F;
                }
            });
            register(ItemInit.OBSIDIAN_BOW.get(), new ResourceLocation("pulling"), (p_239428_0_, p_239428_1_, p_239428_2_) -> p_239428_2_ != null && p_239428_2_.isUsingItem() && p_239428_2_.getUseItem() == p_239428_0_ ? 1.0F : 0.0F);

            register(ItemInit.OBSIDIAN_CROSSBOW.get(), new ResourceLocation("pull"), (itemStack, world, livingEntity) ->
            {
                if (livingEntity == null)
                {
                    return 0.0F;
                }
                else
                {
                    return CrossbowItem.isCharged(itemStack) ? 0.0F : (float)(itemStack.getUseDuration() - livingEntity.getUseItemRemainingTicks()) / (float) CrossbowItem.getChargeDuration(itemStack);
                }
            });
            register(ItemInit.OBSIDIAN_CROSSBOW.get(), new ResourceLocation("pulling"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && livingEntity.isUsingItem() && livingEntity.getUseItem() == itemStack && !CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.OBSIDIAN_CROSSBOW.get(), new ResourceLocation("charged"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.OBSIDIAN_CROSSBOW.get(), new ResourceLocation("firework"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) && CrossbowItem.containsChargedProjectile(itemStack, Items.FIREWORK_ROCKET) ? 1.0F : 0.0F;
            });
            register(ItemInit.GOLD_BOW.get(), new ResourceLocation("pull"), (p_239429_0_, p_239429_1_, p_239429_2_) -> {
                if (p_239429_2_ == null) {
                    return 0.0F;
                } else {
                    return p_239429_2_.getUseItem() != p_239429_0_ ? 0.0F : (float)(p_239429_0_.getUseDuration() - p_239429_2_.getUseItemRemainingTicks()) / 20.0F;
                }
            });
            register(ItemInit.GOLD_BOW.get(), new ResourceLocation("pulling"), (p_239428_0_, p_239428_1_, p_239428_2_) -> p_239428_2_ != null && p_239428_2_.isUsingItem() && p_239428_2_.getUseItem() == p_239428_0_ ? 1.0F : 0.0F);

            register(ItemInit.GOLD_CROSSBOW.get(), new ResourceLocation("pull"), (itemStack, world, livingEntity) ->
            {
                if (livingEntity == null)
                {
                    return 0.0F;
                }
                else
                {
                    return CrossbowItem.isCharged(itemStack) ? 0.0F : (float)(itemStack.getUseDuration() - livingEntity.getUseItemRemainingTicks()) / (float) CrossbowItem.getChargeDuration(itemStack);
                }
            });
            register(ItemInit.GOLD_CROSSBOW.get(), new ResourceLocation("pulling"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && livingEntity.isUsingItem() && livingEntity.getUseItem() == itemStack && !CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.GOLD_CROSSBOW.get(), new ResourceLocation("charged"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) ? 1.0F : 0.0F;
            });
            register(ItemInit.GOLD_CROSSBOW.get(), new ResourceLocation("firework"), (itemStack, world, livingEntity) ->
            {
                return livingEntity != null && CrossbowItem.isCharged(itemStack) && CrossbowItem.containsChargedProjectile(itemStack, Items.FIREWORK_ROCKET) ? 1.0F : 0.0F;
            });
        }
    }

}


