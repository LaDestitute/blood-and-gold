package com.ladestitute.bloodandgold;

import com.ladestitute.bloodandgold.util.bagcaps.blood.BaGBloodCapabilityFactory;
import com.ladestitute.bloodandgold.util.bagcaps.blood.BaGBloodCapabilityHandler;
import com.ladestitute.bloodandgold.util.bagcaps.blood.BaGBloodCapabilityStorage;
import com.ladestitute.bloodandgold.util.bagcaps.blood.IBaGBloodCapability;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityFactory;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityHandler;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityStorage;
import com.ladestitute.bloodandgold.util.bagcaps.effects.IBaGEffectsCapability;
import com.ladestitute.bloodandgold.util.bagcaps.rhythm.BaGRhythmCapabilityFactory;
import com.ladestitute.bloodandgold.util.bagcaps.rhythm.BaGRhythmCapabilityHandler;
import com.ladestitute.bloodandgold.util.bagcaps.rhythm.BaGRhythmCapabilityStorage;
import com.ladestitute.bloodandgold.util.bagcaps.rhythm.IBaGRhythmCapability;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BaGCommonEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticCommonSetup(FMLCommonSetupEvent event) {

        CapabilityManager.INSTANCE.register(IBaGBloodCapability.class, new BaGBloodCapabilityStorage(), BaGBloodCapabilityFactory::new);
        MinecraftForge.EVENT_BUS.register(new BaGBloodCapabilityHandler());
        CapabilityManager.INSTANCE.register(IBaGRhythmCapability.class, new BaGRhythmCapabilityStorage(), BaGRhythmCapabilityFactory::new);
        MinecraftForge.EVENT_BUS.register(new BaGRhythmCapabilityHandler());
        CapabilityManager.INSTANCE.register(IBaGEffectsCapability.class, new BaGEffectsCapabilityStorage(), BaGEffectsCapabilityFactory::new);
        MinecraftForge.EVENT_BUS.register(new BaGEffectsCapabilityHandler());
    }

}
