package com.ladestitute.bloodandgold.container.slot;

import com.ladestitute.bloodandgold.items.BagItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

//Code credited to Daripher
public class BagSlot extends Slot
{
    private final BagItem bagItem;

    public BagSlot(IInventory inventory, int index, int x, int y, BagItem bagItem)
    {
        super(inventory, index, x, y);
        this.bagItem = bagItem;
    }

    @Override
    public boolean mayPlace(ItemStack stack)
    {
        return bagItem.canHoldItem(stack);
    }
}
