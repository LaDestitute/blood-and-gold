package com.ladestitute.bloodandgold.util.network;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.util.network.server.ExtendedAttackPacket;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;

import java.util.function.Function;

public class BaGPacketHandler
{
    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(
            new ResourceLocation(BaGMain.MOD_ID, "main"),
            () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals
    );

    private static int index;

    public static synchronized void register() {
        // CLIENT

        // SERVER
        register(ExtendedAttackPacket.class, ExtendedAttackPacket::decode);
    }

    private static <MSG extends IBaGPacket.BaGPacket> void register(final Class<MSG> packet, Function<PacketBuffer, MSG> decoder) {
        INSTANCE.messageBuilder(packet, index++).encoder(IBaGPacket.BaGPacket::encode).decoder(decoder).consumer(IBaGPacket.BaGPacket::handle).add();
    }

    public static <MSG> void sendToPlayer(MSG message, ServerPlayerEntity player) {
        INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), message);
    }

    public static <MSG> void sendToNear(MSG message, double x, double y, double z, double radius, RegistryKey<World> dimension) {
        INSTANCE.send(PacketDistributor.NEAR.with(PacketDistributor.TargetPoint.p(x, y, z, radius, dimension)), message);
    }

    public static <MSG> void sendToAll(MSG message) {
        INSTANCE.send(PacketDistributor.ALL.noArg(), message);
    }

    public static <MSG> void sendToServer(MSG message)
    {
        INSTANCE.sendToServer(message);
    }
}
