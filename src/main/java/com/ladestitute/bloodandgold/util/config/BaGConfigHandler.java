package com.ladestitute.bloodandgold.util.config;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import com.ladestitute.bloodandgold.BaGMain;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Path;
import java.nio.file.Paths;

public class BaGConfigHandler {
    private static final BaGConfigHandler INSTANCE;

    public static final ForgeConfigSpec SPEC;

    static {
        Pair<BaGConfigHandler, ForgeConfigSpec> specPair =
                new ForgeConfigSpec.Builder().configure(BaGConfigHandler::new);
        INSTANCE = specPair.getLeft();
        SPEC = specPair.getRight();
    }

    //General
    private final ForgeConfigSpec.BooleanValue directdmgbreaksglass;
    private final ForgeConfigSpec.BooleanValue rangedmgbreaksglass;
    private final ForgeConfigSpec.BooleanValue environmentaldmgbreaksglass;
    private final ForgeConfigSpec.BooleanValue rapiersperformlunges;
    private final ForgeConfigSpec.IntValue bloodkillsrequirement;
    private final ForgeConfigSpec.IntValue daggeroffrostspecialeffectmultiplier;
    private final ForgeConfigSpec.IntValue goldspecialeffectmultiplier;
    private final ForgeConfigSpec.IntValue bloodspecialeffectmultiplier;
    private final ForgeConfigSpec.BooleanValue rangedmgresetsrhythmlevel;
    private final ForgeConfigSpec.BooleanValue environmentaldmgresetsrhythmlevel;
    private final ForgeConfigSpec.IntValue mintimenotmovingtoresetrhythmlevel;
    private final ForgeConfigSpec.BooleanValue crossbowsdopiercingdamage;
    private final ForgeConfigSpec.BooleanValue thrownweaponsdopiercingdamage;
    private final ForgeConfigSpec.BooleanValue daggeroffrostmelts;
    private final ForgeConfigSpec.BooleanValue shrineofpeaceremovesarmor;
    private final ForgeConfigSpec.BooleanValue shrineofpeacegiveslessabsorption;

    private BaGConfigHandler(ForgeConfigSpec.Builder configSpecBuilder) {
        //General
        directdmgbreaksglass = configSpecBuilder
                .define("directdmgbreaksglass", true);
        rangedmgbreaksglass = configSpecBuilder
                .define("rangedmgbreaksglass", false);
        environmentaldmgbreaksglass = configSpecBuilder
                .define("environmentaldmgbreaksglass", false);
        rapiersperformlunges = configSpecBuilder
                .define("rapiersperformlunges", true);
        bloodkillsrequirement = configSpecBuilder
                .defineInRange("bloodkillsrequirement", 10, 5, 15);
        daggeroffrostspecialeffectmultiplier = configSpecBuilder
                .defineInRange("goldspecialeffectmultiplier", 1, 1, 3);
        bloodspecialeffectmultiplier = configSpecBuilder
                .defineInRange("bloodspecialeffectmultiplier", 1, 1, 3);
        goldspecialeffectmultiplier = configSpecBuilder
                .defineInRange("goldspecialeffectmultiplier", 1, 1, 3);
        rangedmgresetsrhythmlevel = configSpecBuilder
                .define("rangedmgresetsrhythmlevel", false);
        environmentaldmgresetsrhythmlevel = configSpecBuilder
                .define("environmentaldmgresetsrhythmlevel", false);
        mintimenotmovingtoresetrhythmlevel = configSpecBuilder
                .defineInRange("mintimenotmovingtoresetrhythmlevel", 600, 300, 1200);
        crossbowsdopiercingdamage = configSpecBuilder
                .define("crossbowsdopiercingdamage", true);
        thrownweaponsdopiercingdamage = configSpecBuilder
                .define("thrownweaponsdopiercingdamage", true);
        daggeroffrostmelts = configSpecBuilder
                .define("daggeroffrostmelts", false);
        shrineofpeaceremovesarmor = configSpecBuilder
                .define("shrineofpeaceremovesarmor", true);
        shrineofpeacegiveslessabsorption = configSpecBuilder
                .define("shrineofpeacegiveslessabsorption", true);


    }

    public static BaGConfigHandler getInstance() {
        return INSTANCE;
    }
    // Query Operations

    //General
    public boolean directdmgbreaksglass() { return directdmgbreaksglass.get(); }
    public boolean rangedmgbreaksglass() { return rangedmgbreaksglass.get(); }
    public boolean environmentaldmgbreaksglass() { return environmentaldmgbreaksglass.get(); }
    public boolean rapiersperformlunges() { return rapiersperformlunges.get(); }
    public int bloodkillsrequirement() { return bloodkillsrequirement.get(); }
    public int daggeroffrostspecialeffectmultiplier() { return daggeroffrostspecialeffectmultiplier.get(); }
    public int goldspecialeffectmultiplier() { return goldspecialeffectmultiplier.get(); }
    public int bloodspecialeffectmultiplier() { return bloodspecialeffectmultiplier.get(); }
    public boolean rangedmgresetsrhythmlevel() { return rangedmgresetsrhythmlevel.get(); }
    public boolean environmentaldmgresetsrhythmlevel() { return environmentaldmgresetsrhythmlevel.get(); }
    public int mintimenotmovingtoresetrhythmlevel() { return mintimenotmovingtoresetrhythmlevel.get(); }
    public boolean crossbowsdopiercingdamage() { return crossbowsdopiercingdamage.get(); }
    public boolean thrownweaponsdopiercingdamage() { return thrownweaponsdopiercingdamage.get(); }
    public boolean daggeroffrostmelts() { return daggeroffrostmelts.get(); }
    public boolean shrineofpeaceremovesarmor() { return shrineofpeaceremovesarmor.get(); }
    public boolean shrineofpeacegiveslessabsorption() { return shrineofpeacegiveslessabsorption.get(); }

    ////////
    // Modification Operations
    ////////
    //General
    public void changedirectdmgbreaksglass(boolean newValue) {
        directdmgbreaksglass.set(newValue);
    }
    public void changerangedmgbreaksglass(boolean newValue) {
        rangedmgbreaksglass.set(newValue);
    }
    public void changeenvironmentaldmgbreaksglass(boolean newValue) {
        environmentaldmgbreaksglass.set(newValue);
    }
    public void changerapiersperformlunges(boolean newValue) {
        rapiersperformlunges.set(newValue);
    }
    public void changebloodkillsrequirement(int newValue) {bloodkillsrequirement.set(newValue);}
    public void changedaggeroffrostspecialeffectmultiplier(int newValue) {daggeroffrostspecialeffectmultiplier.set(newValue);}
    public void changegoldspecialeffectmultiplier(int newValue) {goldspecialeffectmultiplier.set(newValue);}
    public void changebloodspecialeffectmultiplier(int newValue) {bloodspecialeffectmultiplier.set(newValue);}
    public void changerangedmgresetsrhythmlevel(boolean newValue) {
        rangedmgresetsrhythmlevel.set(newValue);
    }
    public void changeenvironmentaldmgresetsrhythmlevel(boolean newValue) {
        environmentaldmgresetsrhythmlevel.set(newValue);
    }
    public void changemintimenotmovingtoresetrhythmlevel(int newValue) {mintimenotmovingtoresetrhythmlevel.set(newValue);}
    public void changecrossbowsdopiercingdamage(boolean newValue) {
        crossbowsdopiercingdamage.set(newValue);
    }
    public void changethrownweaponsdopiercingdamage(boolean newValue) {
        thrownweaponsdopiercingdamage.set(newValue);
    }
    public void changedaggeroffrostmelts(boolean newValue) {
        daggeroffrostmelts.set(newValue);
    }
    public void changeshrineofpeaceremovesarmor(boolean newValue) {
        shrineofpeaceremovesarmor.set(newValue);
    }
    public void changeshrineofpeacegiveslessabsorption(boolean newValue) {
        shrineofpeacegiveslessabsorption.set(newValue);
    }
    public void save() {
        SPEC.save();
    }
}
