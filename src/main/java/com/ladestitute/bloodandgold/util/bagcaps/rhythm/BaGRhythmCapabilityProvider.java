package com.ladestitute.bloodandgold.util.bagcaps.rhythm;

import com.ladestitute.bloodandgold.util.bagcaps.blood.IBaGBloodCapability;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

//This is actually a capability provider but we use ICapSerializable because we want to have the data saved/loaded
//ICapSerializable is required for saving/loading the data, you can instead use ICapabilityProvider if you do not want the data to persist
public class BaGRhythmCapabilityProvider implements ICapabilitySerializable<INBT> {

    //The capability is null, we inject it like other modded-objects before attaching it to entities/items/etc when we initialize
    @CapabilityInject(IBaGRhythmCapability.class)
    public static Capability<IBaGRhythmCapability> BAGRHYTHM = null;
    //The difference between normal/lazy values, normal ones are executed when defined
    //Lazy values are executed the first time that they are accessed
    private LazyOptional<IBaGRhythmCapability> instance = LazyOptional.of(BAGRHYTHM::getDefaultInstance);

    //See an explanation of why this is needed in the PlayerDataCapabilityHandler class
    public void invalidate() {
        instance.invalidate();
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return cap == BAGRHYTHM ? instance.cast() : LazyOptional.empty();
    }

    //Serialize and deserialize are from ICapSerializable, which is delegated to our Storage class
    @Override
    public INBT serializeNBT() {

        return BAGRHYTHM.getStorage().writeNBT(BAGRHYTHM, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null);
    }

    @Override
    public void deserializeNBT(INBT nbt) {
        BAGRHYTHM.getStorage().readNBT(BAGRHYTHM, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null, nbt);
    }
}

