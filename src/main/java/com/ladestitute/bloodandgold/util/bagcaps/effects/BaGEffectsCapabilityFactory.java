package com.ladestitute.bloodandgold.util.bagcaps.effects;

public class BaGEffectsCapabilityFactory implements IBaGEffectsCapability {
    private double preventityxposition;
    private double preventityyposition;
    private double preventityzposition;
    private int isentityfrozen;
    private int ticksfrozen;
    private int frozenblock;
    private int ticksarrhythmic;
    private int regenringtime;
    private int setshieldingringdestroyed;
    private int shieldtime;
    private int courageactive;
    private int couragetime;
    private int fireballcharge;
    private int healcharge;
    private int shieldcharge;
    private int freezecharge;
    private int bombcharge;
    private int transmutecharge;

    public BaGEffectsCapabilityFactory() {
    }

    @Override
    public void preventityxposition(double amount) {
        this.preventityxposition = amount;
    }

    @Override
    public void preventityyposition(double amount) {
        this.preventityyposition = amount;
    }

    @Override
    public void preventityzposition(double amount) {
        this.preventityzposition = amount;
    }

    @Override
    public void isEntityFrozen(int amount) {
        this.isentityfrozen = amount;
    }

    @Override
    public void addticksFrozen(int amount) {
      this.ticksfrozen += amount;
    }

    @Override
    public void resetticksFrozen(int amount) {
        this.ticksfrozen = amount;
    }

    @Override
    public void addticksarrhythmic(int amount) {
        this.ticksarrhythmic += amount;
    }

    @Override
    public void resetticksarrhythmic(int amount) {
        this.ticksarrhythmic = amount;
    }

    @Override
    public void setfrozenblock(int amount) {
        this.frozenblock += amount;
    }

    @Override
    public void addregenringtime(int regenringtime) {
        this.regenringtime += regenringtime;
    }

    @Override
    public void setregenrintrime(int regenringtime) {
        this.regenringtime = regenringtime;
    }

    @Override
    public void setshieldtime(int shieldtime) {
        this.shieldtime = shieldtime;
    }

    @Override
    public void addshieldtime(int shieldtime) {
        this.shieldtime += shieldtime;
    }

    @Override
    public void setshieldingringdestroyed(int setshieldingringdestroyed) {
        this.setshieldingringdestroyed = setshieldingringdestroyed;
    }

    @Override
    public void setcouragetime(int couragetime) {
        this.couragetime = couragetime;
    }

    @Override
    public void addcouragetime(int couragetime) {
        this.couragetime += couragetime;
    }

    @Override
    public void courageactive(int courageactive) {
            this.courageactive = courageactive;
    }

    @Override
    public void decreasetransmutecooldown(int transmutecharge) {
        if (this.transmutecharge > 0) {
            this.transmutecharge -= transmutecharge;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.transmutecharge = 0;

    }

    @Override
    public void settransmutecharge(int transmutecharge) {
        this.transmutecharge = transmutecharge;
    }

    @Override
    public void decreasefireballcooldown(int fireballcharge) {
        if (this.fireballcharge > 0) {
            this.fireballcharge -= fireballcharge;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.fireballcharge = 0;

    }

    @Override
    public void setfireballcharge(int fireballcharge) {
         this.fireballcharge = fireballcharge;
    }

    @Override
    public int checkfireballcharge() {
        return fireballcharge;
    }

    @Override
    public int checktransmutecharge() {
        return transmutecharge;
    }

    @Override
    public void decreasehealcooldown(int healcharge) {
        if (this.healcharge > 0) {
            this.healcharge -= healcharge;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.healcharge = 0;

    }

    @Override
    public void sethealcharge(int healcharge) {
        this.healcharge = healcharge;
    }

    @Override
    public int checkhealcharge() {
        return healcharge;
    }

    @Override
    public void decreaseshieldcooldown(int shieldcharge) {
        if (this.shieldcharge > 0) {
            this.shieldcharge -= shieldcharge;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.shieldcharge = 0;

    }

    @Override
    public void setshieldcharge(int shieldcharge) {
        this.shieldcharge = shieldcharge;
    }

    @Override
    public int checkshieldcharge() {
        return shieldcharge;
    }

    @Override
    public void decreasefreezecooldown(int freezecharge) {
        if (this.freezecharge > 0) {
            this.freezecharge -= freezecharge;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.freezecharge = 0;

    }

    @Override
    public void setfreezecharge(int freezecharge) {
        this.freezecharge = freezecharge;
    }

    @Override
    public int checkfreezecharge() {
        return freezecharge;
    }

    @Override
    public void decreasebombcooldown(int bombcharge) {
        if (this.bombcharge > 0) {
            this.bombcharge -= bombcharge;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.bombcharge = 0;

    }

    @Override
    public void setbombcharge(int bombcharge) {
        this.bombcharge = bombcharge;
    }

    @Override
    public int checkbombcharge() {
        return bombcharge;
    }

    @Override
    public int isEntityFrozen() {
        return isentityfrozen;
    }

    @Override
    public int ticksFrozen() {
        return ticksfrozen;
    }

    @Override
    public int checkfrozenblock() {
        return frozenblock;
    }

    @Override
    public int checkshieldtime() {
        return shieldtime;
    }

    @Override
    public int checkcouragetime() {
        return couragetime;
    }

    @Override
    public int iscourageactive() {
        return courageactive;
    }

    @Override
    public int checksetshieldingringdestroyed() {
        return setshieldingringdestroyed;
    }

    @Override
    public int checkregenringtime() {
        return regenringtime;
    }

    @Override
    public int getticksarrhythmic() {
        return ticksarrhythmic;
    }

    @Override
    public double getpreventityxposition() {
        return this.preventityxposition;
    }

    @Override
    public double getpreventityyposition() {
        return this.preventityyposition;
    }

    @Override
    public double getpreventityzposition() {
        return this.preventityzposition;
    }
}

