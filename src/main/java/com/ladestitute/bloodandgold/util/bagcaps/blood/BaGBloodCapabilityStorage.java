package com.ladestitute.bloodandgold.util.bagcaps.blood;

import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class BaGBloodCapabilityStorage implements Capability.IStorage<IBaGBloodCapability> {

    @Nullable
    @Override
    public INBT writeNBT(Capability<IBaGBloodCapability> capability, IBaGBloodCapability instance, Direction side) {
        CompoundNBT tag = new CompoundNBT();
        //It doesn't matter what you name the tags but its best to be consistent
       tag.putInt("currentblood", (int) instance.getBlood());
       tag.putInt("maxblood", (int) instance.getMaxBlood());
       tag.putInt("kills", instance.getKills());
       tag.putInt("requiredkillsforhealing", BaGConfigHandler.getInstance().bloodkillsrequirement());
        return tag;
    }

    @Override
    public void readNBT(Capability<IBaGBloodCapability> capability, IBaGBloodCapability instance, Direction side, INBT nbt) {
        CompoundNBT tag = (CompoundNBT) nbt;
        instance.setBlood(tag.getInt("currentblood"));
        instance.setMaxBlood(tag.getInt("maxblood"));
        instance.setKills(tag.getInt("kills"));
        instance.setRequiredKills(tag.getInt("requiredkillsforhealing"));
    }
}
