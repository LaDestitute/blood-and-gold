package com.ladestitute.bloodandgold.util.bagcaps.effects;

import com.ladestitute.bloodandgold.util.bagcaps.blood.IBaGBloodCapability;
import com.ladestitute.bloodandgold.util.bagcaps.rhythm.IBaGRhythmCapability;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

//This is actually a capability provider but we use ICapSerializable because we want to have the data saved/loaded
//ICapSerializable is required for saving/loading the data, you can instead use ICapabilityProvider if you do not want the data to persist
public class BaGEffectsCapabilityProvider implements ICapabilitySerializable<INBT> {

    //The capability is null, we inject it like other modded-objects before attaching it to entities/items/etc when we initialize
    @CapabilityInject(IBaGEffectsCapability.class)
    public static Capability<IBaGEffectsCapability> BAGEFFECTS = null;
    //The difference between normal/lazy values, normal ones are executed when defined
    //Lazy values are executed the first time that they are accessed
    private LazyOptional<IBaGEffectsCapability> instance = LazyOptional.of(BAGEFFECTS::getDefaultInstance);

    //See an explanation of why this is needed in the PlayerDataCapabilityHandler class
    public void invalidate() {
        instance.invalidate();
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return cap == BAGEFFECTS ? instance.cast() : LazyOptional.empty();
    }

    //Serialize and deserialize are from ICapSerializable, which is delegated to our Storage class
    @Override
    public INBT serializeNBT() {

        return BAGEFFECTS.getStorage().writeNBT(BAGEFFECTS, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null);
    }

    @Override
    public void deserializeNBT(INBT nbt) {
        BAGEFFECTS.getStorage().readNBT(BAGEFFECTS, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null, nbt);
    }
}

