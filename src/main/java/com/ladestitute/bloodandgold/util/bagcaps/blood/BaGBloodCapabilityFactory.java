package com.ladestitute.bloodandgold.util.bagcaps.blood;

public class BaGBloodCapabilityFactory implements IBaGBloodCapability {

    private float maxblood = 20.0F;
    private float blood;
    private int kills;
    private int requiredkills = 0;



    public BaGBloodCapabilityFactory() {
    }


    @Override
    public float getBlood() {
        return blood;
    }

    //Set the value to a specified amount
    @Override
    public void setBlood(float amount) {
        blood = Math.max(0, Math.min(maxblood, amount));
    }

    //Add to the value
    @Override
    public void addBlood(float amount) {
        blood = Math.min(maxblood, blood + amount);
    }

    //Subtract from the value
    @Override
    public void subtractBlood(float amount) {
        blood = Math.min(maxblood, blood - amount);
    }

    @Override
    public float getMaxBlood() {
        return maxblood;
    }

    //Set the value to a specified amount
    @Override
    public void setMaxBlood(float amount) {
        this.maxblood = amount;
    }

    //Add to the value
    @Override
    public void addMaxBlood(float amount) {this.maxblood += amount;}

    //Subtract from the value
    @Override
    public void subtractMaxBlood(float amount) {this.maxblood -= amount;}

    @Override
    public void setKills(int amount) {
        this.kills = amount;
    }

    @Override
    public void setRequiredKills(int amount) {
        this.requiredkills = amount;
    }

    //Add to the value
    @Override
    public void addKills(int amount) {this.kills += amount;}

    @Override
    public int getKills() {
        return kills;
    }
}
