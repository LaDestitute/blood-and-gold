package com.ladestitute.bloodandgold.util.bagcaps.rhythm;

import com.ladestitute.bloodandgold.util.bagcaps.blood.IBaGBloodCapability;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class BaGRhythmCapabilityStorage implements Capability.IStorage<IBaGRhythmCapability> {

    @Nullable
    @Override
    public INBT writeNBT(Capability<IBaGRhythmCapability> capability, IBaGRhythmCapability instance, Direction side) {
        CompoundNBT tag = new CompoundNBT();
        //It doesn't matter what you name the tags but its best to be consistent
       tag.putInt("rhythmlevel", (int) instance.getRhythmLevel());
        tag.putInt("rhythmkills", (int) instance.getRhythmKills());
        return tag;
    }

    @Override
    public void readNBT(Capability<IBaGRhythmCapability> capability, IBaGRhythmCapability instance, Direction side, INBT nbt) {
        CompoundNBT tag = (CompoundNBT) nbt;
       instance.setRhythmLevel(tag.getInt("rhythmlevel"));
        instance.setRhythmKills(tag.getInt("rhythmkills"));
    }
}
