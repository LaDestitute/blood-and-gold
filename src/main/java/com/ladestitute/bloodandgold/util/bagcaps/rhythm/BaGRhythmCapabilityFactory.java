package com.ladestitute.bloodandgold.util.bagcaps.rhythm;

public class BaGRhythmCapabilityFactory implements IBaGRhythmCapability {
    private int rhythmlevel;
    private int rhythmkills;

    public BaGRhythmCapabilityFactory() {
    }

    @Override
    public int getRhythmLevel() {
        return this.rhythmlevel;
    }

    @Override
    public void setRhythmLevel(int amount) {
        this.rhythmlevel = amount;
    }

    //Add to the value
    @Override
    public void increaseRhythmLevel(int amount) {this.rhythmlevel += amount;}

    @Override
    public void decreaseRhythmLevel(int amount) {
        this.rhythmlevel -= amount;
    }

    //Add to the value
    @Override
    public void addRhythmKills(int amount) {this.rhythmkills += amount;}

    @Override
    public void setRhythmKills(int amount) {
        this.rhythmkills = amount;
    }

    @Override
    public int getRhythmKills() {
        return rhythmkills;
    }
}

