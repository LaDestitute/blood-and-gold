package com.ladestitute.bloodandgold.util.bagcaps.rhythm;

public interface IBaGRhythmCapability {
    //Pretty simple, just an interface with an integer and some methods
    int getRhythmLevel();
    int getRhythmKills();

    void setRhythmLevel(int rhythmlevel);

    void increaseRhythmLevel(int rhythmlevel);

    void decreaseRhythmLevel(int rhythmlevel);

    void setRhythmKills(int rhythmkills);

    void addRhythmKills(int rhythmkills);
}


