package com.ladestitute.bloodandgold.util.bagcaps.effects;

public interface IBaGEffectsCapability {
    //Future todo: confusion, charged and ooze effects
    // Confusion: direction the player moves in when pressing a movement key is reversed
    // Charged: Causes the player's attacks on a single enemy to damage nearby enemies in an arc-lightning effect
    // Ooze: inflicts the player with Mining Fatigue IX, Weakness IX and causes them to take double damage from enemies
    // (Ooze prevents the player from attacking or digging in NecroDancer along with the x2 taking damage penalty
    // so strong amounts of mining fatigue and weakness is applied to replicate this)
    // Also used to store rings effects for shield/regen rings/track spell charges

    void preventityxposition (double preventityxposition);
    void preventityyposition (double preventityyposition);
    void preventityzposition (double preventityzposition);
    void isEntityFrozen(int isentityfrozen);
    void addticksFrozen(int ticksfrozen);
    void resetticksFrozen(int ticksfrozen);
    void addticksarrhythmic(int ticksarrhythmic);
    void resetticksarrhythmic(int ticksarrhythmic);
    void setfrozenblock(int frozenblock);
    void addregenringtime(int regenringtime);
    void setregenrintrime (int regenringtime);
    void setshieldtime (int shieldtime);
    void addshieldtime (int shieldtime);
    void setshieldingringdestroyed (int setshieldingringdestroyed);
    void setcouragetime (int couragetime);
    void addcouragetime (int couragetime);
    void courageactive (int courageactive);
    void decreasefireballcooldown (int fireballcharge);
    void setfireballcharge (int fireballcharge);
    void decreasehealcooldown (int healcharge);
    void sethealcharge (int healcharge);
    void decreaseshieldcooldown (int shieldcharge);
    void setshieldcharge (int shieldcharge);
    void decreasefreezecooldown (int freezecharge);
    void setfreezecharge (int freezecharge);
    void decreasebombcooldown (int bombcharge);
    void setbombcharge (int bombcharge);
    void decreasetransmutecooldown (int transmutecharge);
    void settransmutecharge (int transmutecharge);

    int isEntityFrozen();

    int ticksFrozen();

    int checkfrozenblock();

    int checkshieldtime();

    int checkcouragetime();

    int checkfireballcharge();

    int checkhealcharge();

    int checkshieldcharge();

    int checkfreezecharge();

    int checktransmutecharge();

    int checkbombcharge();

    int iscourageactive();

    int checksetshieldingringdestroyed();

    int checkregenringtime();

    int getticksarrhythmic();

    double getpreventityxposition();
    double getpreventityyposition();
    double getpreventityzposition();
}