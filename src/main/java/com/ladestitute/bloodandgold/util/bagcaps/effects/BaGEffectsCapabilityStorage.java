package com.ladestitute.bloodandgold.util.bagcaps.effects;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class BaGEffectsCapabilityStorage implements Capability.IStorage<IBaGEffectsCapability> {

    @Nullable
    @Override
    public INBT writeNBT(Capability<IBaGEffectsCapability> capability, IBaGEffectsCapability instance, Direction side) {
        CompoundNBT tag = new CompoundNBT();
        //It doesn't matter what you name the tags but its best to be consistent
        tag.putInt("isentityfrozen", instance.isEntityFrozen());
        tag.putInt("fireballcharge", instance.checkfireballcharge());
        tag.putInt("healcharge", instance.checkhealcharge());
        tag.putInt("freezecharge", instance.checkfreezecharge());
        tag.putInt("bombcharge", instance.checkbombcharge());
       // tag.putInt("ticksfrozen", instance.resetticksFrozen());
        return tag;
    }

    @Override
    public void readNBT(Capability<IBaGEffectsCapability> capability, IBaGEffectsCapability instance, Direction side, INBT nbt) {
        CompoundNBT tag = (CompoundNBT) nbt;
        instance.isEntityFrozen(tag.getInt("isentityfrozen"));
        instance.setfireballcharge(tag.getInt("fireballcharge"));
        instance.sethealcharge(tag.getInt("healcharge"));
        instance.setfreezecharge(tag.getInt("freezecharge"));
        instance.setbombcharge(tag.getInt("bombcharge"));
       // instance.resetticksFrozen(tag.getInt("ticksfrozen"));
    }
}
