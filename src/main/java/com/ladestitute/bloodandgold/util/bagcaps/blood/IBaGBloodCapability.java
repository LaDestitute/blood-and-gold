package com.ladestitute.bloodandgold.util.bagcaps.blood;

public interface IBaGBloodCapability {
    //Pretty simple, just an interface with an integer and some methods
    float getBlood();
    float getMaxBlood();
    int getKills();
    void setBlood(float blood);

    void addBlood(float blood);

    void subtractBlood(float blood);

    void setMaxBlood(float maxblood);

    void addMaxBlood(float maxblood);

    void subtractMaxBlood(float maxblood);

    void setKills(int kills);

    void addKills(int kills);

    void setRequiredKills(int requiredkills);

}

