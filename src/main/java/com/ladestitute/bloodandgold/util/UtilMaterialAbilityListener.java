package com.ladestitute.bloodandgold.util;

import com.ladestitute.bloodandgold.util.network.BaGPacketHandler;
import com.ladestitute.bloodandgold.util.network.server.ExtendedAttackPacket;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class UtilMaterialAbilityListener
{
    private static boolean handleExtendedReach(PlayerEntity player) {
        double reach = player.getAttribute(ForgeMod.REACH_DISTANCE.get()).getValue();
        Vector3d eyePos = player.getEyePosition(1.0F);
        Vector3d lookVec = player.getLookAngle();
        Vector3d reachVec = eyePos.add(lookVec.x * reach, lookVec.y * reach, lookVec.z * reach);
        AxisAlignedBB playerBox = player.getBoundingBox().expandTowards(lookVec.scale(reach)).inflate(1.0D, 1.0D, 1.0D);
        EntityRayTraceResult traceResult = ProjectileHelper.getEntityHitResult(player, eyePos, reachVec, playerBox, (target) -> !target.isSpectator() && target.isPickable(), reach * reach);
        if (traceResult != null) {
            Entity target = traceResult.getEntity();
            Vector3d hitVec = traceResult.getLocation();
            double distance = eyePos.distanceToSqr(hitVec);
            if (distance < reach * reach) {
                BaGPacketHandler.sendToServer(new ExtendedAttackPacket(player.getUUID(), target.getId()));
                return true;
            }
        }
        return false;
    }
}
