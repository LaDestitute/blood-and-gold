package com.ladestitute.bloodandgold.util.tags;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ItemInit;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.ItemTagsProvider;
import net.minecraft.item.Items;
import net.minecraftforge.common.data.ExistingFileHelper;

public class BaGTagsList {
    public static class ItemTagsDataGen extends ItemTagsProvider {

        public ItemTagsDataGen(DataGenerator generatorIn, BlockTagsProvider blockTagsProvider,
                               ExistingFileHelper existingFileHelper) {
            super(generatorIn, blockTagsProvider, BaGMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
            //Adding your own blocks/items from init must have the type-subtype builder before it
            // To refresh tags after edits below, run preparerundata then run rundata
            //copy the tags folder to your mod's data folder, ie resources/data/modid/tags

            tag(BaGTags.Items.BLOODSHRINE_ITEMPOOL).add(ItemInit.BLOOD_SWORD.get());
            tag(BaGTags.Items.BLOODSHRINE_ITEMPOOL).add(ItemInit.BLOOD_BOW.get());
            tag(BaGTags.Items.BLOODSHRINE_ITEMPOOL).add(ItemInit.BLOOD_SPEAR.get());
            tag(BaGTags.Items.BLOODSHRINE_ITEMPOOL).add(ItemInit.BLOOD_DAGGER.get());
            tag(BaGTags.Items.BLOODSHRINE_ITEMPOOL).add(ItemInit.BLOOD_FLAIL.get());
            tag(BaGTags.Items.BLOODSHRINE_ITEMPOOL).add(ItemInit.BLOOD_CROSSBOW.get());
            tag(BaGTags.Items.BLOODSHRINE_ITEMPOOL).add(ItemInit.BLOOD_RAPIER.get());

            tag(BaGTags.Items.SPECIALSHRINE_GLASSITEMPOOL).add(ItemInit.GLASS_SWORD.get());
            tag(BaGTags.Items.SPECIALSHRINE_GLASSITEMPOOL).add(ItemInit.GLASS_BOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_GLASSITEMPOOL).add(ItemInit.GLASS_SPEAR.get());
            tag(BaGTags.Items.SPECIALSHRINE_GLASSITEMPOOL).add(ItemInit.GLASS_DAGGER.get());
            tag(BaGTags.Items.SPECIALSHRINE_GLASSITEMPOOL).add(ItemInit.GLASS_FLAIL.get());
            tag(BaGTags.Items.SPECIALSHRINE_GLASSITEMPOOL).add(ItemInit.GLASS_CROSSBOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_GLASSITEMPOOL).add(ItemInit.GLASS_RAPIER.get());

            tag(BaGTags.Items.SPACE_SHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_SHOVEL.get());
            tag(BaGTags.Items.SPACE_SHRINE_ITEMPOOL).add(Items.IRON_PICKAXE);
            tag(BaGTags.Items.SPACE_SHRINE_ITEMPOOL).add(ItemInit.GLASS_SHOVEL.get());

            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.BLOOD_SWORD.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.BLOOD_BOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.BLOOD_SPEAR.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.BLOOD_DAGGER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.BLOOD_FLAIL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.BLOOD_CROSSBOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.BLOOD_RAPIER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TITANIUM_SWORD.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TITANIUM_BOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TITANIUM_SPEAR.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TITANIUM_DAGGER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TITANIUM_FLAIL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TITANIUM_CROSSBOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TITANIUM_RAPIER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_SWORD.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_BOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_SPEAR.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_DAGGER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_FLAIL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_CROSSBOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_RAPIER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.DAGGER_OF_FROST.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.JEWELED_DAGGER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.BLOOD_SHOVEL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GLASS_SHOVEL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_SHOVEL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.CRYSTAL_SHOVEL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TITANIUM_SHOVEL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GLASS_ARMOR.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.OBSIDIAN_ARMOR.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.KARATE_GI.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.GOLDEN_SWORD);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.BOW);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.CROSSBOW);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.LEATHER_CHESTPLATE);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.CHAINMAIL_CHESTPLATE);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.IRON_CHESTPLATE);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.MAP);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.COMPASS);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.APPLE);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.CARROT);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.COOKIE);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(Items.IRON_PICKAXE);
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.MAUSOLEUM_MASH_DISC.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.PORTABELLOHEAD_DISC.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.DANCE_OF_THE_DECOROUS_DISC.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.A_HOT_MESS_DISC.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.STYX_AND_STONES_DISC.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.POWER_CORDS_DISC.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GOLD_BOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GOLD_SPEAR.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GOLD_DAGGER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GOLD_FLAIL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GOLD_CROSSBOW.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GOLD_RAPIER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.GOLD_RAPIER.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RING_OF_MANA.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RING_OF_PEACE.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RING_OF_REGENERATION.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RING_OF_SHIELDING.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.PROTECTION_CHARM.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RISK_CHARM.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.FROST_CHARM.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.STRENGTH_CHARM.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RING_OF_PROTECTION.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RING_OF_MIGHT.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RING_OF_GOLD.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.RING_OF_WAR.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.FREEZE_SPELL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.FIREBALL_SPELL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.SHIELD_SPELL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.HEAL_SPELL.get());
            tag(BaGTags.Items.SPECIALSHRINE_ITEMPOOL).add(ItemInit.TRANSMUTE_SPELL.get());
            //Todo: add transmute spell to list

            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.BLOOD_SWORD.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.BLOOD_BOW.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.BLOOD_SPEAR.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.BLOOD_DAGGER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.BLOOD_FLAIL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.BLOOD_CROSSBOW.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.BLOOD_RAPIER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.TITANIUM_SWORD.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.TITANIUM_BOW.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.TITANIUM_SPEAR.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.TITANIUM_DAGGER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.TITANIUM_FLAIL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.TITANIUM_CROSSBOW.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.TITANIUM_RAPIER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_SWORD.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_BOW.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_SPEAR.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_DAGGER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_FLAIL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_CROSSBOW.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_RAPIER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.DAGGER_OF_FROST.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.JEWELED_DAGGER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.BLOOD_SHOVEL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GLASS_SHOVEL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_SHOVEL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.CRYSTAL_SHOVEL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.TITANIUM_SHOVEL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GLASS_ARMOR.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.OBSIDIAN_ARMOR.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.KARATE_GI.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.GOLDEN_SWORD);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.BOW);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.CROSSBOW);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.LEATHER_CHESTPLATE);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.CHAINMAIL_CHESTPLATE);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.APPLE);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.CARROT);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.COOKIE);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(Items.IRON_PICKAXE);
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.MAUSOLEUM_MASH_DISC.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.PORTABELLOHEAD_DISC.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.DANCE_OF_THE_DECOROUS_DISC.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.A_HOT_MESS_DISC.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.STYX_AND_STONES_DISC.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.POWER_CORDS_DISC.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GOLD_BOW.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GOLD_SPEAR.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GOLD_DAGGER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GOLD_FLAIL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GOLD_CROSSBOW.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GOLD_RAPIER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.GOLD_RAPIER.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RING_OF_MANA.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RING_OF_PEACE.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RING_OF_REGENERATION.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RING_OF_SHIELDING.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.PROTECTION_CHARM.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RISK_CHARM.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.FROST_CHARM.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.STRENGTH_CHARM.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RING_OF_PROTECTION.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RING_OF_MIGHT.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RING_OF_GOLD.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.RING_OF_WAR.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.FREEZE_SPELL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.FIREBALL_SPELL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.SHIELD_SPELL.get());
            tag(BaGTags.Items.TRANSMUTE_ITEMPOOL).add(ItemInit.HEAL_SPELL.get());

            //getOrCreateBuilder(ZTags.Items.MATERIAL).addTag(ZTags.Items.MATERIAL_WOOL);
            // getOrCreateBuilder(ZTags.Items.MATERIAL_WOOL).add(Blocks.BLACK_WOOL.asItem());
        }
    }

    public static class BlockTagsDataGen extends BlockTagsProvider {

        public BlockTagsDataGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
            super(generatorIn, BaGMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
            //    getOrCreateBuilder(SpelunkTags.Blocks.ORES_RUBY).add(BlockInit.ORERUBY.get());
        }
    }
}
