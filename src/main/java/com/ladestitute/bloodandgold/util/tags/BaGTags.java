package com.ladestitute.bloodandgold.util.tags;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;

public class BaGTags
{
    public static void init ()
    {
        Blocks.init();
        Items.init();
    }

    public static class Items
    {
        private static void init(){}

        //Implement Gears

        public static final Tags.IOptionalNamedTag<Item> SPACE_SHRINE_ITEMPOOL = tag("spaceshrine_itempool");
        public static final Tags.IOptionalNamedTag<Item> BLOODSHRINE_ITEMPOOL = tag("bloodshrine_itempool");
        public static final Tags.IOptionalNamedTag<Item> SPECIALSHRINE_GLASSITEMPOOL = tag("specialshrine_glassitempool");
        public static final Tags.IOptionalNamedTag<Item> SPECIALSHRINE_ITEMPOOL = tag("specialshrine_itempool");
        public static final Tags.IOptionalNamedTag<Item> TRANSMUTE_ITEMPOOL = tag("transmute_itempool");
        public static final Tags.IOptionalNamedTag<Item> DISALLOWED_SHRINEDARKNESS_ITEMS = tag("disallowed_shrinedarkness_items");
        public static final Tags.IOptionalNamedTag<Item> DISALLOWED_SHRINEPEACE_ITEMS = tag("disallowed_shrinepeace_items");

        private static Tags.IOptionalNamedTag<Item> tag(String name)
        {
            return ItemTags.createOptional(new ResourceLocation("forge", name));
        }
    }
    public static class Blocks
    {
        private static void init(){}

        public static final Tags.IOptionalNamedTag<Block> ORES = tag("ores");

        private static Tags.IOptionalNamedTag<Block> tag(String name)
        {
            return BlockTags.createOptional(new ResourceLocation("forge", name));
        }
    }
}
