package com.ladestitute.bloodandgold.util.tags;

import com.ladestitute.bloodandgold.BaGMain;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BaGDataGen
{
    @SubscribeEvent
    public static void gatherData(GatherDataEvent event)
    {
        DataGenerator generator = event.getGenerator();

        if (event.includeServer())
        {
            BaGMain.LOGGER.debug("Starting Server Data Generators");
            BaGTagsList.BlockTagsDataGen blockTagsProvider = new BaGTagsList.BlockTagsDataGen(event.getGenerator(), event.getExistingFileHelper());
            // generator.addProvider(new RecipeDataGen(generator));
            // generator.addProvider(new LootTableDataGen(generator));
            generator.addProvider(new BaGTagsList.BlockTagsDataGen(generator, event.getExistingFileHelper()));
            generator.addProvider(new BaGTagsList.ItemTagsDataGen(generator, blockTagsProvider, event.getExistingFileHelper()));
        }
        if (event.includeClient())
        {
            BaGMain.LOGGER.debug("Starting Client Data Generators");
            // generator.addProvider(new ItemModelDataGen(generator, CLib.MOD_ID, event.getExistingFileHelper()));
            // generator.addProvider(new BlockModelDataGen(generator, CLib.MOD_ID, event.getExistingFileHelper()));
        }
    }
}

