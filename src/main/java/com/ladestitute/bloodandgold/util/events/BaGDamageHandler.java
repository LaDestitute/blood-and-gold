package com.ladestitute.bloodandgold.util.events;

import com.ladestitute.bloodandgold.entities.thrown.*;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

public class BaGDamageHandler {

    @SubscribeEvent
    public void livingDamageEvent(LivingDamageEvent event) {
        //Check if is PlayerEntity doing the damage.
        if (event.getSource().getDirectEntity() instanceof PlayerEntity) {

            //Get PlayerEntity.
            PlayerEntity player = (PlayerEntity) event.getSource().getDirectEntity();

            //Get the Ring as an ItemStack

            ItemStack armor = player.getItemBySlot(EquipmentSlotType.CHEST);

            //Check if PlayerEntity is wearing it. Check if Sword Item.
            if (armor.getItem() == ItemInit.KARATE_GI.get()) {
                event.setAmount(event.getAmount() * 2);
            }
        }
    }

    @SubscribeEvent
    public void onLivingHurt(LivingHurtEvent event) {
        //Check if it is the PlayerEntity who takes damage.
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity PlayerEntity = (PlayerEntity) event.getEntityLiving();

            //Get the Ring as an ItemStack
            ItemStack armor = PlayerEntity.getItemBySlot(EquipmentSlotType.CHEST);
            ItemStack protection_charm =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.PROTECTION_CHARM.get(), PlayerEntity).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            ItemStack ring_of_protection =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_PROTECTION.get(), PlayerEntity).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if(armor.getItem() == ItemInit.KARATE_GI.get())
            {
                event.setAmount(event.getAmount()*2);
            }
            //Check if PlayerEntity is wearing it.
            if (!protection_charm.isEmpty()||!ring_of_protection.isEmpty()) {
                //Don't do a Check to see if the damage comes from DamageSource.GENERIC. I don't know what mob/block uses the "GENERIC" damage in the game so I normally do a (event.getSource != DamageSource.*Type*) if I don't want it to take less damage from something in particular.
                if(armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.DROWN||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.ON_FIRE||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.IN_FIRE||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.ANVIL||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.CACTUS||
                         armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.HOT_FLOOR||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.LAVA||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.SWEET_BERRY_BUSH)
                {
                    event.setAmount(event.getAmount()*2 - 1f);
                }
                else event.setAmount(event.getAmount() - 1f);
            }
            if (!protection_charm.isEmpty() && !ring_of_protection.isEmpty()) {
                //Don't do a Check to see if the damage comes from DamageSource.GENERIC. I don't know what mob/block uses the "GENERIC" damage in the game so I normally do a (event.getSource != DamageSource.*Type*) if I don't want it to take less damage from something in particular.
                if(armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.DROWN||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.ON_FIRE||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.IN_FIRE||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.ANVIL||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.CACTUS||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.HOT_FLOOR||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.LAVA||
                        armor.getItem() == ItemInit.KARATE_GI.get() && event.getSource() != DamageSource.SWEET_BERRY_BUSH)
                {
                    event.setAmount(event.getAmount()*2 - 2f);
                }
                else event.setAmount(event.getAmount() - 2f);
            }
        }
    }

    @SubscribeEvent
    public void piercingdamageeffect(LivingHurtEvent event) {
        Entity sourceEntity = event.getSource().getEntity();
        Entity thrownEntity = event.getSource().getDirectEntity();

        if(thrownEntity instanceof EntityThrownGoldDagger||thrownEntity instanceof EntityThrownGoldSpear||
                thrownEntity instanceof EntityThrownBloodDagger ||thrownEntity instanceof EntityThrownBloodSpear||
                thrownEntity instanceof EntityThrownTitaniumDagger ||thrownEntity instanceof EntityThrownTitaniumSpear||
                thrownEntity instanceof EntityThrownGlassDagger||thrownEntity instanceof EntityThrownGlassSpear||
                thrownEntity instanceof EntityThrownObsidianDagger||thrownEntity instanceof EntityThrownObsidianSpear||
                thrownEntity instanceof EntityThrownFrostDagger||thrownEntity instanceof EntityThrownJeweledDagger||
                thrownEntity instanceof EntityThrownDagger|| thrownEntity instanceof EntityThrownGlassShard)
        {
            if(BaGConfigHandler.getInstance().thrownweaponsdopiercingdamage()) {
                event.getSource().bypassArmor();
            }
        }
        if (sourceEntity instanceof PlayerEntity) {
            for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                if (held.getItem() == ItemInit.DAGGER_OF_FROST.get()) {
                    event.getSource().bypassArmor();
                }
                if (held.getItem() == ItemInit.BLOOD_CROSSBOW.get()||
                        held.getItem() == ItemInit.GLASS_CROSSBOW.get()||
                        held.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get()||
                        held.getItem() == ItemInit.TITANIUM_CROSSBOW.get()||
                        held.getItem() == ItemInit.GOLD_CROSSBOW.get()||
                        held.getItem() == Items.CROSSBOW) {
                    if(BaGConfigHandler.getInstance().crossbowsdopiercingdamage()) {
                        event.getSource().bypassArmor();
                    }
                }
                //  PlayerEntityEntity PlayerEntity = (PlayerEntityEntity)event.getEntityLiving();
            }
        }

    }
}
