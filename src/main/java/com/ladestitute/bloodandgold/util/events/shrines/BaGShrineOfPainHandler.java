package com.ladestitute.bloodandgold.util.events.shrines;

import com.ladestitute.bloodandgold.blocks.shrines.ShrineOfPainUnactivatedBlock;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

public class BaGShrineOfPainHandler {
    public static final DirectionProperty HORIZONTAL_FACING = BlockStateProperties.HORIZONTAL_FACING;
    public int offeringmade;
    public int totalofferings;
    public int glassreward;
    public int glassrewardgiven;
    public static final ITag.INamedTag<Item> specialshrine_itempool =
            ItemTags.bind("bloodandgold:specialshrine_itempool");
    public static final ITag.INamedTag<Item> specialshrine_glassitempool =
            ItemTags.bind("bloodandgold:specialshrine_glassitempool");
    Random rand1 = new Random();
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    @SubscribeEvent
    public void damagelistener(LivingDamageEvent event) {
        if (event.getEntityLiving() instanceof PlayerEntity)
        {
            if (event.getSource() == DamageSource.DROWN ||
                    event.getSource() == DamageSource.LAVA || event.getSource() == DamageSource.IN_FIRE ||
                    event.getSource() == DamageSource.SWEET_BERRY_BUSH || event.getSource() == DamageSource.LIGHTNING_BOLT||
                    event.getSource() == DamageSource.HOT_FLOOR|| event.getSource() == DamageSource.FALLING_BLOCK||
                    event.getSource() == DamageSource.FALL || event.getSource() == DamageSource.CACTUS)
            {
                BlockPos blockPos = event.getEntityLiving().blockPosition();
                int searchRadius = (int) 1.5;
                int radius = (int) (Math.floor(2.0) + searchRadius);

                for (int x = -radius; x <= radius; ++x) {
                    for (int y = -1; y <= 1; ++y) {
                        for (int z = -radius; z <= radius; ++z) {
                            BlockPos shrinePos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                            BlockState blockState = event.getEntityLiving().level.getBlockState(shrinePos);
                            Block block = blockState.getBlock();
                            //  BlockPos sacriPos = blockState.getBlock().defaultBlockState().;
                            // BlockPos PAINPos = new BlockPos(block.defaultBlockState().getBlock().);
                            if (block instanceof ShrineOfPainUnactivatedBlock) {
                                if (event.getAmount() < 12F) {
                                    totalofferings = totalofferings + 1;
                                    offeringmade = 1;
                                }
                                if (event.getAmount() >= 12F) {
                                    glassreward = 1;
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void PAINshrinenormalsearcher(TickEvent.PlayerTickEvent event)
    {
        LivingEntity player = event.player;
        BlockPos posWest = player.blockPosition().west();
        BlockState blockStateWest = player.level.getBlockState(posWest);
        Block w = blockStateWest.getBlock(); //West
        BlockPos posEast = player.blockPosition().east();
        BlockState blockStateEast = player.level.getBlockState(posEast);
        Block e = blockStateEast.getBlock(); //East
        BlockPos posNorth = player.blockPosition().north();
        BlockState blockStateNorth = player.level.getBlockState(posNorth);
        Block n = blockStateNorth.getBlock(); //North
        BlockPos posSouth = player.blockPosition().south();
        BlockState blockStateSouth = player.level.getBlockState(posSouth);
        Block s = blockStateSouth.getBlock(); //South
        //Diagonal directions one block from player
        BlockPos posSW = player.blockPosition().south().west();
        BlockState blockStateSW = player.level.getBlockState(posSW);
        Block sw = blockStateSW.getBlock(); //SW
        BlockPos posSE = player.blockPosition().south().east();
        BlockState blockStateSE = player.level.getBlockState(posSE);
        Block se = blockStateSE.getBlock(); //SE
        BlockPos posNW = player.blockPosition().north().west();
        BlockState blockStateNW = player.level.getBlockState(posNW);
        Block nw = blockStateNW.getBlock(); //NW
        BlockPos posNE = player.blockPosition().north().east();
        BlockState blockStateNE = player.level.getBlockState(posNE);
        Block ne = blockStateNE.getBlock(); //NE
        //Cardinal directions two blocks from player
        BlockPos posWestWest = player.blockPosition().west().west();
        BlockState blockStateWestWest = player.level.getBlockState(posWestWest);
        Block ww = blockStateWestWest.getBlock(); //West x2
        BlockPos posEastEast = player.blockPosition().east().east();
        BlockState blockStateEastEast = player.level.getBlockState(posEastEast);
        Block ee = blockStateEastEast.getBlock(); //East x2
        BlockPos posNorthNorth = player.blockPosition().north().north();
        BlockState blockStateNorthNorth = player.level.getBlockState(posNorthNorth);
        Block nn = blockStateNorthNorth.getBlock(); //North x2
        BlockPos posSouthSouth = player.blockPosition().south().south();
        BlockState blockStateSouthSouth = player.level.getBlockState(posSouthSouth);
        Block ss = blockStateSouthSouth.getBlock(); //South x2
        //Diagonal directions two blocks from the player
        BlockPos posNorthNorthWest = player.blockPosition().north().north().west();
        BlockState blockStateNorthNorthWest = player.level.getBlockState(posNorthNorthWest);
        Block nnw = blockStateNorthNorthWest.getBlock(); //nw x2
        BlockPos posNorthNorthEast = player.blockPosition().north().north().east();
        BlockState blockStateNorthNorthEast = player.level.getBlockState(posNorthNorthEast);
        Block nne = blockStateNorthNorthEast.getBlock(); //ne x2
        BlockPos posSouthSouthWest = player.blockPosition().south().south().west();
        BlockState blockStateSouthSouthWest = player.level.getBlockState(posSouthSouthWest);
        Block ssw = blockStateSouthSouthWest.getBlock(); //sw x2
        BlockPos posSouthSouthEast = player.blockPosition().south().south().east();
        BlockState blockStateSouthSouthEast = player.level.getBlockState(posSouthSouthEast);
        Block sse = blockStateSouthSouthEast.getBlock(); //ne x2
        //Corners, outmost
        BlockPos posNorthNorthWestCorner = player.blockPosition().north().north().west().west();
        BlockState blockStateNorthNorthWestCorner = player.level.getBlockState(posNorthNorthWestCorner);
        Block c1 = blockStateNorthNorthWestCorner.getBlock(); //nw x2
        BlockPos posNorthNorthEastCorner = player.blockPosition().north().north().east().east();
        BlockState blockStateNorthNorthEastCorner = player.level.getBlockState(posNorthNorthEastCorner);
        Block c2 = blockStateNorthNorthEastCorner.getBlock(); //ne x2
        BlockPos posSouthSouthWestCorner = player.blockPosition().south().south().west().west();
        BlockState blockStateSouthSouthWestCorner = player.level.getBlockState(posSouthSouthWestCorner);
        Block c3 = blockStateSouthSouthWestCorner.getBlock(); //sw x2
        BlockPos posSouthSouthEastCorner = player.blockPosition().south().south().east().east();
        BlockState blockStateSouthSouthEastCorner = player.level.getBlockState(posSouthSouthEastCorner);
        Block c4 = blockStateSouthSouthEastCorner.getBlock(); //ne x2
        if(totalofferings == 5 && glassreward == 0)
        {
            if (w == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = w.defaultBlockState().getBlockState();
                event.player.level.setBlock(posWest, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (e == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = e.defaultBlockState().getBlockState();
                event.player.level.setBlock(posEast, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (n == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = n.defaultBlockState().getBlockState();
                event.player.level.setBlock(posNorth, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (s == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = s.defaultBlockState().getBlockState();
                event.player.level.setBlock(posSouth, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            //Diagonal directions one block from player
            if (sw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = sw.defaultBlockState().getBlockState();
                event.player.level.setBlock(posSW, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (se == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = se.defaultBlockState().getBlockState();
                event.player.level.setBlock(posSE, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (nw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = nw.defaultBlockState().getBlockState();
                event.player.level.setBlock(posNW, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (ne == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ne.defaultBlockState().getBlockState();
                event.player.level.setBlock(posNE, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            //Cardinal directions two blocks from player
            if (ww == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ww.defaultBlockState().getBlockState();
                event.player.level.setBlock(posWestWest, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (ee == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ee.defaultBlockState().getBlockState();
                event.player.level.setBlock(posEastEast, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (nn == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = nn.defaultBlockState().getBlockState();
                event.player.level.setBlock(posNorthNorth, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (ss == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ss.defaultBlockState().getBlockState();
                event.player.level.setBlock(posSouthSouth, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            //Diagonal directions two blocks from the player
            if (nnw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = nnw.defaultBlockState().getBlockState();
                event.player.level.setBlock(posNorthNorthWest, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (nne == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = nne.defaultBlockState().getBlockState();
                event.player.level.setBlock(posNorthNorthEast, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (ssw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ssw.defaultBlockState().getBlockState();
                event.player.level.setBlock(posSouthSouthWest, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (sse == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = sse.defaultBlockState().getBlockState();
                event.player.level.setBlock(posSouthSouthEast, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            //Corners, outmost
            if (c1 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = c1.defaultBlockState().getBlockState();
                event.player.level.setBlock(posNorthNorthWestCorner, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (c2 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = c2.defaultBlockState().getBlockState();
                event.player.level.setBlock(posNorthNorthEastCorner, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (c3 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = c3.defaultBlockState().getBlockState();
                event.player.level.setBlock(posSouthSouthWestCorner, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
            if (c4 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = c4.defaultBlockState().getBlockState();
                event.player.level.setBlock(posSouthSouthEastCorner, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
                totalofferings = 0;
            }
        }
        if(offeringmade == 1 && glassreward == 0) {
            if (w == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posWest.getX(), posWest.getY(), posWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (e == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posEast.getX(), posEast.getY(), posEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (n == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorth.getX(), posNorth.getY(), posNorth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (s == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouth.getX(), posSouth.getY(), posSouth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            //Diagonal directions one block from player
            if (sw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSW.getX(), posSW.getY(), posSW.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (se == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSE.getX(), posSE.getY(), posSE.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (nw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNW.getX(), posNW.getY(), posNW.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (ne == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNE.getX(), posNE.getY(), posNE.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            //Cardinal directions two blocks from player
            if (ww == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posWestWest.getX(), posWestWest.getY(), posWestWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (ee == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posEastEast.getX(), posEastEast.getY(), posEastEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (nn == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorth.getX(), posNorthNorth.getY(), posNorthNorth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (ss == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouth.getX(), posSouthSouth.getY(), posSouthSouth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            //Diagonal directions two blocks from the player
            if (nnw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthWest.getX(), posNorthNorthWest.getY(), posNorthNorthWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (nne == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthEast.getX(), posNorthNorthEast.getY(), posNorthNorthEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (ssw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthWest.getX(), posSouthSouthWest.getY(), posSouthSouthWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (sse == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthEast.getX(), posSouthSouthEast.getY(), posSouthSouthEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            //Corners, outmost
            if (c1 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthWestCorner.getX(), posNorthNorthWestCorner.getY(), posNorthNorthWestCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (c2 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthEastCorner.getX(), posNorthNorthEastCorner.getY(), posNorthNorthEastCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (c3 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthWestCorner.getX(), posSouthSouthWestCorner.getY(), posSouthSouthWestCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
            if (c4 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthEastCorner.getX(), posSouthSouthEastCorner.getY(), posSouthSouthEastCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
            }
        }
    }

    @SubscribeEvent
    public void PAINshrineglasssearcher(TickEvent.PlayerTickEvent event)
    {
        LivingEntity player = event.player;
        BlockPos posWest = player.blockPosition().west();
        BlockState blockStateWest = player.level.getBlockState(posWest);
        Block w = blockStateWest.getBlock(); //West
        BlockPos posEast = player.blockPosition().east();
        BlockState blockStateEast = player.level.getBlockState(posEast);
        Block e = blockStateEast.getBlock(); //East
        BlockPos posNorth = player.blockPosition().north();
        BlockState blockStateNorth = player.level.getBlockState(posNorth);
        Block n = blockStateNorth.getBlock(); //North
        BlockPos posSouth = player.blockPosition().south();
        BlockState blockStateSouth = player.level.getBlockState(posSouth);
        Block s = blockStateSouth.getBlock(); //South
        //Diagonal directions one block from player
        BlockPos posSW = player.blockPosition().south().west();
        BlockState blockStateSW = player.level.getBlockState(posSW);
        Block sw = blockStateSW.getBlock(); //SW
        BlockPos posSE = player.blockPosition().south().east();
        BlockState blockStateSE = player.level.getBlockState(posSE);
        Block se = blockStateSE.getBlock(); //SE
        BlockPos posNW = player.blockPosition().north().west();
        BlockState blockStateNW = player.level.getBlockState(posNW);
        Block nw = blockStateNW.getBlock(); //NW
        BlockPos posNE = player.blockPosition().north().east();
        BlockState blockStateNE = player.level.getBlockState(posNE);
        Block ne = blockStateNE.getBlock(); //NE
        //Cardinal directions two blocks from player
        BlockPos posWestWest = player.blockPosition().west().west();
        BlockState blockStateWestWest = player.level.getBlockState(posWestWest);
        Block ww = blockStateWestWest.getBlock(); //West x2
        BlockPos posEastEast = player.blockPosition().east().east();
        BlockState blockStateEastEast = player.level.getBlockState(posEastEast);
        Block ee = blockStateEastEast.getBlock(); //East x2
        BlockPos posNorthNorth = player.blockPosition().north().north();
        BlockState blockStateNorthNorth = player.level.getBlockState(posNorthNorth);
        Block nn = blockStateNorthNorth.getBlock(); //North x2
        BlockPos posSouthSouth = player.blockPosition().south().south();
        BlockState blockStateSouthSouth = player.level.getBlockState(posSouthSouth);
        Block ss = blockStateSouthSouth.getBlock(); //South x2
        //Diagonal directions two blocks from the player
        BlockPos posNorthNorthWest = player.blockPosition().north().north().west();
        BlockState blockStateNorthNorthWest = player.level.getBlockState(posNorthNorthWest);
        Block nnw = blockStateNorthNorthWest.getBlock(); //nw x2
        BlockPos posNorthNorthEast = player.blockPosition().north().north().east();
        BlockState blockStateNorthNorthEast = player.level.getBlockState(posNorthNorthEast);
        Block nne = blockStateNorthNorthEast.getBlock(); //ne x2
        BlockPos posSouthSouthWest = player.blockPosition().south().south().west();
        BlockState blockStateSouthSouthWest = player.level.getBlockState(posSouthSouthWest);
        Block ssw = blockStateSouthSouthWest.getBlock(); //sw x2
        BlockPos posSouthSouthEast = player.blockPosition().south().south().east();
        BlockState blockStateSouthSouthEast = player.level.getBlockState(posSouthSouthEast);
        Block sse = blockStateSouthSouthEast.getBlock(); //ne x2
        //Corners, outmost
        BlockPos posNorthNorthWestCorner = player.blockPosition().north().north().west().west();
        BlockState blockStateNorthNorthWestCorner = player.level.getBlockState(posNorthNorthWestCorner);
        Block c1 = blockStateNorthNorthWestCorner.getBlock(); //nw x2
        BlockPos posNorthNorthEastCorner = player.blockPosition().north().north().east().east();
        BlockState blockStateNorthNorthEastCorner = player.level.getBlockState(posNorthNorthEastCorner);
        Block c2 = blockStateNorthNorthEastCorner.getBlock(); //ne x2
        BlockPos posSouthSouthWestCorner = player.blockPosition().south().south().west().west();
        BlockState blockStateSouthSouthWestCorner = player.level.getBlockState(posSouthSouthWestCorner);
        Block c3 = blockStateSouthSouthWestCorner.getBlock(); //sw x2
        BlockPos posSouthSouthEastCorner = player.blockPosition().south().south().east().east();
        BlockState blockStateSouthSouthEastCorner = player.level.getBlockState(posSouthSouthEastCorner);
        Block c4 = blockStateSouthSouthEastCorner.getBlock(); //ne x2
        if(glassreward == 1) {
            if (w == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posWest.getX(), posWest.getY(), posWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (e == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posEast.getX(), posEast.getY(), posEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (n == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorth.getX(), posNorth.getY(), posNorth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (s == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouth.getX(), posSouth.getY(), posSouth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            //Diagonal directions one block from player
            if (sw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSW.getX(), posSW.getY(), posSW.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (se == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSE.getX(), posSE.getY(), posSE.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (nw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNW.getX(), posNW.getY(), posNW.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (ne == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNE.getX(), posNE.getY(), posNE.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            //Cardinal directions two blocks from player
            if (ww == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posWestWest.getX(), posWestWest.getY(), posWestWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (ee == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posEastEast.getX(), posEastEast.getY(), posEastEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (nn == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorth.getX(), posNorthNorth.getY(), posNorthNorth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (ss == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouth.getX(), posSouthSouth.getY(), posSouthSouth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            //Diagonal directions two blocks from the player
            if (nnw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthWest.getX(), posNorthNorthWest.getY(), posNorthNorthWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (nne == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthEast.getX(), posNorthNorthEast.getY(), posNorthNorthEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (ssw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthWest.getX(), posSouthSouthWest.getY(), posSouthSouthWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (sse == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthEast.getX(), posSouthSouthEast.getY(), posSouthSouthEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            //Corners, outmost
            if (c1 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthWestCorner.getX(), posNorthNorthWestCorner.getY(), posNorthNorthWestCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (c2 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthEastCorner.getX(), posNorthNorthEastCorner.getY(), posNorthNorthEastCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (c3 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthWestCorner.getX(), posSouthSouthWestCorner.getY(), posSouthSouthWestCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
            if (c4 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthEastCorner.getX(), posSouthSouthEastCorner.getY(), posSouthSouthEastCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                offeringmade = 0;
                glassrewardgiven = 1;
            }
        }
        if(glassrewardgiven >= 1)
        {
            if (w == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = w.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posWest, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (e == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = e.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posEast, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (n == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = n.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorth, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (s == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = s.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouth, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Diagonal directions one block from player
            if (sw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = sw.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posSW, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (se == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = se.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posSE, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (nw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = nw.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posNW, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (ne == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ne.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posNE, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Cardinal directions two blocks from player
            if (ww == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ww.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posWestWest, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (ee == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ee.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posEastEast, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (nn == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = nn.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorth, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (ss == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ss.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouth, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Diagonal directions two blocks from the player
            if (nnw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = nnw.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorthWest, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (nne == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = nne.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorthEast, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (ssw == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = ssw.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouthWest, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (sse == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = sse.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouthEast, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Corners, outmost
            if (c1 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = c1.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorthWestCorner, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (c2 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = c2.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorthEastCorner, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (c3 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = c3.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouthWestCorner, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            if (c4 == SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()) {
                BlockState state = c4.defaultBlockState().getBlockState();
                glassrewardgiven = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouthEastCorner, SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
        }
    }


}