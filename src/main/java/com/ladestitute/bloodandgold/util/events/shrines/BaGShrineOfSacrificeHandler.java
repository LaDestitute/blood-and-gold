package com.ladestitute.bloodandgold.util.events.shrines;

import com.ladestitute.bloodandgold.blocks.shrines.ShrineOfSacrificeUnactivatedBlock;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import com.ladestitute.bloodandgold.util.bagcaps.blood.BaGBloodCapabilityProvider;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.block.*;
import net.minecraft.dispenser.IPosition;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.CaveSpiderEntity;
import net.minecraft.entity.monster.EndermanEntity;
import net.minecraft.entity.monster.SlimeEntity;
import net.minecraft.entity.monster.WitchEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.Property;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.Random;

public class BaGShrineOfSacrificeHandler {
    public static final DirectionProperty HORIZONTAL_FACING = BlockStateProperties.HORIZONTAL_FACING;
    public int sacrificecount;
    public int glassreward;
    public static final ITag.INamedTag<Item> specialshrine_itempool =
            ItemTags.bind("bloodandgold:specialshrine_itempool");
    public static final ITag.INamedTag<Item> specialshrine_glassitempool =
            ItemTags.bind("bloodandgold:specialshrine_glassitempool");
    Random rand1 = new Random();
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    @SubscribeEvent
    public void deathlistener(LivingDeathEvent event) {
        BlockPos blockPos = event.getEntityLiving().blockPosition();
        int searchRadius = (int) 1.5;
        int radius = (int) (Math.floor(2.0) + searchRadius);

        for (int x = -radius; x <= radius; ++x) {
            for (int y = -1; y <= 1; ++y) {
                for (int z = -radius; z <= radius; ++z) {
                    BlockPos shrinePos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                    BlockState blockState = event.getEntityLiving().level.getBlockState(shrinePos);
                    Block block = blockState.getBlock();
                  //  BlockPos sacriPos = blockState.getBlock().defaultBlockState().;
                   // BlockPos sacrificePos = new BlockPos(block.defaultBlockState().getBlock().);
                    if (block instanceof ShrineOfSacrificeUnactivatedBlock) {
                        if(event.getEntityLiving() instanceof WitchEntity||event.getEntityLiving() instanceof SlimeEntity||
                                event.getEntityLiving() instanceof EndermanEntity||event.getEntityLiving() instanceof CaveSpiderEntity)
                        {
                            glassreward = 1;
                            sacrificecount = sacrificecount+3;
                        }
                        sacrificecount = sacrificecount+1;
                        System.out.println("Mob was sacrificed to the shrine! Sacrifice count is: " + sacrificecount);
                    }
                }

            }
        }
    }

    @SubscribeEvent
    public void sacrificeshrinenormalsearcher(TickEvent.PlayerTickEvent event)
    {
        if(sacrificecount >= 3 && glassreward == 0) {
            LivingEntity player = event.player;
            BlockPos posWest = player.blockPosition().west();
            BlockState blockStateWest = player.level.getBlockState(posWest);
            Block w = blockStateWest.getBlock(); //West
            if (w == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posWest.getX(), posWest.getY(), posWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = w.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posWest, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posEast = player.blockPosition().east();
            BlockState blockStateEast = player.level.getBlockState(posEast);
            Block e = blockStateEast.getBlock(); //East
            if (e == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posEast.getX(), posEast.getY(), posEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = e.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posEast, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNorth = player.blockPosition().north();
            BlockState blockStateNorth = player.level.getBlockState(posNorth);
            Block n = blockStateNorth.getBlock(); //North
            if (n == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorth.getX(), posNorth.getY(), posNorth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = n.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posNorth, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouth = player.blockPosition().south();
            BlockState blockStateSouth = player.level.getBlockState(posSouth);
            Block s = blockStateSouth.getBlock(); //South
            if (s == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouth.getX(), posSouth.getY(), posSouth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = s.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posSouth, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Diagonal directions one block from player
            BlockPos posSW = player.blockPosition().south().west();
            BlockState blockStateSW = player.level.getBlockState(posSW);
            Block sw = blockStateSW.getBlock(); //SW
            if (sw == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSW.getX(), posSW.getY(), posSW.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = sw.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posSW, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSE = player.blockPosition().south().east();
            BlockState blockStateSE = player.level.getBlockState(posSE);
            Block se = blockStateSE.getBlock(); //SE
            if (se == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSE.getX(), posSE.getY(), posSE.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = se.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posSE, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNW = player.blockPosition().north().west();
            BlockState blockStateNW = player.level.getBlockState(posNW);
            Block nw = blockStateNW.getBlock(); //NW
            if (nw == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNW.getX(), posNW.getY(), posNW.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = nw.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posNW, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNE = player.blockPosition().north().east();
            BlockState blockStateNE = player.level.getBlockState(posNE);
            Block ne = blockStateNE.getBlock(); //NE
            if (ne == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNE.getX(), posNE.getY(), posNE.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ne.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posNE, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Cardinal directions two blocks from player
            BlockPos posWestWest = player.blockPosition().west().west();
            BlockState blockStateWestWest = player.level.getBlockState(posWestWest);
            Block ww = blockStateWestWest.getBlock(); //West x2
            if (ww == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posWestWest.getX(), posWestWest.getY(), posWestWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ww.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posWestWest, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posEastEast = player.blockPosition().east().east();
            BlockState blockStateEastEast = player.level.getBlockState(posEastEast);
            Block ee = blockStateEastEast.getBlock(); //East x2
            if (ee == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posEastEast.getX(), posEastEast.getY(), posEastEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ee.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posEastEast, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNorthNorth = player.blockPosition().north().north();
            BlockState blockStateNorthNorth = player.level.getBlockState(posNorthNorth);
            Block nn = blockStateNorthNorth.getBlock(); //North x2
            if (nn == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorth.getX(), posNorthNorth.getY(), posNorthNorth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = nn.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posNorthNorth, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouth = player.blockPosition().south().south();
            BlockState blockStateSouthSouth = player.level.getBlockState(posSouthSouth);
            Block ss = blockStateSouthSouth.getBlock(); //South x2
            if (ss == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouth.getX(), posSouthSouth.getY(), posSouthSouth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ss.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posSouthSouth, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Diagonal directions two blocks from the player
            BlockPos posNorthNorthWest = player.blockPosition().north().north().west();
            BlockState blockStateNorthNorthWest = player.level.getBlockState(posNorthNorthWest);
            Block nnw = blockStateNorthNorthWest.getBlock(); //nw x2
            if (nnw == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthWest.getX(), posNorthNorthWest.getY(), posNorthNorthWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = nnw.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posNorthNorthWest, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNorthNorthEast = player.blockPosition().north().north().east();
            BlockState blockStateNorthNorthEast = player.level.getBlockState(posNorthNorthEast);
            Block nne = blockStateNorthNorthEast.getBlock(); //ne x2
            if (nne == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthEast.getX(), posNorthNorthEast.getY(), posNorthNorthEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = nne.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posNorthNorthEast, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouthWest = player.blockPosition().south().south().west();
            BlockState blockStateSouthSouthWest = player.level.getBlockState(posSouthSouthWest);
            Block ssw = blockStateSouthSouthWest.getBlock(); //sw x2
            if (ssw == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthWest.getX(), posSouthSouthWest.getY(), posSouthSouthWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ssw.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posSouthSouthWest, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouthEast = player.blockPosition().south().south().east();
            BlockState blockStateSouthSouthEast = player.level.getBlockState(posSouthSouthEast);
            Block sse = blockStateSouthSouthEast.getBlock(); //ne x2
            if (sse == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthEast.getX(), posSouthSouthEast.getY(), posSouthSouthEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = sse.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posSouthSouthEast, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Corners, outmost
            BlockPos posNorthNorthWestCorner = player.blockPosition().north().north().west().west();
            BlockState blockStateNorthNorthWestCorner = player.level.getBlockState(posNorthNorthWestCorner);
            Block c1 = blockStateNorthNorthWestCorner.getBlock(); //nw x2
            if (c1 == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthWestCorner.getX(), posNorthNorthWestCorner.getY(), posNorthNorthWestCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = c1.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posNorthNorthWestCorner, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNorthNorthEastCorner = player.blockPosition().north().north().east().east();
            BlockState blockStateNorthNorthEastCorner = player.level.getBlockState(posNorthNorthEastCorner);
            Block c2 = blockStateNorthNorthEastCorner.getBlock(); //ne x2
            if (c2 == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthEastCorner.getX(), posNorthNorthEastCorner.getY(), posNorthNorthEastCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = c2.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posNorthNorthEastCorner, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouthWestCorner = player.blockPosition().south().south().west().west();
            BlockState blockStateSouthSouthWestCorner = player.level.getBlockState(posSouthSouthWestCorner);
            Block c3 = blockStateSouthSouthWestCorner.getBlock(); //sw x2
            if (c3 == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthWestCorner.getX(), posSouthSouthWestCorner.getY(), posSouthSouthWestCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = c3.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posSouthSouthWestCorner, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouthEastCorner = player.blockPosition().south().south().east().east();
            BlockState blockStateSouthSouthEastCorner = player.level.getBlockState(posSouthSouthEastCorner);
            Block c4 = blockStateSouthSouthEastCorner.getBlock(); //ne x2
            if (c4 == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_itempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthEastCorner.getX(), posSouthSouthEastCorner.getY(), posSouthSouthEastCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = c4.defaultBlockState().getBlockState();
                sacrificecount = 0;
                event.player.level.setBlock(posSouthSouthEastCorner, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
        }
    }

    @SubscribeEvent
    public void sacrificeshrineglasssearcher(TickEvent.PlayerTickEvent event)
    {
        if(sacrificecount >= 3 && glassreward == 1) {
            LivingEntity player = event.player;
            BlockPos posWest = player.blockPosition().west();
            BlockState blockStateWest = player.level.getBlockState(posWest);
            Block w = blockStateWest.getBlock(); //West
            if (w == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posWest.getX(), posWest.getY(), posWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = w.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posWest, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posEast = player.blockPosition().east();
            BlockState blockStateEast = player.level.getBlockState(posEast);
            Block e = blockStateEast.getBlock(); //East
            if (e == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posEast.getX(), posEast.getY(), posEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = e.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posEast, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNorth = player.blockPosition().north();
            BlockState blockStateNorth = player.level.getBlockState(posNorth);
            Block n = blockStateNorth.getBlock(); //North
            if (n == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorth.getX(), posNorth.getY(), posNorth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = n.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorth, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouth = player.blockPosition().south();
            BlockState blockStateSouth = player.level.getBlockState(posSouth);
            Block s = blockStateSouth.getBlock(); //South
            if (s == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouth.getX(), posSouth.getY(), posSouth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = s.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouth, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Diagonal directions one block from player
            BlockPos posSW = player.blockPosition().south().west();
            BlockState blockStateSW = player.level.getBlockState(posSW);
            Block sw = blockStateSW.getBlock(); //SW
            if (sw == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSW.getX(), posSW.getY(), posSW.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = sw.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posSW, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSE = player.blockPosition().south().east();
            BlockState blockStateSE = player.level.getBlockState(posSE);
            Block se = blockStateSE.getBlock(); //SE
            if (se == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSE.getX(), posSE.getY(), posSE.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = se.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posSE, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNW = player.blockPosition().north().west();
            BlockState blockStateNW = player.level.getBlockState(posNW);
            Block nw = blockStateNW.getBlock(); //NW
            if (nw == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNW.getX(), posNW.getY(), posNW.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = nw.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posNW, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNE = player.blockPosition().north().east();
            BlockState blockStateNE = player.level.getBlockState(posNE);
            Block ne = blockStateNE.getBlock(); //NE
            if (ne == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNE.getX(), posNE.getY(), posNE.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ne.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posNE, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Cardinal directions two blocks from player
            BlockPos posWestWest = player.blockPosition().west().west();
            BlockState blockStateWestWest = player.level.getBlockState(posWestWest);
            Block ww = blockStateWestWest.getBlock(); //West x2
            if (ww == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posWestWest.getX(), posWestWest.getY(), posWestWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ww.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posWestWest, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posEastEast = player.blockPosition().east().east();
            BlockState blockStateEastEast = player.level.getBlockState(posEastEast);
            Block ee = blockStateEastEast.getBlock(); //East x2
            if (ee == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posEastEast.getX(), posEastEast.getY(), posEastEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ee.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posEastEast, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNorthNorth = player.blockPosition().north().north();
            BlockState blockStateNorthNorth = player.level.getBlockState(posNorthNorth);
            Block nn = blockStateNorthNorth.getBlock(); //North x2
            if (nn == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorth.getX(), posNorthNorth.getY(), posNorthNorth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = nn.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorth, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouth = player.blockPosition().south().south();
            BlockState blockStateSouthSouth = player.level.getBlockState(posSouthSouth);
            Block ss = blockStateSouthSouth.getBlock(); //South x2
            if (ss == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouth.getX(), posSouthSouth.getY(), posSouthSouth.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ss.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouth, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Diagonal directions two blocks from the player
            BlockPos posNorthNorthWest = player.blockPosition().north().north().west();
            BlockState blockStateNorthNorthWest = player.level.getBlockState(posNorthNorthWest);
            Block nnw = blockStateNorthNorthWest.getBlock(); //nw x2
            if (nnw == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthWest.getX(), posNorthNorthWest.getY(), posNorthNorthWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = nnw.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorthWest, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNorthNorthEast = player.blockPosition().north().north().east();
            BlockState blockStateNorthNorthEast = player.level.getBlockState(posNorthNorthEast);
            Block nne = blockStateNorthNorthEast.getBlock(); //ne x2
            if (nne == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthEast.getX(), posNorthNorthEast.getY(), posNorthNorthEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = nne.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorthEast, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouthWest = player.blockPosition().south().south().west();
            BlockState blockStateSouthSouthWest = player.level.getBlockState(posSouthSouthWest);
            Block ssw = blockStateSouthSouthWest.getBlock(); //sw x2
            if (ssw == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthWest.getX(), posSouthSouthWest.getY(), posSouthSouthWest.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = ssw.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouthWest, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouthEast = player.blockPosition().south().south().east();
            BlockState blockStateSouthSouthEast = player.level.getBlockState(posSouthSouthEast);
            Block sse = blockStateSouthSouthEast.getBlock(); //ne x2
            if (sse == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthEast.getX(), posSouthSouthEast.getY(), posSouthSouthEast.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = sse.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouthEast, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            //Corners, outmost
            BlockPos posNorthNorthWestCorner = player.blockPosition().north().north().west().west();
            BlockState blockStateNorthNorthWestCorner = player.level.getBlockState(posNorthNorthWestCorner);
            Block c1 = blockStateNorthNorthWestCorner.getBlock(); //nw x2
            if (c1 == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthWestCorner.getX(), posNorthNorthWestCorner.getY(), posNorthNorthWestCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = c1.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorthWestCorner, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posNorthNorthEastCorner = player.blockPosition().north().north().east().east();
            BlockState blockStateNorthNorthEastCorner = player.level.getBlockState(posNorthNorthEastCorner);
            Block c2 = blockStateNorthNorthEastCorner.getBlock(); //ne x2
            if (c2 == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posNorthNorthEastCorner.getX(), posNorthNorthEastCorner.getY(), posNorthNorthEastCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = c2.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posNorthNorthEastCorner, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouthWestCorner = player.blockPosition().south().south().west().west();
            BlockState blockStateSouthSouthWestCorner = player.level.getBlockState(posSouthSouthWestCorner);
            Block c3 = blockStateSouthSouthWestCorner.getBlock(); //sw x2
            if (c3 == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthWestCorner.getX(), posSouthSouthWestCorner.getY(), posSouthSouthWestCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = c3.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouthWestCorner, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
            BlockPos posSouthSouthEastCorner = player.blockPosition().south().south().east().east();
            BlockState blockStateSouthSouthEastCorner = player.level.getBlockState(posSouthSouthEastCorner);
            Block c4 = blockStateSouthSouthEastCorner.getBlock(); //ne x2
            if (c4 == SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get()) {
                Random LootSelector = new Random();
                ItemStack rewardstack1 = new ItemStack(specialshrine_glassitempool.getRandomElement(LootSelector));
                ItemEntity rewardentity1 = new ItemEntity(event.player.level, posSouthSouthEastCorner.getX(), posSouthSouthEastCorner.getY(), posSouthSouthEastCorner.getZ(), rewardstack1);
                event.player.level.addFreshEntity(rewardentity1);
                BlockState state = c4.defaultBlockState().getBlockState();
                sacrificecount = 0;
                glassreward = 0;
                event.player.level.setBlock(posSouthSouthEastCorner, SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                        state.getValue(HORIZONTAL_FACING)), 2);
            }
        }
    }
}
