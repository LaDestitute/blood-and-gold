package com.ladestitute.bloodandgold.util.events.materials;

import com.ladestitute.bloodandgold.entities.thrown.*;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.blood.BaGBloodCapabilityProvider;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityFactory;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.Random;

public class BaGGoldWeaponsHandler {

    @SubscribeEvent
    public void killlistener(LivingDeathEvent event) {
        Entity thrownEntity = event.getSource().getDirectEntity();

        if(thrownEntity instanceof EntityThrownGoldDagger ||thrownEntity instanceof EntityThrownGoldSpear)
        {
            if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
            Random goldchance = new Random();
            int golddrop = goldchance.nextInt(100);
            ItemStack nuggetstack = new ItemStack(Items.GOLD_NUGGET);
            ItemEntity nugget = new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().getX(), event.getEntityLiving().getY(), event.getEntityLiving().getZ(), nuggetstack);
                ItemStack stack =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_GOLD.get(), event.getEntityLiving()).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if (stack.isEmpty() && golddrop <= 19) {
                event.getEntityLiving().level.addFreshEntity(nugget);
            }
                if (!stack.isEmpty() && golddrop <= 49) {
                    event.getEntityLiving().level.addFreshEntity(nugget);
                }
            });
        }

        if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                {
                    for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                        if (held.getItem() == Items.GOLDEN_SWORD||
                                held.getItem() == ItemInit.GOLD_BOW.get()||
                                held.getItem() == ItemInit.GOLD_DAGGER.get()||
                                held.getItem() == ItemInit.GOLD_CROSSBOW.get()||
                                held.getItem() == ItemInit.GOLD_FLAIL.get()||
                                held.getItem() == ItemInit.GOLD_RAPIER.get()||
                                held.getItem() == ItemInit.GOLD_SPEAR.get()) {
                            Random goldchance = new Random();
                            int golddrop = goldchance.nextInt(100);
                            ItemStack nuggetstack = new ItemStack(Items.GOLD_NUGGET);
                            ItemEntity nugget = new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().getX(), event.getEntityLiving().getY(), event.getEntityLiving().getZ(), nuggetstack);
                            ItemStack stack =
                                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_GOLD.get(), event.getEntityLiving()).map(
                                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                            if (stack.isEmpty() && golddrop <= 19) {
                                event.getEntityLiving().level.addFreshEntity(nugget);
                            }
                            if (!stack.isEmpty() && golddrop <= 49) {
                                event.getEntityLiving().level.addFreshEntity(nugget);
                            }
                        }
                    }
                }
            });
    }

    @SubscribeEvent
    public void golddamagebonus(LivingHurtEvent event) {
        Entity sourceEntity = event.getSource().getEntity();

        if (sourceEntity instanceof PlayerEntity) {
            for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                sourceEntity.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
                {
                    for (ItemStack goldcost : ((PlayerEntity) sourceEntity).inventory.items) {
                        if (goldcost.getItem() == Items.GOLD_NUGGET && goldcost.getItem().getDefaultInstance().getCount() >= 1 && held.getItem() == Items.GOLDEN_SWORD
                        ||goldcost.getItem() == Items.GOLD_NUGGET && goldcost.getItem().getDefaultInstance().getCount() >= 1
                                && held.getItem() == ItemInit.GOLD_DAGGER.get()||
                                goldcost.getItem() == Items.GOLD_NUGGET && goldcost.getItem().getDefaultInstance().getCount() >= 1
                                        && held.getItem() == ItemInit.GOLD_BOW.get()||
                                goldcost.getItem() == Items.GOLD_NUGGET && goldcost.getItem().getDefaultInstance().getCount() >= 1
                                        && held.getItem() == ItemInit.GOLD_FLAIL.get()|
                                        goldcost.getItem() == Items.GOLD_NUGGET && goldcost.getItem().getDefaultInstance().getCount() >= 1
                                        && held.getItem() == ItemInit.GOLD_CROSSBOW.get()||
                                goldcost.getItem() == Items.GOLD_NUGGET && goldcost.getItem().getDefaultInstance().getCount() >= 1
                                        && held.getItem() == ItemInit.GOLD_RAPIER.get()||
                                goldcost.getItem() == Items.GOLD_NUGGET && goldcost.getItem().getDefaultInstance().getCount() >= 1
                                        && held.getItem() == ItemInit.GOLD_SPEAR.get()) {
                            Random goldmgbonus = new Random();
                            int golddmg = goldmgbonus.nextInt(100);
                            ItemStack stack =
                                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_GOLD.get(), event.getEntityLiving()).map(
                                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                            if (stack.isEmpty() && golddmg <= 24)
                            {
                                if (BaGConfigHandler.getInstance().goldspecialeffectmultiplier() == 1) {
                                    event.setAmount((float) (event.getAmount() * 1.5));
                                    if (goldcost.getItem() == Items.GOLD_NUGGET) {
                                        goldcost.shrink(1);
                                    }
                                }
                                if (BaGConfigHandler.getInstance().goldspecialeffectmultiplier() == 2) {
                                    event.setAmount((float) (event.getAmount() * 2.25));
                                    if (goldcost.getItem() == Items.GOLD_NUGGET) {
                                        goldcost.shrink(1);
                                    }
                                }
                                if (BaGConfigHandler.getInstance().goldspecialeffectmultiplier() == 3) {
                                    event.setAmount((float) (event.getAmount() * 3));
                                    if (goldcost.getItem() == Items.GOLD_NUGGET) {
                                        goldcost.shrink(1);
                                    }
                                }
                            }
                            if (!stack.isEmpty() && golddmg <= 74)
                            {
                                if (BaGConfigHandler.getInstance().goldspecialeffectmultiplier() == 1) {
                                    event.setAmount((float) (event.getAmount() * 1.5));
                                    if (goldcost.getItem() == Items.GOLD_NUGGET) {
                                        goldcost.shrink(1);
                                    }
                                }
                                if (BaGConfigHandler.getInstance().goldspecialeffectmultiplier() == 2) {
                                    event.setAmount((float) (event.getAmount() * 2.25));
                                    if (goldcost.getItem() == Items.GOLD_NUGGET) {
                                        goldcost.shrink(1);
                                    }
                                }
                                if (BaGConfigHandler.getInstance().goldspecialeffectmultiplier() == 3) {
                                    event.setAmount((float) (event.getAmount() * 3));
                                    if (goldcost.getItem() == Items.GOLD_NUGGET) {
                                        goldcost.shrink(1);
                                    }
                                }
                            }
                        }
                    }
                });

            }

        }

        }
          }



