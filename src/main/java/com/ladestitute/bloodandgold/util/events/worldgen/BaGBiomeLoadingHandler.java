package com.ladestitute.bloodandgold.util.events.worldgen;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ConfiguredFeaturesInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.feature.template.RuleTest;
import net.minecraft.world.gen.placement.IPlacementConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class BaGBiomeLoadingHandler {
    @SubscribeEvent(priority = EventPriority.HIGH)
    public static void modifyBiomes(BiomeLoadingEvent event) {
        if (event.getName() != null) {
            RegistryKey<Biome> biomeRegistryKey = RegistryKey.create(Registry.BIOME_REGISTRY, event.getName());
            ModificationHelper helper = new ModificationHelper(event);

            if (BiomeDictionary.hasType(biomeRegistryKey, BiomeDictionary.Type.OVERWORLD)) {
                if(event.getCategory() == Biome.Category.FOREST ||
                        event.getCategory() == Biome.Category.SAVANNA ||
                        event.getCategory() == Biome.Category.MESA ||
                        event.getCategory() == Biome.Category.PLAINS ||
                        event.getCategory() == Biome.Category.ICY ||
                        event.getCategory() == Biome.Category.DESERT ||
                        event.getCategory() == Biome.Category.BEACH ||
                        event.getCategory() == Biome.Category.JUNGLE ||
                        event.getCategory() == Biome.Category.OCEAN ||
                        event.getCategory() == Biome.Category.RIVER ||
                        event.getCategory() == Biome.Category.SWAMP ||
                        event.getCategory() == Biome.Category.TAIGA) {
                    // helper.addFeature(ConfiguredFeaturesInit.SHRINE_STONE_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_ORES);
                    helper.addFeature(ConfiguredFeaturesInit.SHRINE_OF_BLOOD_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_DECORATION);
                     helper.addFeature(ConfiguredFeaturesInit.SHRINE_OF_DARKNESS_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_DECORATION);
                     helper.addFeature(ConfiguredFeaturesInit.SHRINE_OF_PEACE_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_DECORATION);
                    helper.addFeature(ConfiguredFeaturesInit.SHRINE_OF_GLASS_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_DECORATION);
                     helper.addFeature(ConfiguredFeaturesInit.SHRINE_OF_RHYTHM_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_DECORATION);
                     helper.addFeature(ConfiguredFeaturesInit.SHRINE_OF_SACRIFICE_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_DECORATION);
                      helper.addFeature(ConfiguredFeaturesInit.SHRINE_OF_PAIN_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_DECORATION);
                     helper.addFeature(ConfiguredFeaturesInit.SHRINE_OF_SPACE_CONFIGURED, GenerationStage.Decoration.UNDERGROUND_DECORATION);
                }
            }
        }
    }

    private static void generateOre(BiomeGenerationSettingsBuilder settings, RuleTest fillerType, BlockState state,
                                    int veinSize, int minHeight, int maxHeight, int maxperchunk) {
        settings.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES,
                Feature.ORE.configured(new OreFeatureConfig(fillerType, state, veinSize))
                        .decorated(Placement.RANGE.configured(new TopSolidRangeConfig(minHeight, 0, maxHeight)))
                        .squared().count(maxperchunk));
    }

    private static class ModificationHelper {
        private static BiomeLoadingEvent event;

        private ModificationHelper(BiomeLoadingEvent event) {
            ModificationHelper.event = event;
        }

        private void addFeature(ConfiguredFeature<?, ?> feature, GenerationStage.Decoration stage) {
            event.getGeneration().addFeature(stage, feature);
        }

        private void addStructure(StructureFeature<?, ?> structure) {
            event.getGeneration().getStructures().add(() -> structure);
        }
    }
}

