package com.ladestitute.bloodandgold.util.events;

import com.ladestitute.bloodandgold.entities.spells.EntityFireball;
import com.ladestitute.bloodandgold.registries.ItemInit;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingDestroyBlockEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.Random;

public class BaGInfernalTorchHandler {

    @SubscribeEvent
    public void effectproc(PlayerEvent.HarvestCheck event)
    {
        ItemStack stack =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.INFERNAL_TORCH.get(), event.getPlayer()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        //  double radius = 8;
        //  AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().blockPosition()).inflate(radius).expandTowards(0.0D, 16, 0.0D);
        //  List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
        if (!stack.isEmpty()) {
                Random rand = new Random();
                int prochance = rand.nextInt(101);
                if(prochance <= 19)
                {
                    EntityFireball fireball1 = new EntityFireball(event.getPlayer(), event.getPlayer().level);
                    fireball1.shootFromRotation(event.getPlayer(), event.getPlayer().xRot,
                            event.getPlayer().yRot+0, 0.0F, 1.5F, 1F);
                    EntityFireball fireball2 = new EntityFireball(event.getPlayer(), event.getPlayer().level);
                    fireball2.shootFromRotation(event.getPlayer(), event.getPlayer().xRot,
                            event.getPlayer().yRot+0, 0.0F, -1.5F, 1F);
                    event.getPlayer().level.addFreshEntity(fireball1);
                    event.getPlayer().level.addFreshEntity(fireball2);
                }

        }
    }
}
