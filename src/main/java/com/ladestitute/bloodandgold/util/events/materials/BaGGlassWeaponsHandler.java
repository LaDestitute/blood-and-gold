package com.ladestitute.bloodandgold.util.events.materials;

import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.player.PlayerDestroyItemEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;

public class BaGGlassWeaponsHandler {

    public BaGGlassWeaponsHandler() {
    }

    @SubscribeEvent
    public void glassdigability(PlayerInteractEvent.RightClickBlock event)
    {
        BlockState blockState = event.getEntityLiving().level.getBlockState(event.getPos());
        Block block = blockState.getBlock();
        ItemStack shovelstack = event.getPlayer().getItemBySlot(EquipmentSlotType.MAINHAND);
        if (event.getEntityLiving() instanceof PlayerEntity
                && shovelstack.getItem() == ItemInit.GLASS_SHOVEL.get()) {
            if (block != Blocks.BARRIER || block != Blocks.BEDROCK ||
                    block != Blocks.COMMAND_BLOCK || block != Blocks.END_GATEWAY ||
                    block != Blocks.END_PORTAL || block != Blocks.END_PORTAL_FRAME ||
                    block != Blocks.JIGSAW || block != Blocks.NETHER_PORTAL ||
                    block != Blocks.STRUCTURE_BLOCK || block != Blocks.STRUCTURE_VOID ||
                    block != Blocks.LAVA || block != Blocks.WATER ||
                    block != SpecialBlocksInit.SHRINE_OF_BLOOD_UNACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_DARKNESS_UNACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_GLASS_UNACTIVATED.get()||
                    block != SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()||
                    block != SpecialBlocksInit.SHRINE_OF_PEACE_UNACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_RHYTHM_UNACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_SPACE_UNACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_BLOOD_ACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_DARKNESS_ACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_GLASS_ACTIVATED.get()||
                    block != SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get()||
                    block != SpecialBlocksInit.SHRINE_OF_PEACE_ACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_RHYTHM_ACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get() ||
                    block != SpecialBlocksInit.SHRINE_OF_SPACE_ACTIVATED.get()) {
                event.getPlayer().level.destroyBlock(event.getPos(), true);
                shovelstack.hurtAndBreak(1, event.getEntityLiving(), (p_220045_0_) -> {
                    p_220045_0_.broadcastBreakEvent(EquipmentSlotType.MAINHAND);
                });
            }
        }
    }

    //Handler for breaking glass weapons
    @SubscribeEvent
    public void glassarmoreffect(LivingDamageEvent event) {
        ItemStack armorstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.CHEST);
        Entity sourceEntity = event.getSource().getDirectEntity();
        if (event.getEntityLiving() instanceof PlayerEntity
                && armorstack.getItem() == ItemInit.GLASS_ARMOR.get())
        {
            if(BaGConfigHandler.getInstance().rangedmgbreaksglass()
                    && sourceEntity instanceof AbstractArrowEntity)
            {
                event.setAmount(0F);
                armorstack.hurtAndBreak(100, event.getEntityLiving(), (p_220045_0_) -> {
                    p_220045_0_.broadcastBreakEvent(EquipmentSlotType.MAINHAND);
                    ItemHandlerHelper.giveItemToPlayer((PlayerEntity) event.getEntityLiving(), ItemInit.GLASS_SHARD.get().getDefaultInstance());
                });
            }
            if (BaGConfigHandler.getInstance().environmentaldmgbreaksglass()) {
                if (event.getSource() == DamageSource.CACTUS || event.getSource() == DamageSource.ANVIL ||
                        event.getSource() == DamageSource.CRAMMING || event.getSource() == DamageSource.DROWN ||
                        event.getSource() == DamageSource.FALL || event.getSource() == DamageSource.FALLING_BLOCK ||
                        event.getSource() == DamageSource.HOT_FLOOR || event.getSource() == DamageSource.IN_FIRE ||
                        event.getSource() == DamageSource.LAVA || event.getSource() == DamageSource.LIGHTNING_BOLT ||
                        event.getSource() == DamageSource.SWEET_BERRY_BUSH || event.getSource() == DamageSource.ON_FIRE) {
                    event.setAmount(0F);
                    armorstack.hurtAndBreak(100, event.getEntityLiving(), (p_220045_0_) -> {
                        p_220045_0_.broadcastBreakEvent(EquipmentSlotType.MAINHAND);
                        ItemHandlerHelper.giveItemToPlayer((PlayerEntity) event.getEntityLiving(), ItemInit.GLASS_SHARD.get().getDefaultInstance());
                    });
                }
            }

            if (BaGConfigHandler.getInstance().directdmgbreaksglass()) {
                if (event.getSource() != DamageSource.CACTUS & event.getSource() != DamageSource.ANVIL &
                        event.getSource() != DamageSource.CRAMMING & event.getSource() != DamageSource.DROWN &
                        event.getSource() != DamageSource.FALL & event.getSource() != DamageSource.FALLING_BLOCK &
                        event.getSource() != DamageSource.HOT_FLOOR & event.getSource() != DamageSource.IN_FIRE &
                        event.getSource() != DamageSource.LAVA & event.getSource() != DamageSource.LIGHTNING_BOLT &
                        event.getSource() != DamageSource.SWEET_BERRY_BUSH & event.getSource() != DamageSource.ON_FIRE) {
                    event.setAmount(0F);
                    armorstack.hurtAndBreak(100, event.getEntityLiving(), (p_220045_0_) -> {
                        p_220045_0_.broadcastBreakEvent(EquipmentSlotType.MAINHAND);
                        ItemHandlerHelper.giveItemToPlayer((PlayerEntity) event.getEntityLiving(), ItemInit.GLASS_SHARD.get().getDefaultInstance());
                    });
                }
            }

        }
    }

    @SubscribeEvent
    public void dropmattockhead(PlayerDestroyItemEvent event)
    {
        if(event.getOriginal().getItem() == ItemInit.GLASS_SWORD.get()||
                event.getOriginal().getItem() == ItemInit.GLASS_DAGGER.get()||
                event.getOriginal().getItem() == ItemInit.GLASS_SPEAR.get()||
                event.getOriginal().getItem() == ItemInit.GLASS_RAPIER.get()||
                event.getOriginal().getItem() == ItemInit.GLASS_FLAIL.get()||
                event.getOriginal().getItem() == ItemInit.GLASS_BOW.get()||
                event.getOriginal().getItem() == ItemInit.GLASS_CROSSBOW.get()||
                event.getOriginal().getItem() == ItemInit.GLASS_SHOVEL.get())
        {
            ItemStack stack1 = new ItemStack(ItemInit.GLASS_SHARD.get());
            ItemEntity shard = new ItemEntity(event.getPlayer().level, event.getPlayer().position().x, event.getPlayer().position().y, event.getPlayer().position().z, stack1);
            event.getPlayer().level.addFreshEntity(shard);
        }
    }


    @SubscribeEvent
    public void breakingglasshandling(LivingDamageEvent event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        ItemStack armorstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.CHEST);
        Entity sourceEntity = event.getSource().getDirectEntity();
        if (event.getEntityLiving() instanceof PlayerEntity
                && armorstack.getItem() != ItemInit.GLASS_ARMOR.get())
        {
        if (event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.GLASS_SWORD.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.GLASS_DAGGER.get() ||
                event.getEntityLiving() instanceof PlayerEntity
                        && swordstack.getItem() == ItemInit.GLASS_SPEAR.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.GLASS_RAPIER.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.GLASS_FLAIL.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.GLASS_BOW.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.GLASS_CROSSBOW.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.GLASS_SHOVEL.get()) {
            if (BaGConfigHandler.getInstance().rangedmgbreaksglass()
                    && sourceEntity instanceof AbstractArrowEntity) {
                swordstack.hurtAndBreak(16, event.getEntityLiving(), (p_220045_0_) -> {
                    p_220045_0_.broadcastBreakEvent(EquipmentSlotType.MAINHAND);
                    ItemHandlerHelper.giveItemToPlayer((PlayerEntity) event.getEntityLiving(), ItemInit.GLASS_SHARD.get().getDefaultInstance());
                });
            }
            if (BaGConfigHandler.getInstance().environmentaldmgbreaksglass()) {
                if (event.getSource() == DamageSource.CACTUS || event.getSource() == DamageSource.ANVIL ||
                        event.getSource() == DamageSource.CRAMMING || event.getSource() == DamageSource.DROWN ||
                        event.getSource() == DamageSource.FALL || event.getSource() == DamageSource.FALLING_BLOCK ||
                        event.getSource() == DamageSource.HOT_FLOOR || event.getSource() == DamageSource.IN_FIRE ||
                        event.getSource() == DamageSource.LAVA || event.getSource() == DamageSource.LIGHTNING_BOLT ||
                        event.getSource() == DamageSource.SWEET_BERRY_BUSH || event.getSource() == DamageSource.ON_FIRE) {
                    swordstack.hurtAndBreak(16, event.getEntityLiving(), (p_220045_0_) -> {
                        p_220045_0_.broadcastBreakEvent(EquipmentSlotType.MAINHAND);
                        ItemHandlerHelper.giveItemToPlayer((PlayerEntity) event.getEntityLiving(), ItemInit.GLASS_SHARD.get().getDefaultInstance());
                    });
                }
            }

            if (BaGConfigHandler.getInstance().directdmgbreaksglass()) {
                if (event.getSource() != DamageSource.CACTUS & event.getSource() != DamageSource.ANVIL &
                        event.getSource() != DamageSource.CRAMMING & event.getSource() != DamageSource.DROWN &
                        event.getSource() != DamageSource.FALL & event.getSource() != DamageSource.FALLING_BLOCK &
                        event.getSource() != DamageSource.HOT_FLOOR & event.getSource() != DamageSource.IN_FIRE &
                        event.getSource() != DamageSource.LAVA & event.getSource() != DamageSource.LIGHTNING_BOLT &
                        event.getSource() != DamageSource.SWEET_BERRY_BUSH & event.getSource() != DamageSource.ON_FIRE) {
                    swordstack.hurtAndBreak(16, event.getEntityLiving(), (p_220045_0_) -> {
                        p_220045_0_.broadcastBreakEvent(EquipmentSlotType.MAINHAND);
                        ItemHandlerHelper.giveItemToPlayer((PlayerEntity) event.getEntityLiving(), ItemInit.GLASS_SHARD.get().getDefaultInstance());
                    });
                }
            }
        }

        }
    }

}

