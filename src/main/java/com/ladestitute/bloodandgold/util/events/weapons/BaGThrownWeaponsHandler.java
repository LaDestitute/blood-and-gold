package com.ladestitute.bloodandgold.util.events.weapons;

import com.ladestitute.bloodandgold.entities.thrown.*;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.client.BaGKeybinds;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BaGThrownWeaponsHandler {

    @SubscribeEvent
    public void throwables(TickEvent.PlayerTickEvent event)
    {
        //Spears travel much further than daggers
        if (BaGKeybinds.throwweapon.isDown()) {
            for (ItemStack held : event.player.getHandSlots()) {
                if (held.getItem() == ItemInit.GOLD_DAGGER.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownGoldDagger thrown = new EntityThrownGoldDagger(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_GOLD_DAGGER.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                if (held.getItem() == ItemInit.GOLD_SPEAR.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownGoldSpear thrown = new EntityThrownGoldSpear(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_GOLD_DAGGER.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.BLOOD_DAGGER.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownBloodDagger thrown = new EntityThrownBloodDagger(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_BLOOD_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.BLOOD_SPEAR.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownBloodSpear thrown = new EntityThrownBloodSpear(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_BLOOD_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.TITANIUM_DAGGER.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownTitaniumDagger thrown = new EntityThrownTitaniumDagger(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_BLOOD_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.TITANIUM_SPEAR.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownTitaniumSpear thrown = new EntityThrownTitaniumSpear(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_BLOOD_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.GLASS_DAGGER.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownGlassDagger thrown = new EntityThrownGlassDagger(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_GLASS_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.GLASS_SPEAR.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownGlassSpear thrown = new EntityThrownGlassSpear(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_GLASS_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.OBSIDIAN_DAGGER.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownObsidianDagger thrown = new EntityThrownObsidianDagger(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_OBSIDIAN_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.OBSIDIAN_SPEAR.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownObsidianSpear thrown = new EntityThrownObsidianSpear(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_OBSIDIAN_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.JEWELED_DAGGER.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownJeweledDagger thrown = new EntityThrownJeweledDagger(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_JEWELED_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.DAGGER_OF_FROST.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownFrostDagger thrown = new EntityThrownFrostDagger(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_FROST_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.DAGGER.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownDagger thrown = new EntityThrownDagger(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_BLOOD_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
                //
                if (held.getItem() == ItemInit.GLASS_SHARD.get()) {
                    if(!event.player.level.isClientSide) {
                        held.shrink(1);
                        //Create a new instance of our entity
                        EntityThrownGlassShard thrown = new EntityThrownGlassShard(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack daggerstack = new ItemStack(ItemInit.THROWN_GLASS_WEAPON.get());
                        thrown.setItem(daggerstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                        event.player.level.addFreshEntity(thrown);
                    }
                }
            }
        }
    }

}
