package com.ladestitute.bloodandgold.util.events.weapons;

import com.ladestitute.bloodandgold.client.BaGKeybinds;
import com.ladestitute.bloodandgold.entities.thrown.EntityThrownFrostDagger;
import com.ladestitute.bloodandgold.entities.thrown.EntityThrownGoldSpear;
import com.ladestitute.bloodandgold.registries.EffectInit;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import com.ladestitute.bloodandgold.util.bagcaps.rhythm.BaGRhythmCapabilityProvider;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.Explosion;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.List;

public class BaGEffectsHandler {

    private Object LivingEntity;
    public int freezingblockadded;

    @SubscribeEvent
    public void daggeroffrosteffect(LivingHurtEvent event) {
        ItemStack stack =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.FROST_CHARM.get(), event.getEntityLiving()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

        Entity sourceEntity = event.getSource().getEntity();

        Entity thrownEntity = event.getSource().getDirectEntity();

        if(thrownEntity instanceof EntityThrownFrostDagger)
        {
            event.getEntityLiving().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                //event.getSource().bypassArmor();
                event.getEntityLiving().addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 80, 10));
                h.isEntityFrozen(1);
                System.out.println("ENTITY FROZEN!");
            });
        }
        if (event.getEntityLiving() instanceof PlayerEntity)
        {
            for (ItemStack held : event.getEntityLiving().getHandSlots())
            {
                if (held.getItem() == ItemInit.DAGGER_OF_FROST.get() && BaGConfigHandler.getInstance().daggeroffrostmelts())
                {
                    if (event.getSource() == DamageSource.HOT_FLOOR || event.getSource() == DamageSource.IN_FIRE ||
                            event.getSource() == DamageSource.LAVA || event.getSource() == DamageSource.ON_FIRE)
                    {
                        held.shrink(1);
                        ItemHandlerHelper.giveItemToPlayer((PlayerEntity) event.getEntityLiving(), ItemInit.DAGGER.get().getDefaultInstance());
                    }
                }
            }
        }
        if (sourceEntity instanceof PlayerEntity) {
            for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                if (held.getItem() == ItemInit.DAGGER_OF_FROST.get()) {
                    event.getEntityLiving().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
                    {
                        //event.getSource().bypassArmor();
                        event.getEntityLiving().addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 80, 10));
                        h.isEntityFrozen(1);
                        System.out.println("ENTITY FROZEN!");
                    });
                }
                //  PlayerEntity player = (PlayerEntity)event.getEntityLiving();
            }
        }

        event.getEntityLiving().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
        if (!stack.isEmpty())
        {
            //Don't do a Check to see if the damage comes from DamageSource.GENERIC. I don't know what mob/block uses the "GENERIC" damage in the game so I normally do a (event.getSource != DamageSource.*Type*) if I don't want it to take less damage from something in particular.
            event.setAmount(0);
            double radius = 3;
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().blockPosition()).inflate(radius).expandTowards(0.0D, 16, 0.0D);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list)
            {
                if (!(livingEntity instanceof PlayerEntity)) {
                    livingEntity.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 80, 10));
                    livingEntity.level.setBlockAndUpdate(livingEntity.blockPosition(), SpecialBlocksInit.FROZEN_EFFECT.get().defaultBlockState());
                    h.isEntityFrozen(1);
                    System.out.println("ENTITY FROZEN!");
                }
            }
            stack.shrink(1);
        }

            if(h.checkfreezecharge() < 0)
            {
                h.setfreezecharge(0);
            }

            //
            if (h.isEntityFrozen() == 1) {
                if (BaGConfigHandler.getInstance().daggeroffrostspecialeffectmultiplier() == 1) {
                    event.setAmount((float) (event.getAmount() * 1.5));
                }
                if (BaGConfigHandler.getInstance().daggeroffrostspecialeffectmultiplier() == 2) {
                    event.setAmount((float) (event.getAmount() * 2.25));
                }
                if (BaGConfigHandler.getInstance().daggeroffrostspecialeffectmultiplier() == 3) {
                    event.setAmount((float) (event.getAmount() * 3));
                }
            }

        });

    }

    @SubscribeEvent
    public void frozeneffectdecay(LivingEvent.LivingUpdateEvent event) {

        event.getEntityLiving().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
            if (h.checkfrozenblock() == 0 && h.ticksFrozen() >= 15 && event.getEntityLiving().hasEffect(Effects.MOVEMENT_SLOWDOWN)) {
                event.getEntityLiving().level.setBlock(event.getEntityLiving().blockPosition(), SpecialBlocksInit.FROZEN_EFFECT.get().defaultBlockState(), 2);
                h.setfrozenblock(1);
            }
            if (h.isEntityFrozen() == 1) {
                h.addticksFrozen(1);
            }
            if (h.ticksFrozen() >= 70) {
                event.getEntityLiving().level.setBlock(event.getEntityLiving().blockPosition(), Blocks.AIR.defaultBlockState(), 2);
                h.setfrozenblock(0);
                h.resetticksFrozen(0);
                h.isEntityFrozen(0);
            }
        });
    }

    @SubscribeEvent
    public void iceilllistener(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                event.getEntityLiving().level.setBlock(event.getEntityLiving().blockPosition(), Blocks.AIR.defaultBlockState(), 2);
            });
    }

    @SubscribeEvent
    public void arrhythmiclistener(TickEvent.PlayerTickEvent event) {
        if (event.phase == TickEvent.Phase.END) {

            event.player.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                double radius = 9;
                ItemStack spell =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.FREEZE_SPELL.get(), event.player).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                ItemStack ring_of_mana =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_MANA.get(), event.player).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

                if(!spell.isEmpty() && BaGKeybinds.castheldspell.consumeClick() && h.checkfreezecharge() == 0) {
                    if(ring_of_mana.isEmpty())
                    {
                        AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.player.blockPosition()).inflate(radius).expandTowards(0.0D, 16, 0.0D);
                        List<LivingEntity> list = event.player.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
                        for (LivingEntity livingEntity : list) {
                            if (!(livingEntity instanceof PlayerEntity)) {
                                livingEntity.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 80, 10));
                                livingEntity.level.setBlockAndUpdate(livingEntity.blockPosition(), SpecialBlocksInit.FROZEN_EFFECT.get().defaultBlockState());
                                h.isEntityFrozen(1);
                                System.out.println("ENTITY FROZEN!");
                            }
                        }
                        h.setfreezecharge(8);
                    }
                    if(!ring_of_mana.isEmpty())
                    {
                        AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.player.blockPosition()).inflate(radius+6).expandTowards(0.0D, 16, 0.0D);
                        List<LivingEntity> list = event.player.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
                        for (LivingEntity livingEntity : list) {
                            if (!(livingEntity instanceof PlayerEntity)) {
                                livingEntity.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 160, 10));
                                livingEntity.level.setBlockAndUpdate(livingEntity.blockPosition(), SpecialBlocksInit.FROZEN_EFFECT.get().defaultBlockState());
                                h.isEntityFrozen(1);
                                System.out.println("ENTITY FROZEN!");
                            }
                        }
                        h.setfreezecharge(8);
                    }
                }


                h.preventityxposition(event.player.blockPosition().getX());
                h.preventityyposition(event.player.blockPosition().getY());
                h.preventityzposition(event.player.blockPosition().getZ());
                if (!event.player.hasEffect(EffectInit.ARRHYTHMIA_EFFECT.get())) {
                    h.resetticksarrhythmic(0);
                }

                if (event.player.hasEffect(EffectInit.ARRHYTHMIA_EFFECT.get())) {
                    if (event.player.blockPosition().getX() == h.getpreventityxposition() &&
                            event.player.blockPosition().getY() == h.getpreventityyposition() &&
                            event.player.blockPosition().getZ() == h.getpreventityzposition()) {
                        h.addticksarrhythmic(1);
                        //System.out.println("Move timer incrementing!");
                    }

                   if (event.player.getSpeed() >= 0.13) {
                        h.resetticksarrhythmic(0);
                        System.out.println("Move timer reset, player is moving!");
                    }

                 //  System.out.println("Player speed is " + event.player.getSpeed());

                    if (h.getticksarrhythmic() >= 200) {
                        event.player.hurt(DamageSource.GENERIC, 2F);
                        h.resetticksarrhythmic(0);
                    }

                }
            });
        }
    }
}







