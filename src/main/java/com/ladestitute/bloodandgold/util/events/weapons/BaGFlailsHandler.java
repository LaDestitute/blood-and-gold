package com.ladestitute.bloodandgold.util.events.weapons;

import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;

public class BaGFlailsHandler {

    double range = 5;

    @SubscribeEvent
    public void flailaoe1(AttackEntityEvent event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        if (swordstack.getItem() == ItemInit.BLOOD_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 3F);
            }
        }
        if (swordstack.getItem() == ItemInit.GLASS_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 3.5F);
            }
        }
        if (swordstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 2.5F);
            }
        }
        if (swordstack.getItem() == ItemInit.GOLD_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 1.25F);
            }
        }
        if (swordstack.getItem() == ItemInit.TITANIUM_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 2F);
            }
        }
    }

    @SubscribeEvent
    public void flailaoe2(PlayerInteractEvent.LeftClickBlock event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        if (swordstack.getItem() == ItemInit.BLOOD_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 3F);
            }
        }
        if (swordstack.getItem() == ItemInit.GLASS_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 3.5F);
            }
        }
        if (swordstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 2.5F);
            }
        }
        if (swordstack.getItem() == ItemInit.TITANIUM_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 2F);
            }
        }
    }

    @SubscribeEvent
    public void flailaoe3(PlayerInteractEvent.LeftClickEmpty event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        if (swordstack.getItem() == ItemInit.BLOOD_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 3F);
            }
        }
        if (swordstack.getItem() == ItemInit.GLASS_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 3.5F);
            }
        }
        if (swordstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 2.5F);
            }
        }
        if (swordstack.getItem() == ItemInit.TITANIUM_FLAIL.get())
        {
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().position().x - range, event.getEntityLiving().position().y - range, event.getEntityLiving().position().z - range,
                    event.getEntityLiving().position().x + range, event.getEntityLiving().position().y + range, event.getEntityLiving().position().z + range);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof PlayerEntity) {
                    return;
                }
                livingEntity.knockback(0.5F, (double)MathHelper.sin(event.getEntityLiving().yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(event.getEntityLiving().yRot * ((float)Math.PI / 180F))));
                livingEntity.hurt(DamageSource.playerAttack(event.getPlayer()), 2F);
            }
        }
    }
}
