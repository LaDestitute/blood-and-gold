package com.ladestitute.bloodandgold.util.events.materials;

import com.ladestitute.bloodandgold.entities.thrown.*;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.rhythm.BaGRhythmCapabilityProvider;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BaGKarateGiEffectHandler {

    @SubscribeEvent
    public void giarmorattackeffect(LivingHurtEvent event) {
        ItemStack armorstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.CHEST);
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        Entity sourceEntity = event.getSource().getDirectEntity();

        Entity thrownEntity = event.getSource().getDirectEntity();

        if (event.getEntityLiving() instanceof PlayerEntity
                && armorstack.getItem() == ItemInit.KARATE_GI.get()) {
            if(event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.BLOOD_SWORD.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.BLOOD_DAGGER.get() ||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.BLOOD_SPEAR.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.BLOOD_RAPIER.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.BLOOD_FLAIL.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.BLOOD_BOW.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.BLOOD_CROSSBOW.get()||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.GLASS_SWORD.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GLASS_DAGGER.get() ||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.GLASS_SPEAR.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GLASS_RAPIER.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GLASS_FLAIL.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GLASS_BOW.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GLASS_CROSSBOW.get()||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.OBSIDIAN_SWORD.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.OBSIDIAN_DAGGER.get() ||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.OBSIDIAN_SPEAR.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.OBSIDIAN_BOW.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get()||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.TITANIUM_SWORD.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.TITANIUM_DAGGER.get() ||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.TITANIUM_SPEAR.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.TITANIUM_RAPIER.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.TITANIUM_FLAIL.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.TITANIUM_BOW.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.TITANIUM_CROSSBOW.get()||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.DAGGER_OF_FROST.get()||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.JEWELED_DAGGER.get()||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.DAGGER.get()||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.GOLDEN_SWORD||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.WOODEN_SWORD||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.STONE_SWORD||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.IRON_SWORD||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.DIAMOND_SWORD||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.NETHERITE_SWORD||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.BOW||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.CROSSBOW||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == Items.TRIDENT||
                    event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GOLD_DAGGER.get() ||
                    event.getEntityLiving() instanceof PlayerEntity
                            && swordstack.getItem() == ItemInit.GOLD_SPEAR.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GOLD_RAPIER.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GOLD_FLAIL.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GOLD_BOW.get() || event.getEntityLiving() instanceof PlayerEntity
                    && swordstack.getItem() == ItemInit.GOLD_CROSSBOW.get()) {
                event.setAmount((float) (event.getAmount() * 2));
            }

            if(thrownEntity instanceof EntityThrownGoldSpear ||
                    thrownEntity instanceof EntityThrownBloodDagger ||thrownEntity instanceof EntityThrownBloodSpear ||
                    thrownEntity instanceof EntityThrownTitaniumDagger ||thrownEntity instanceof EntityThrownTitaniumSpear||
                    thrownEntity instanceof EntityThrownGlassDagger||thrownEntity instanceof EntityThrownGlassSpear||
                    thrownEntity instanceof EntityThrownObsidianDagger||thrownEntity instanceof EntityThrownObsidianSpear||
                    thrownEntity instanceof EntityThrownFrostDagger||thrownEntity instanceof EntityThrownJeweledDagger||
                    thrownEntity instanceof EntityThrownDagger||thrownEntity instanceof EntityThrownGlassShard)
            {
                event.setAmount((float) (event.getAmount() * 2));
            }
        }
    }

    @SubscribeEvent
    public void giarmoreffect(LivingDamageEvent event) {
        ItemStack armorstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.CHEST);
        Entity sourceEntity = event.getSource().getDirectEntity();
        if (event.getEntityLiving() instanceof PlayerEntity
                && armorstack.getItem() == ItemInit.KARATE_GI.get()) {
                if (sourceEntity instanceof AbstractArrowEntity) {
                        event.setAmount((float) (event.getAmount()*2));
                }
                if (event.getSource() == DamageSource.CACTUS || event.getSource() == DamageSource.ANVIL ||
                        event.getSource() == DamageSource.CRAMMING || event.getSource() == DamageSource.DROWN ||
                        event.getSource() == DamageSource.FALL || event.getSource() == DamageSource.FALLING_BLOCK ||
                        event.getSource() == DamageSource.HOT_FLOOR || event.getSource() == DamageSource.IN_FIRE ||
                        event.getSource() == DamageSource.LAVA || event.getSource() == DamageSource.LIGHTNING_BOLT ||
                        event.getSource() == DamageSource.SWEET_BERRY_BUSH || event.getSource() == DamageSource.ON_FIRE) {
                        event.setAmount((float) (event.getAmount()*2));
                }

                if (event.getSource() != DamageSource.CACTUS & event.getSource() != DamageSource.ANVIL &
                        event.getSource() != DamageSource.CRAMMING & event.getSource() != DamageSource.DROWN &
                        event.getSource() != DamageSource.FALL & event.getSource() != DamageSource.FALLING_BLOCK &
                        event.getSource() != DamageSource.HOT_FLOOR & event.getSource() != DamageSource.IN_FIRE &
                        event.getSource() != DamageSource.LAVA & event.getSource() != DamageSource.LIGHTNING_BOLT &
                        event.getSource() != DamageSource.SWEET_BERRY_BUSH & event.getSource() != DamageSource.ON_FIRE) {

                        event.setAmount((float) (event.getAmount()*2));
                }
        }
    }
}
