package com.ladestitute.bloodandgold.util.events.weapons;

import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.entity.Entity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.Direction;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BaGRapiersHandler {

    public double dashrange = 1D;

    @SubscribeEvent
    public void lungedamagebonus(LivingHurtEvent event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);

        if (swordstack.getItem() == ItemInit.GLASS_RAPIER.get()||
                swordstack.getItem() == ItemInit.TITANIUM_RAPIER.get()||
                swordstack.getItem() == ItemInit.BLOOD_RAPIER.get()||
                swordstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get()
                ||swordstack.getItem() == ItemInit.GOLD_RAPIER.get())
        {
            Entity sourceEntity = event.getSource().getDirectEntity();
            if(sourceEntity.getDeltaMovement().y != 0 || sourceEntity.getDeltaMovement().z != 0) {
                event.setAmount(event.getAmount() * 2);
            }
        }
    }

    @SubscribeEvent
    public void rapierattacklunge(AttackEntityEvent event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        if (swordstack.getItem() == ItemInit.GLASS_RAPIER.get()||
                swordstack.getItem() == ItemInit.TITANIUM_RAPIER.get()||
                swordstack.getItem() == ItemInit.BLOOD_RAPIER.get()||
                swordstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get()
                ||swordstack.getItem() == ItemInit.GOLD_RAPIER.get()) {
            if (event.getEntityLiving().isOnGround()) {
                if (BaGConfigHandler.getInstance().rapiersperformlunges()) {
                    if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                        event.getEntityLiving().setDeltaMovement(0, 0, dashrange);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                        event.getEntityLiving().setDeltaMovement(0, 0, -dashrange);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.WEST) {
                        event.getEntityLiving().setDeltaMovement(-dashrange, 0, 0D);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.EAST) {
                        event.getEntityLiving().setDeltaMovement(dashrange, 0, 0D);
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void rapierlunge(PlayerInteractEvent.LeftClickBlock event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        if (swordstack.getItem() == ItemInit.GLASS_RAPIER.get()||
                swordstack.getItem() == ItemInit.TITANIUM_RAPIER.get()||
                swordstack.getItem() == ItemInit.BLOOD_RAPIER.get()||
                swordstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get()
                ||swordstack.getItem() == ItemInit.GOLD_RAPIER.get()) {
            if(BaGConfigHandler.getInstance().rapiersperformlunges()) {
                if (event.getEntityLiving().isOnGround()) {
                    if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                        event.getEntityLiving().setDeltaMovement(0, 0, dashrange);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                        event.getEntityLiving().setDeltaMovement(0, 0, -dashrange);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.WEST) {
                        event.getEntityLiving().setDeltaMovement(-dashrange, 0, 0D);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.EAST) {
                        event.getEntityLiving().setDeltaMovement(dashrange, 0, 0D);
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void emptyrapierlunge(PlayerInteractEvent.LeftClickEmpty event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        if (swordstack.getItem() == ItemInit.GLASS_RAPIER.get()||
                swordstack.getItem() == ItemInit.TITANIUM_RAPIER.get()||
                swordstack.getItem() == ItemInit.BLOOD_RAPIER.get()||
                swordstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get()
                ||swordstack.getItem() == ItemInit.GOLD_RAPIER.get()) {
            if(event.getEntityLiving().isOnGround()) {
                if (BaGConfigHandler.getInstance().rapiersperformlunges()) {
                    if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                        event.getEntityLiving().setDeltaMovement(0, 0, dashrange);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                        event.getEntityLiving().setDeltaMovement(0, 0, -dashrange);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.WEST) {
                        event.getEntityLiving().setDeltaMovement(-dashrange, 0, 0D);
                    }
                    if (event.getEntityLiving().getDirection() == Direction.EAST) {
                        event.getEntityLiving().setDeltaMovement(dashrange, 0, 0D);
                    }
                }
            }
        }
    }
}
