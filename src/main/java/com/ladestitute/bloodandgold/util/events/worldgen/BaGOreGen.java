package com.ladestitute.bloodandgold.util.events.worldgen;

import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import net.minecraft.block.BlockState;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.template.RuleTest;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;

public class BaGOreGen {
    protected static ConfiguredFeature<?, ?> SHRINE_OF_RHYTHM;
    protected static ConfiguredFeature<?, ?> SHRINE_OF_PEACE;
    protected static ConfiguredFeature<?, ?> SHRINE_OF_BLOOD;
    protected static ConfiguredFeature<?, ?> SHRINE_OF_GLASS;
    protected static ConfiguredFeature<?, ?> SHRINE_OF_PAIN;
    protected static ConfiguredFeature<?, ?> SHRINE_OF_SPACE;
    protected static ConfiguredFeature<?, ?> SHRINE_OF_SACRIFICE;
    protected static ConfiguredFeature<?, ?> SHRINE_OF_DARKNESS;

    public static void register()
    {
        Registry<ConfiguredFeature<?, ?>> registry = WorldGenRegistries.CONFIGURED_FEATURE;

        SHRINE_OF_RHYTHM = Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                        SpecialBlocksInit.SHRINE_OF_RHYTHM_UNACTIVATED.get().defaultBlockState(),
                        3)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(4,
                0, 48)).squared().count(10));
        SHRINE_OF_PEACE = Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                SpecialBlocksInit.SHRINE_OF_PEACE_UNACTIVATED.get().defaultBlockState(),
                3)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(4,
                0, 48)).squared().count(10));
        SHRINE_OF_BLOOD = Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                SpecialBlocksInit.SHRINE_OF_BLOOD_UNACTIVATED.get().defaultBlockState(),
                3)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(4,
                0, 48)).squared().count(10));
        SHRINE_OF_DARKNESS = Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                SpecialBlocksInit.SHRINE_OF_DARKNESS_UNACTIVATED.get().defaultBlockState(),
                3)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(4,
                0, 48)).squared().count(10));
        SHRINE_OF_SACRIFICE = Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get().defaultBlockState(),
                3)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(4,
                0, 48)).squared().count(10));
        SHRINE_OF_SPACE = Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                SpecialBlocksInit.SHRINE_OF_SPACE_UNACTIVATED.get().defaultBlockState(),
                3)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(4,
                0, 48)).squared().count(10));
        SHRINE_OF_PAIN = Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get().defaultBlockState(),
                3)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(4,
                0, 48)).squared().count(10));
        SHRINE_OF_GLASS = Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                SpecialBlocksInit.SHRINE_OF_GLASS_UNACTIVATED.get().defaultBlockState(),
                3)).decorated(Placement.RANGE.configured(new TopSolidRangeConfig(4,
                0, 48)).squared().count(10));
        //anything smaller than 3 for vein size will not allow ore to generate
        //vein size, min height, 0, max height, tries per chunk

        Registry.register(registry, "shrine_of_rhythm", SHRINE_OF_RHYTHM);
        Registry.register(registry, "shrine_of_peace", SHRINE_OF_PEACE);
        Registry.register(registry, "shrine_of_blood", SHRINE_OF_BLOOD);
        Registry.register(registry, "shrine_of_darkness", SHRINE_OF_DARKNESS);
        Registry.register(registry, "shrine_of_sacrifice", SHRINE_OF_SACRIFICE);
        Registry.register(registry, "shrine_of_space", SHRINE_OF_SPACE);
        Registry.register(registry, "shrine_of_pain", SHRINE_OF_PAIN);
        Registry.register(registry, "shrine_of_glass", SHRINE_OF_GLASS);
    }

    public static void oregen(BiomeLoadingEvent event)
    {
        if (event.getCategory() != Biome.Category.THEEND && event.getCategory() != Biome.Category.NETHER)
        {
            event.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, BaGOreGen.SHRINE_OF_RHYTHM);
            event.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, BaGOreGen.SHRINE_OF_PEACE);
            event.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, BaGOreGen.SHRINE_OF_BLOOD);
            event.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, BaGOreGen.SHRINE_OF_DARKNESS);
            event.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, BaGOreGen.SHRINE_OF_SACRIFICE);
            event.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, BaGOreGen.SHRINE_OF_SPACE);
            event.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, BaGOreGen.SHRINE_OF_PAIN);
            event.getGeneration().addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, BaGOreGen.SHRINE_OF_GLASS);
        }
    }

}

