package com.ladestitute.bloodandgold.util.events.materials;

import com.ladestitute.bloodandgold.registries.EffectInit;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.blood.BaGBloodCapabilityProvider;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import com.ladestitute.bloodandgold.util.bagcaps.rhythm.BaGRhythmCapabilityProvider;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;

public class BaGObsidianWeaponsHandler {
    public int movetimer;

    @SubscribeEvent
    public void rhythmlevelistener(TickEvent.PlayerTickEvent event) {
        event.player.getCapability(BaGRhythmCapabilityProvider.BAGRHYTHM).ifPresent(h ->
        {
            if (h.getRhythmLevel() >= 4) {
                h.setRhythmLevel(3);
            }
            if (h.getRhythmLevel() < 1) {
                h.setRhythmLevel(1);
            }
            if(h.getRhythmKills() > 0 && h.getRhythmKills() <= 4)
            {
                h.increaseRhythmLevel(1);
            }
            if(h.getRhythmKills() >= 5)
            {
                h.increaseRhythmLevel(1);
               // System.out.println("Rhythm level is at max!");
            }

            ItemStack swordstack = event.player.getItemBySlot(EquipmentSlotType.MAINHAND);
            if (swordstack.getItem() == ItemInit.OBSIDIAN_SWORD.get() || swordstack.getItem() == ItemInit.OBSIDIAN_DAGGER.get() ||
                    swordstack.getItem() == ItemInit.OBSIDIAN_SPEAR.get() || swordstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get()
                    || swordstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get() || swordstack.getItem() == ItemInit.OBSIDIAN_BOW.get()
                    || swordstack.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get()) {
                event.player.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(e ->
                {
                    e.preventityxposition(event.player.blockPosition().getX());
                    e.preventityyposition(event.player.blockPosition().getY());
                    e.preventityzposition(event.player.blockPosition().getZ());

                        if (event.player.blockPosition().getX() == e.getpreventityxposition() &&
                                event.player.blockPosition().getY() == e.getpreventityyposition() &&
                                event.player.blockPosition().getZ() == e.getpreventityzposition()) {
                            movetimer++;
                        }

                        if (event.player.getSpeed() >= 0.13) {
                            movetimer = 0;
                        }

                        //  System.out.println("Player speed is " + event.player.getSpeed());

                    if (movetimer >= BaGConfigHandler.getInstance().mintimenotmovingtoresetrhythmlevel()) {
                        h.decreaseRhythmLevel(1);
                        h.setRhythmKills(0);
                        //  System.out.println("Rhythm reset.");
                        movetimer = 0;
                    }


                });
            }
        });
    }

    @SubscribeEvent
    public void killlistener(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGRhythmCapabilityProvider.BAGRHYTHM).ifPresent(h ->
            {
                {
                    for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                        if (held.getItem() == ItemInit.OBSIDIAN_FLAIL.get() ||
                                held.getItem() == ItemInit.OBSIDIAN_BOW.get() ||
                                held.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get() ||
                                held.getItem() == ItemInit.OBSIDIAN_DAGGER.get() ||
                                held.getItem() == ItemInit.OBSIDIAN_SPEAR.get() ||
                                held.getItem() == ItemInit.OBSIDIAN_RAPIER.get() ||
                                held.getItem() == ItemInit.OBSIDIAN_SWORD.get()) {
                            h.addRhythmKills(1);
                        }
                    }
                }
            });
    }

    @SubscribeEvent
    public void blooddamagebonus(LivingHurtEvent event) {
        Entity sourceEntity = event.getSource().getEntity();

        if (sourceEntity instanceof PlayerEntity) {
            for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                if (held.getItem() == ItemInit.OBSIDIAN_FLAIL.get() ||
                        held.getItem() == ItemInit.OBSIDIAN_BOW.get() ||
                        held.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get() ||
                        held.getItem() == ItemInit.OBSIDIAN_DAGGER.get() ||
                        held.getItem() == ItemInit.OBSIDIAN_SPEAR.get() ||
                        held.getItem() == ItemInit.OBSIDIAN_RAPIER.get() ||
                        held.getItem() == ItemInit.OBSIDIAN_SWORD.get())
                {
                    sourceEntity.getCapability(BaGRhythmCapabilityProvider.BAGRHYTHM).ifPresent(h ->
                    {
                        if(h.getRhythmLevel() == 1)
                        {
                            event.setAmount(event.getAmount());
                        }
                        if(h.getRhythmLevel() == 2)
                        {
                            event.setAmount(event.getAmount()+1F);
                        }
                        if(h.getRhythmLevel() == 3)
                        {
                            event.setAmount(event.getAmount()+2F);
                        }
                });
                }
                //  PlayerEntity player = (PlayerEntity)event.getEntityLiving();
            }
        }

    }

    @SubscribeEvent
    public void damagerducingrhythmlevel(LivingDamageEvent event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
        Entity sourceEntity = event.getSource().getDirectEntity();
        if (event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.OBSIDIAN_SWORD.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.OBSIDIAN_DAGGER.get() ||
                event.getEntityLiving() instanceof PlayerEntity
                        && swordstack.getItem() == ItemInit.OBSIDIAN_SPEAR.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.OBSIDIAN_BOW.get() || event.getEntityLiving() instanceof PlayerEntity
                && swordstack.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get())
        {
            event.getEntityLiving().getCapability(BaGRhythmCapabilityProvider.BAGRHYTHM).ifPresent(h ->
            {
            if(BaGConfigHandler.getInstance().rangedmgresetsrhythmlevel()
                    && sourceEntity instanceof AbstractArrowEntity)
            {
                h.decreaseRhythmLevel(1);
                h.setRhythmKills(0);
            }
            if (BaGConfigHandler.getInstance().environmentaldmgresetsrhythmlevel()) {
                if (event.getSource() == DamageSource.CACTUS || event.getSource() == DamageSource.ANVIL ||
                        event.getSource() == DamageSource.CRAMMING || event.getSource() == DamageSource.DROWN ||
                        event.getSource() == DamageSource.FALL || event.getSource() == DamageSource.FALLING_BLOCK ||
                        event.getSource() == DamageSource.HOT_FLOOR || event.getSource() == DamageSource.IN_FIRE ||
                        event.getSource() == DamageSource.LAVA || event.getSource() == DamageSource.LIGHTNING_BOLT ||
                        event.getSource() == DamageSource.SWEET_BERRY_BUSH || event.getSource() == DamageSource.ON_FIRE) {
                    h.decreaseRhythmLevel(1);
                    h.setRhythmKills(0);
                }
            }

                if (event.getSource() != DamageSource.CACTUS & event.getSource() != DamageSource.ANVIL &
                        event.getSource() != DamageSource.CRAMMING & event.getSource() != DamageSource.DROWN &
                        event.getSource() != DamageSource.FALL & event.getSource() != DamageSource.FALLING_BLOCK &
                        event.getSource() != DamageSource.HOT_FLOOR & event.getSource() != DamageSource.IN_FIRE &
                        event.getSource() != DamageSource.LAVA & event.getSource() != DamageSource.LIGHTNING_BOLT &
                        event.getSource() != DamageSource.SWEET_BERRY_BUSH & event.getSource() != DamageSource.ON_FIRE) {
                    h.decreaseRhythmLevel(1);
                    h.setRhythmKills(0);
                }
            });

        }
    }

    @SubscribeEvent
    public void glassarmoreffect(LivingDamageEvent event) {
        ItemStack armorstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.CHEST);
        Entity sourceEntity = event.getSource().getDirectEntity();
        if (event.getEntityLiving() instanceof PlayerEntity
                && armorstack.getItem() == ItemInit.OBSIDIAN_ARMOR.get()) {
            event.getEntityLiving().getCapability(BaGRhythmCapabilityProvider.BAGRHYTHM).ifPresent(h ->
            {
            if (sourceEntity instanceof AbstractArrowEntity) {
                if(h.getRhythmLevel() == 1)
                {
                    event.setAmount((float) (event.getAmount()*0.85));
                }
                if(h.getRhythmLevel() == 2)
                {
                    event.setAmount((float) (event.getAmount()*0.65));
                }
                if(h.getRhythmLevel() == 3)
                {
                    event.setAmount((float) (event.getAmount()*0.52));
                }
            }
            if (event.getSource() == DamageSource.CACTUS || event.getSource() == DamageSource.ANVIL ||
                    event.getSource() == DamageSource.CRAMMING || event.getSource() == DamageSource.DROWN ||
                    event.getSource() == DamageSource.FALL || event.getSource() == DamageSource.FALLING_BLOCK ||
                    event.getSource() == DamageSource.HOT_FLOOR || event.getSource() == DamageSource.IN_FIRE ||
                    event.getSource() == DamageSource.LAVA || event.getSource() == DamageSource.LIGHTNING_BOLT ||
                    event.getSource() == DamageSource.SWEET_BERRY_BUSH || event.getSource() == DamageSource.ON_FIRE) {
                if(h.getRhythmLevel() == 1)
                {
                    event.setAmount((float) (event.getAmount()*0.85));
                }
                if(h.getRhythmLevel() == 2)
                {
                    event.setAmount((float) (event.getAmount()*0.65));
                }
                if(h.getRhythmLevel() == 3)
                {
                    event.setAmount((float) (event.getAmount()*0.52));
                }
            }

            if (event.getSource() != DamageSource.CACTUS & event.getSource() != DamageSource.ANVIL &
                    event.getSource() != DamageSource.CRAMMING & event.getSource() != DamageSource.DROWN &
                    event.getSource() != DamageSource.FALL & event.getSource() != DamageSource.FALLING_BLOCK &
                    event.getSource() != DamageSource.HOT_FLOOR & event.getSource() != DamageSource.IN_FIRE &
                    event.getSource() != DamageSource.LAVA & event.getSource() != DamageSource.LIGHTNING_BOLT &
                    event.getSource() != DamageSource.SWEET_BERRY_BUSH & event.getSource() != DamageSource.ON_FIRE) {
                if(h.getRhythmLevel() == 1)
                {
                    event.setAmount((float) (event.getAmount()*0.85));
                }
                if(h.getRhythmLevel() == 2)
                {
                    event.setAmount((float) (event.getAmount()*0.65));
                }
                if(h.getRhythmLevel() == 3)
                {
                    event.setAmount((float) (event.getAmount()*0.52));
                }
            }
            });
        }
    }
}
