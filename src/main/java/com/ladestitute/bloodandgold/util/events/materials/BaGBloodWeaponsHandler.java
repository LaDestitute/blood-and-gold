package com.ladestitute.bloodandgold.util.events.materials;

import com.ladestitute.bloodandgold.blocks.shrines.ShrineOfSacrificeUnactivatedBlock;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import com.ladestitute.bloodandgold.util.bagcaps.blood.BaGBloodCapabilityProvider;
import com.ladestitute.bloodandgold.util.config.BaGConfigHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.OreBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.BlazeEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Objects;

public class BaGBloodWeaponsHandler {

    @SubscribeEvent
    public void blooddigability(PlayerInteractEvent.RightClickBlock event) {
        ItemStack shovelstack = event.getPlayer().getItemBySlot(EquipmentSlotType.MAINHAND);
        if (event.getEntityLiving() instanceof PlayerEntity
                && shovelstack.getItem() == ItemInit.BLOOD_SHOVEL.get() && event.getEntityLiving().isOnGround()) {
            BlockPos blockPos = event.getEntityLiving().blockPosition();
            int searchRadius = 1;
            int radius = (int) (Math.floor(0.5) + searchRadius);

            for (int x = -radius; x <= radius; ++x) {
                for (int y = -1; y <= 1; ++y) {
                    for (int z = -radius; z <= radius; ++z) {
                        BlockPos shrinePos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                        BlockState blockState = event.getEntityLiving().level.getBlockState(shrinePos);
                        Block block = blockState.getBlock();
                        //  BlockPos sacriPos = blockState.getBlock().defaultBlockState().;
                        // BlockPos sacrificePos = new BlockPos(block.defaultBlockState().getBlock().);
                        if (block != Blocks.BARRIER || block != Blocks.BEDROCK ||
                                block != Blocks.COMMAND_BLOCK || block != Blocks.END_GATEWAY||
                                block != Blocks.END_PORTAL||block != Blocks.END_PORTAL_FRAME||
                                block != Blocks.JIGSAW||block != Blocks.NETHER_PORTAL||
                                block != Blocks.STRUCTURE_BLOCK||block != Blocks.STRUCTURE_VOID||
                                block != Blocks.LAVA|| block != Blocks.WATER ||
                                block != SpecialBlocksInit.SHRINE_OF_BLOOD_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_DARKNESS_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_GLASS_UNACTIVATED.get()||
                                block != SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()||
                                block != SpecialBlocksInit.SHRINE_OF_PEACE_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_RHYTHM_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_SPACE_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_BLOOD_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_DARKNESS_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_GLASS_ACTIVATED.get()||
                                block != SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get()||
                                block != SpecialBlocksInit.SHRINE_OF_PEACE_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_RHYTHM_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_SPACE_ACTIVATED.get())
                        {
                            if (block == Blocks.STONE || block == Blocks.DIRT || block == Blocks.COBBLESTONE ||
                                    block == Blocks.GRASS_BLOCK || block == Blocks.SNOW || block == Blocks.SNOW_BLOCK ||
                                    block == Blocks.ANDESITE || block == Blocks.BRICKS
                                    || block == Blocks.CLAY || block == Blocks.COAL_ORE ||
                                    block == Blocks.COARSE_DIRT || block == Blocks.DIAMOND_ORE || block == Blocks.DIORITE ||
                                    block == Blocks.EMERALD_ORE || block == Blocks.GOLD_ORE || block == Blocks.GRANITE ||
                                    block == Blocks.GRAVEL || block == Blocks.ICE || block == Blocks.IRON_ORE ||
                                    block == Blocks.PODZOL || block == Blocks.SANDSTONE || block == Blocks.SAND ||
                                    block == Blocks.LAPIS_ORE || block == Blocks.STONE_BRICKS || block == Blocks.SMOOTH_STONE ||
                                    block == Blocks.SMOOTH_SANDSTONE || block == Blocks.SMOOTH_RED_SANDSTONE || block == Blocks.RED_SAND ||
                                    block == Blocks.RED_SANDSTONE || block == Blocks.REDSTONE_ORE ||
                                    block == Blocks.MOSSY_COBBLESTONE || block == Blocks.CHISELED_STONE_BRICKS || block == Blocks.MOSSY_STONE_BRICKS ||
                                    block == Blocks.CRACKED_STONE_BRICKS || block == Blocks.MYCELIUM ||
                                    block == Blocks.ACACIA_LOG || block == Blocks.ACACIA_PLANKS || block == Blocks.ACACIA_WOOD ||
                                    block == Blocks.BIRCH_LOG || block == Blocks.BIRCH_PLANKS || block == Blocks.BIRCH_WOOD ||
                                    block == Blocks.DARK_OAK_LOG || block == Blocks.DARK_OAK_PLANKS || block == Blocks.DARK_OAK_WOOD ||
                                    block == Blocks.JUNGLE_LOG || block == Blocks.JUNGLE_PLANKS || block == Blocks.JUNGLE_WOOD ||
                                    block == Blocks.OAK_LOG || block == Blocks.OAK_PLANKS || block == Blocks.OAK_WOOD ||
                                    block == Blocks.SPRUCE_LOG || block == Blocks.SPRUCE_PLANKS || block == Blocks.SPRUCE_WOOD ||
                                    block == Blocks.BLUE_ICE || block == Blocks.CHISELED_RED_SANDSTONE || block == Blocks.CHISELED_SANDSTONE ||
                                    block == Blocks.FROSTED_ICE || block == Blocks.GRASS_PATH || block == Blocks.TERRACOTTA ||
                                    block == Blocks.RED_TERRACOTTA || block == Blocks.ORANGE_TERRACOTTA || block == Blocks.YELLOW_TERRACOTTA ||
                                    block == Blocks.BROWN_TERRACOTTA || block == Blocks.WHITE_TERRACOTTA ||
                                    block == Blocks.LIGHT_GRAY_TERRACOTTA || block == Blocks.LIME_TERRACOTTA || block == Blocks.BLUE_TERRACOTTA) {
                                event.getPlayer().level.destroyBlock(shrinePos, true);
                                event.getPlayer().hurt(DamageSource.GENERIC, 1F);
                            }
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void killlistener(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGBloodCapabilityProvider.BAGBLOOD).ifPresent(h ->
            {
                {
                    for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                        if (held.getItem() == ItemInit.BLOOD_FLAIL.get() ||
                                held.getItem() == ItemInit.BLOOD_BOW.get() ||
                                held.getItem() == ItemInit.BLOOD_CROSSBOW.get() ||
                                held.getItem() == ItemInit.BLOOD_DAGGER.get() ||
                                held.getItem() == ItemInit.BLOOD_SPEAR.get() ||
                                held.getItem() == ItemInit.BLOOD_RAPIER.get() ||
                                held.getItem() == ItemInit.BLOOD_SWORD.get()) {
                            h.addKills(+1);
                            System.out.println("Kills is now" + h.getKills());
                        }
                    }
                }
            });
    }


    @SubscribeEvent
    public void bloodhealinglistener(TickEvent.PlayerTickEvent event) {
        event.player.getCapability(BaGBloodCapabilityProvider.BAGBLOOD).ifPresent(h ->
        {
            if (h.getKills() >= BaGConfigHandler.getInstance().bloodkillsrequirement()) {
                h.setKills(0);
                h.setBlood(0);
                event.player.addEffect(new EffectInstance(Effects.HEAL, 50, 1));
            }
        });

    }

    @SubscribeEvent
    public void blooddamagebonus(LivingHurtEvent event) {
        Entity sourceEntity = event.getSource().getEntity();

        if (sourceEntity instanceof PlayerEntity && ((PlayerEntity) sourceEntity).getHealth() <= 3.33F) {
            for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                if (held.getItem() == ItemInit.BLOOD_FLAIL.get() ||
                        held.getItem() == ItemInit.BLOOD_BOW.get() ||
                        held.getItem() == ItemInit.BLOOD_CROSSBOW.get() ||
                        held.getItem() == ItemInit.BLOOD_DAGGER.get() ||
                        held.getItem() == ItemInit.BLOOD_SPEAR.get() ||
                        held.getItem() == ItemInit.BLOOD_RAPIER.get() ||
                        held.getItem() == ItemInit.BLOOD_SWORD.get())
                {
                    if(BaGConfigHandler.getInstance().bloodspecialeffectmultiplier() == 1) {
                        event.setAmount((float) (event.getAmount()*1.5));
                        System.out.println("Damage buffed" + event.getAmount());
                    }
                    if(BaGConfigHandler.getInstance().bloodspecialeffectmultiplier() == 2) {
                        event.setAmount((float) (event.getAmount()*2.25));
                        System.out.println("Damage buffed" + event.getAmount());
                    }
                    if(BaGConfigHandler.getInstance().bloodspecialeffectmultiplier() == 3) {
                        event.setAmount((float) (event.getAmount()*3));
                        System.out.println("Damage buffed" + event.getAmount());
                    }
                }
                //  PlayerEntity player = (PlayerEntity)event.getEntityLiving();
            }
        }

    }
}

