package com.ladestitute.bloodandgold.util;

import com.ladestitute.bloodandgold.BaGMain;
import net.minecraft.block.Blocks;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;

public enum BaGArmorTypes implements IArmorMaterial
{
   GLASS("glass", 1, new int[] {20, 20, 20, 20}, 5, Blocks.AIR.asItem(),
            SoundEvents.ARMOR_EQUIP_DIAMOND, 8.0f, 0.0f),
    OBSIDIAN("obsidian", 560, new int[] {0, 0, 0, 0}, 10, Blocks.AIR.asItem(),
            SoundEvents.ARMOR_EQUIP_NETHERITE, 0.0f, 0.0f),
    GI("gi", 384, new int[] {3, 3, 3, 3}, 0, Blocks.AIR.asItem(),
            SoundEvents.ARMOR_EQUIP_LEATHER, 0.0f, 0.0f);

    private String name;
    private final SoundEvent soundEvent;
    private int durability, enchantability;
    private int[] dmgReductionAmounts;
    private float toughness, knockbackResistance;
    private Item repairItem;
    private static final int[] max_damage_array = new int[] {13, 15, 16, 11};

    private BaGArmorTypes(String name, int durability, int[]dmgReductionAmounts, int enchantability, Item repairItem, SoundEvent soundEvent, float toughness, float knockbackResistance)
    {
        this.name = name;
        this.soundEvent = soundEvent;
        this.durability = durability; //helmet*11, chest*16, leg*15, boots*13   //Dia 33, Iron 15, Gold 7, Leather 5
        this.enchantability = enchantability; //Leather 15, Chain 12, Iron 9, Gold 25, Dia 10, 1.16Netherite 15
        this.dmgReductionAmounts = dmgReductionAmounts; //{Boots, Leg, Chest, Helmet}
        this.toughness = toughness; //Dia 2, 1.16Netherite 3
        this.repairItem = repairItem;
        this.knockbackResistance = knockbackResistance;

    }

    @Override
    public int getDurabilityForSlot(EquipmentSlotType slot)
    {
        return max_damage_array[slot.getIndex()] * this.durability;
    }

    @Override
    public int getDefenseForSlot(EquipmentSlotType slot)
    {
        return this.dmgReductionAmounts[slot.getIndex()];
    }

    @Override
    public int getEnchantmentValue()
    {
        return this.enchantability;
    }

    @Override
    public SoundEvent getEquipSound() {
        return this.soundEvent;
    }

    @Override
    public Ingredient getRepairIngredient()
    {
        return Ingredient.of(this.repairItem);
    }

    @Override
    public String getName()
    {
        return BaGMain.MOD_ID + ":" + this.name;
    }

    @Override
    public float getToughness()
    {
        return this.toughness;
    }

    @Override
    public float getKnockbackResistance()
    {
        return this.knockbackResistance;
    }
}
