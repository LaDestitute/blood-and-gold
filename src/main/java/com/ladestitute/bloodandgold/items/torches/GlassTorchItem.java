package com.ladestitute.bloodandgold.items.torches;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ItemInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class GlassTorchItem extends Item implements ICurioItem {

    public GlassTorchItem(Properties properties) {
        super(properties);
    }

    @SubscribeEvent
    public static void addglowing(LivingEvent.LivingUpdateEvent event)
    {
        ItemStack stack =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.GLASS_TORCH.get(), event.getEntityLiving()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        double radius = 12;
        AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().blockPosition()).inflate(radius).expandTowards(0.0D, 16, 0.0D);
        List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
        if (!stack.isEmpty()) {
            for (LivingEntity livingEntity : list) {
                if (livingEntity instanceof MonsterEntity) {
                    livingEntity.removeEffect(Effects.INVISIBILITY);
                    livingEntity.addEffect(new EffectInstance(Effects.GLOWING, 6000, 0));
                }
            }
        }
    }

    @SubscribeEvent
    public static void onLivingHurt(LivingHurtEvent event) {
        //Check if it is the Player who takes damage.
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();

            //Get the Ring as an ItemStack
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.GLASS_TORCH.get(), player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);


                //Check if player is wearing it.
                if (!stack.isEmpty()) {
                    //Don't do a Check to see if the damage comes from DamageSource.GENERIC. I don't know what mob/block uses the "GENERIC" damage in the game so I normally do a (event.getSource != DamageSource.*Type*) if I don't want it to take less damage from something in particular.
                    event.setAmount(0);
                    stack.shrink(stack.getCount());
                }
        }
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        return new ICurio.SoundInfo(SoundEvents.ARMOR_EQUIP_IRON, 1.0f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Magic vision, breakable"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

