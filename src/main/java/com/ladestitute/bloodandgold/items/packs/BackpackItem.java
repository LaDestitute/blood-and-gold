package com.ladestitute.bloodandgold.items.packs;

import com.ladestitute.bloodandgold.items.packs.container.Holster;
import com.ladestitute.bloodandgold.registries.ItemInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class BackpackItem extends Holster
{
    public BackpackItem(Properties properties)
    {
        super(properties);
    }

    @Override
    public boolean canHoldItem(ItemStack stack)
    {
        Item item = stack.getItem();
        return item == Items.APPLE || item == Items.BAKED_POTATO || item == Items.COOKED_BEEF || item == Items.BEETROOT
                || item == Items.BEETROOT_SOUP || item == Items.BREAD || item == Items.CAKE
                || item == Items.CARROT || item == Items.COOKED_CHICKEN ||
                item == Items.COCOA_BEANS || item == Items.COOKED_COD ||
                item == Items.COOKIE || item == Items.COOKED_MUTTON ||
                item == Items.COOKED_PORKCHOP || item == Items.COOKED_RABBIT ||
                item == Items.COOKED_SALMON || item == Items.DRIED_KELP ||
                item == Items.HONEY_BOTTLE || item == Items.MELON_SLICE ||
                item == Items.MUSHROOM_STEW || item == Items.POTATO ||
                item == Items.PUMPKIN_PIE || item == Items.RABBIT_STEW ||
                item == Items.SUSPICIOUS_STEW || item == Items.SWEET_BERRIES ||
                item == Items.CHORUS_FRUIT || item == Items.POPPED_CHORUS_FRUIT ||
                item == Items.GOLDEN_APPLE || item == Items.GOLDEN_CARROT ||
                item == Items.ENCHANTED_GOLDEN_APPLE;
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag) {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent("+1 Food Storage"));
    }
}