package com.ladestitute.bloodandgold.items.packs.container;

import com.ladestitute.bloodandgold.container.BackpackContainer;
import com.ladestitute.bloodandgold.container.HolsterContainer;
import com.ladestitute.bloodandgold.items.BagItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.util.Hand;

public abstract class Backpack extends BagItem
{
    public Backpack(Properties properties)
    {
        super(properties);
    }

    @Override
    public Container getContainer(int windowId, PlayerInventory playerInventory, PlayerEntity player, Hand hand)
    {
        return new BackpackContainer(windowId, player.inventory, hand);
    }

}
