package com.ladestitute.bloodandgold.items.packs;

import com.ladestitute.bloodandgold.items.packs.container.Holster;
import com.ladestitute.bloodandgold.registries.ItemInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class HolsterItem extends Holster
{
    public HolsterItem(Properties properties)
    {
        super(properties);
    }

        @Override
    public boolean canHoldItem(ItemStack stack)
    {
        Item item = stack.getItem();
        return item == Items.DIAMOND_SWORD || item == Items.GOLDEN_SWORD || item == Items.IRON_SWORD || item == Items.NETHERITE_SWORD
                || item == Items.BOW || item == Items.CROSSBOW || item == Items.TRIDENT
                || item == Items.STONE_SWORD || item == Items.WOODEN_SWORD ||
                item == ItemInit.GOLD_BOW.get() ||
                item == ItemInit.GOLD_CROSSBOW.get() ||
                item == ItemInit.GOLD_RAPIER.get() ||
                item == ItemInit.GOLD_DAGGER.get() ||
                item == ItemInit.GOLD_FLAIL.get() ||
                item == ItemInit.GOLD_SPEAR.get() ||
                item == ItemInit.BLOOD_SWORD.get() ||
                item == ItemInit.BLOOD_BOW.get() ||
                item == ItemInit.BLOOD_CROSSBOW.get() ||
                item == ItemInit.BLOOD_RAPIER.get() ||
                item == ItemInit.BLOOD_DAGGER.get() ||
                item == ItemInit.BLOOD_FLAIL.get() ||
                item == ItemInit.BLOOD_SPEAR.get() ||
                item == ItemInit.GLASS_SWORD.get() ||
                item == ItemInit.GLASS_CROSSBOW.get()||
                item == ItemInit.GLASS_BOW.get() ||
                item == ItemInit.GLASS_RAPIER.get() ||
                item == ItemInit.GLASS_DAGGER.get() ||
                item == ItemInit.GLASS_FLAIL.get() ||
                item == ItemInit.GLASS_SPEAR.get() ||
                item == ItemInit.OBSIDIAN_SWORD.get() ||
                item == ItemInit.OBSIDIAN_CROSSBOW.get()||
                item == ItemInit.OBSIDIAN_BOW.get() ||
                item == ItemInit.OBSIDIAN_RAPIER.get() ||
                item == ItemInit.OBSIDIAN_DAGGER.get() ||
                item == ItemInit.OBSIDIAN_FLAIL.get() ||
                item == ItemInit.OBSIDIAN_SPEAR.get() ||
                item == ItemInit.TITANIUM_SWORD.get() ||
                item == ItemInit.TITANIUM_CROSSBOW.get()||
                item == ItemInit.TITANIUM_BOW.get() ||
                item == ItemInit.TITANIUM_RAPIER.get() ||
                item == ItemInit.TITANIUM_DAGGER.get() ||
                item == ItemInit.TITANIUM_FLAIL.get() ||
                item == ItemInit.TITANIUM_SPEAR.get() ||
                item == ItemInit.DAGGER.get() ||
                item == ItemInit.GLASS_SHARD.get() ||
                item == ItemInit.DAGGER_OF_FROST.get() ||
                item == ItemInit.JEWELED_DAGGER.get();
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag) {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent("Hold 2nd Weapon"));
    }
}
