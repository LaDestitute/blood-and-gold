package com.ladestitute.bloodandgold.items.rings;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class RingOfShieldingItem extends Item implements ICurioItem {

    public RingOfShieldingItem(Properties builder) {
        super(builder);
    }

    @SubscribeEvent
    public static void onLivingHurt(LivingHurtEvent event) {
        //Check if it is the Player who takes damage.
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();

            //Get the Ring as an ItemStack
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_SHIELDING.get(), player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

            event.getEntityLiving().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                if(h.checksetshieldingringdestroyed() == 1)
                {
                    h.addshieldtime(1);
                }
                if(h.checksetshieldingringdestroyed() == 1 && h.checkshieldtime() < 200)
                {
                    event.setAmount(0);
                }
                if(h.checkshieldtime() >= 200)
                {
                    h.setshieldingringdestroyed(0);
                    h.setshieldtime(0);
                }
            //Check if player is wearing it.
            if (!stack.isEmpty()) {
                //Don't do a Check to see if the damage comes from DamageSource.GENERIC. I don't know what mob/block uses the "GENERIC" damage in the game so I normally do a (event.getSource != DamageSource.*Type*) if I don't want it to take less damage from something in particular.
                    event.setAmount(0);
                    event.getEntityLiving().addEffect(new EffectInstance(Effects.DAMAGE_RESISTANCE, 100, 4));
                    stack.shrink(1);
            }
            });
        }
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        return new ICurio.SoundInfo(SoundEvents.ARMOR_EQUIP_IRON, 1.0f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag) {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent("Automatic Shield"));
    }

}

