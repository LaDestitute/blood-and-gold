package com.ladestitute.bloodandgold.items.rings;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.blood.BaGBloodCapabilityProvider;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class RingOfCourageItem extends Item implements ICurioItem {

    public RingOfCourageItem(Properties builder) {
        super(builder);
    }

    @SubscribeEvent
    public void killlistener(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof PlayerEntity)
        {
            double dashrange = 1D;
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_COURAGE.get(), (LivingEntity) event.getSource().getEntity()).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if(!stack.isEmpty())
            {
                if (event.getSource().getEntity().getDirection() == Direction.SOUTH) {
                    event.getSource().getEntity().setDeltaMovement(0, 0, dashrange);
                    event.getEntityLiving().addEffect(new EffectInstance(Effects.DAMAGE_RESISTANCE, 20, 4));
                }
                if (event.getSource().getEntity().getDirection() == Direction.NORTH) {
                    event.getSource().getEntity().setDeltaMovement(0, 0, -dashrange);
                    event.getEntityLiving().addEffect(new EffectInstance(Effects.DAMAGE_RESISTANCE, 20, 4));
                }
                if (event.getSource().getEntity().getDirection() == Direction.WEST) {
                    event.getSource().getEntity().setDeltaMovement(-dashrange, 0, 0D);
                    event.getEntityLiving().addEffect(new EffectInstance(Effects.DAMAGE_RESISTANCE, 20, 4));
                }
                if (event.getSource().getEntity().getDirection() == Direction.EAST) {
                    event.getSource().getEntity().setDeltaMovement(dashrange, 0, 0D);
                    event.getEntityLiving().addEffect(new EffectInstance(Effects.DAMAGE_RESISTANCE, 20, 4));
                }
            }
        }
    }

    @SubscribeEvent
    public static void onLivingHurt(LivingHurtEvent event) {
        //Check if it is the Player who takes damage.
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();

            //Get the Ring as an ItemStack
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_COURAGE.get(), player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

            event.getEntityLiving().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                if(h.iscourageactive() == 1)
                {
                    h.addcouragetime(1);
                }
                if(h.iscourageactive() == 1 && h.checkcouragetime() < 20)
                {
                    event.setAmount(0);
                }
                if(h.checkshieldtime() >= 20)
                {
                    h.courageactive(0);
                    h.setcouragetime(0);
                }

            });
        }
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        return new ICurio.SoundInfo(SoundEvents.ARMOR_EQUIP_GENERIC, 1.0f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag) {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent("Kill Invulnerability + Dash"));
    }

}
