package com.ladestitute.bloodandgold.items.rings;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ItemInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.boss.WitherEntity;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.monster.piglin.PiglinBruteEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class RingOfPeaceItem extends Item implements ICurioItem {

    private static final UUID HEALTH_BUFF = UUID.fromString("59905a7c-3ee2-4d94-b545-34df954f802b");

    public RingOfPeaceItem(Properties builder) {
        super(builder);
    }

    @SubscribeEvent(priority = EventPriority.HIGH)
    public static void onSpecialSpawn(LivingSpawnEvent.SpecialSpawn event)
    {
        if (!event.getWorld().isClientSide() && (event.getWorld()).dimensionType().bedWorks()
                && !(event.getSpawnReason() == SpawnReason.SPAWN_EGG)
                && !(event.getSpawnReason() == SpawnReason.COMMAND))
        {
            Random rand = new Random();
            ItemStack ring_of_peace =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_PEACE.get(), event.getEntityLiving()).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            int despawnchance = rand.nextInt(101);
            if(!ring_of_peace.isEmpty() && despawnchance <= 24) {
                if(event.getEntity() instanceof HuskEntity ||event.getEntity() instanceof ZombieEntity||
                        event.getEntity() instanceof SkeletonEntity||event.getEntity() instanceof StrayEntity||
                        event.getEntity() instanceof WitchEntity||event.getEntity() instanceof DrownedEntity||
                        event.getEntity() instanceof PhantomEntity||event.getEntity() instanceof CreeperEntity) {
                    event.setCanceled(true);
                }
            }
        }
    }


    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(String identifier, ItemStack stack) {
        Multimap<Attribute, AttributeModifier> map = HashMultimap.create();
        map.put(Attributes.MAX_HEALTH, new AttributeModifier(HEALTH_BUFF, "Curio max health", 2f, AttributeModifier.Operation.ADDITION));
        return map;
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        return new ICurio.SoundInfo(SoundEvents.ARMOR_EQUIP_IRON, 1.0f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag) {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent("+1 Heart, Fewer Enemies"));
    }

}

