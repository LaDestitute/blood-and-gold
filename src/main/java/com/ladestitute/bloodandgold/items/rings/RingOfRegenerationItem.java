package com.ladestitute.bloodandgold.items.rings;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class RingOfRegenerationItem extends Item implements ICurioItem {

    public RingOfRegenerationItem(Properties builder) {
        super(builder);
    }

    @SubscribeEvent
    public static void ringtick(TickEvent.PlayerTickEvent event)
    {
        ItemStack stack =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_REGENERATION.get(), event.player).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

        //Check if player is wearing it.
        if (!stack.isEmpty()) {
            //Don't do a Check to see if the damage comes from DamageSource.GENERIC. I don't know what mob/block uses the "GENERIC" damage in the game so I normally do a (event.getSource != DamageSource.*Type*) if I don't want it to take less damage from something in particular.
            event.player.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                h.addregenringtime(1);
                if(h.checkregenringtime() >= 3000)
                {
                    event.player.addEffect(new EffectInstance(Effects.REGENERATION, 100, 0));
                    h.setregenrintrime(0);
                }
            });
        }
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        return new ICurio.SoundInfo(SoundEvents.ARMOR_EQUIP_IRON, 1.0f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag) {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent("Regen Every 2.5 Mins"));
    }

}


