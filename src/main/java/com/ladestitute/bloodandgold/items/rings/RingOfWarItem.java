package com.ladestitute.bloodandgold.items.rings;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ItemInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.boss.WitherEntity;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.monster.piglin.PiglinBruteEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class RingOfWarItem extends Item implements ICurioItem {

    private static final UUID ATTACK_BUFF = UUID.fromString("0464059f-c15e-4783-b1f0-8829cbefce42");
    private static final UUID KNOCKBACK_BUFF = UUID.fromString("af5386ee-a0d3-4e60-857c-a8f601dbee83");

    public RingOfWarItem(Properties builder) {
        super(builder);
    }

    @SubscribeEvent(priority = EventPriority.HIGH)
    public static void onSpecialSpawn(LivingSpawnEvent.SpecialSpawn event)
    {
        if (!event.getWorld().isClientSide() && (event.getWorld()).dimensionType().bedWorks()
                && !(event.getSpawnReason() == SpawnReason.SPAWN_EGG)
                && !(event.getSpawnReason() == SpawnReason.COMMAND))
        {
            Random rand = new Random();
            ItemStack ring_of_war =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_WAR.get(), event.getEntityLiving()).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            int despawnchance = rand.nextInt(101);
            int enemytype = rand.nextInt(8);
            ZombieEntity zombie = new ZombieEntity(EntityType.ZOMBIE, event.getEntity().level);
            SkeletonEntity skele = new SkeletonEntity(EntityType.SKELETON, event.getEntity().level);
            HuskEntity husk = new HuskEntity(EntityType.HUSK, event.getEntity().level);
            StrayEntity stray = new StrayEntity(EntityType.STRAY, event.getEntity().level);
            PhantomEntity phantom = new PhantomEntity(EntityType.PHANTOM, event.getEntity().level);
            WitchEntity witch = new WitchEntity(EntityType.WITCH, event.getEntity().level);
            CreeperEntity creeper = new CreeperEntity(EntityType.CREEPER, event.getEntity().level);
            DrownedEntity drowned = new DrownedEntity(EntityType.DROWNED, event.getEntity().level);
            if(!ring_of_war.isEmpty() && despawnchance <= 24) {
                {
                    if(enemytype == 0)
                    {
                        event.getWorld().addFreshEntity(zombie);
                    }
                    if(enemytype == 1)
                    {
                        event.getWorld().addFreshEntity(skele);
                    }
                    if(enemytype == 2)
                    {
                        event.getWorld().addFreshEntity(husk);
                    }
                    if(enemytype == 3)
                    {
                        event.getWorld().addFreshEntity(stray);
                    }
                    if(enemytype == 4)
                    {
                        event.getWorld().addFreshEntity(witch);
                    }
                    if(enemytype == 5)
                    {
                        event.getWorld().addFreshEntity(drowned);
                    }
                    if(enemytype == 6)
                    {
                        event.getWorld().addFreshEntity(phantom);
                    }
                    if(enemytype == 7)
                    {
                        event.getWorld().addFreshEntity(creeper);
                    }
                }
            }
            if(!ring_of_war.isEmpty() && event.getEntity() instanceof EnderDragonEntity) {
                ((EnderDragonEntity) event.getEntity()).addEffect(new EffectInstance(Effects.ABSORPTION, 6000, 25));
            }
            if(!ring_of_war.isEmpty() && event.getEntity() instanceof WitherEntity) {
                ((WitherEntity) event.getEntity()).addEffect(new EffectInstance(Effects.ABSORPTION, 6000, 36));
            }
            if(!ring_of_war.isEmpty() && event.getEntity() instanceof RavagerEntity) {
                ((RavagerEntity) event.getEntity()).addEffect(new EffectInstance(Effects.ABSORPTION, 6000, 12));
            }
            if(!ring_of_war.isEmpty() && event.getEntity() instanceof PiglinBruteEntity) {
                ((PiglinBruteEntity) event.getEntity()).addEffect(new EffectInstance(Effects.ABSORPTION, 6000, 5));
            }
            if(!ring_of_war.isEmpty() && event.getEntity() instanceof ElderGuardianEntity) {
                ((ElderGuardianEntity) event.getEntity()).addEffect(new EffectInstance(Effects.ABSORPTION, 6000, 10));
            }
            if(!ring_of_war.isEmpty() && event.getEntity() instanceof EvokerEntity) {
                ((EvokerEntity) event.getEntity()).addEffect(new EffectInstance(Effects.ABSORPTION, 6000, 2));
            }

        }
    }


    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(String identifier, ItemStack stack) {
        Multimap<Attribute, AttributeModifier> map = HashMultimap.create();
        map.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(ATTACK_BUFF,
                "Curio attack damage", 2f, AttributeModifier.Operation.ADDITION));
        map.put(Attributes.ATTACK_KNOCKBACK, new AttributeModifier(KNOCKBACK_BUFF,
                "Curio attack knockback", 0.5, AttributeModifier.Operation.ADDITION));
        return map;
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        return new ICurio.SoundInfo(SoundEvents.ARMOR_EQUIP_IRON, 1.0f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag) {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent("+4 Damage, Stronger Enemies"));
    }

}


