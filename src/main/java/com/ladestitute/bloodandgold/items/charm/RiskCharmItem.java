package com.ladestitute.bloodandgold.items.charm;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.registries.ItemInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class RiskCharmItem extends Item implements ICurioItem {

    public RiskCharmItem(Properties properties) {
        super(properties);
    }

    @SubscribeEvent
    public static void livingDamageEvent(LivingDamageEvent event) {
        //Check if is player doing the damage.
        if (event.getSource().getDirectEntity() instanceof PlayerEntity)
        {

            //Get Player.
            PlayerEntity player = (PlayerEntity) event.getSource().getDirectEntity();

            //Get the Ring as an ItemStack
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RISK_CHARM.get(), player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

            //Check if player is wearing it. Check if Sword Item.
            if (player.getHealth() <= 6)
            {
                    event.setAmount(event.getAmount() + 4f);
            }

        }
    }


    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        return new ICurio.SoundInfo(SoundEvents.ARMOR_EQUIP_IRON, 1.0f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("2 DMG at low Health"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

