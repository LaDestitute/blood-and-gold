package com.ladestitute.bloodandgold.items.spells;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.client.BaGKeybinds;
import com.ladestitute.bloodandgold.entities.EntityBomb;
import com.ladestitute.bloodandgold.entities.spells.EntityFireball;
import com.ladestitute.bloodandgold.registries.EntityTypeInit;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class BombSpellItem extends Item implements ICurioItem {

    public BombSpellItem(Properties properties) {
        super(properties);
    }

    @SubscribeEvent
    public static void castspell(TickEvent.PlayerTickEvent event)
    {
        event.player.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
            if(h.checkbombcharge() < 0)
            {
                h.setbombcharge(0);
            }
            ItemStack spell =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_MANA.get(), event.player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            //Failsafe to prevent underflowing
            //There may be a small chance it will underflow to -1 but
            //It will always usually self-correct to 0 next update if whenever

            if(!spell.isEmpty() && BaGKeybinds.castheldspell.consumeClick() && h.checkbombcharge() == 0) {
                if(!event.player.level.isClientSide) {
                    EntityBomb entity = new EntityBomb(EntityTypeInit.BOMB.get(), event.player.level);
                    entity.setPos(event.player.getX(), event.player.getY(), event.player.getZ());
                  //  worldIn.playSound(null, playerIn.blockPosition(), SoundInit.BOMB_FUSE.get(), SoundCategory.PLAYERS, 1.0F, 1.0F);
                    event.player.level.addFreshEntity(entity);
                }
                h.setbombcharge(5);
            }
        });
    }

    @SubscribeEvent
    public static void killlistener(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                ItemStack spell =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.BOMB_SPELL.get(), event.getEntityLiving()).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                if(!spell.isEmpty() && h.checkbombcharge() > 0)
                {
                    ItemStack ring_of_mana =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.BOMB_SPELL.get(), event.getEntityLiving()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(!ring_of_mana.isEmpty())
                    {
                        h.decreasebombcooldown(2);
                    }
                    else h.decreasebombcooldown(1);
                }
            });
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Override
    public ActionResult<ItemStack> use(World p_77659_1_, PlayerEntity p_77659_2_, Hand p_77659_3_) {
        p_77659_2_.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
            ItemStack spell =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.BOMB_SPELL.get(), p_77659_2_).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if(!spell.isEmpty())
            {
                h.setbombcharge(0);
                p_77659_2_.getMainHandItem().shrink(1);
            }
        });
        return super.use(p_77659_1_, p_77659_2_, p_77659_3_);
    }
}


