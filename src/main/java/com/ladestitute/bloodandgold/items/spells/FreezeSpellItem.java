package com.ladestitute.bloodandgold.items.spells;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.client.BaGKeybinds;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class FreezeSpellItem extends Item implements ICurioItem {

    public FreezeSpellItem(Properties properties) {
        super(properties);
    }

    @SubscribeEvent
    public static void killlistener(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                ItemStack spell =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.FREEZE_SPELL.get(), event.getEntityLiving()).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                if(!spell.isEmpty() && h.checkfreezecharge() > 0)
                {
                    ItemStack ring_of_mana =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.BOMB_SPELL.get(), event.getEntityLiving()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(!ring_of_mana.isEmpty())
                    {
                        h.decreasefreezecooldown(2);
                    }
                    else h.decreasefreezecooldown(1);
                }
            });
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Override
    public ActionResult<ItemStack> use(World p_77659_1_, PlayerEntity p_77659_2_, Hand p_77659_3_) {
        p_77659_2_.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
            ItemStack spell =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_MANA.get(), p_77659_2_).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if(!spell.isEmpty())
            {
                h.setfreezecharge(0);
                p_77659_2_.getMainHandItem().shrink(1);
            }
        });
        return super.use(p_77659_1_, p_77659_2_, p_77659_3_);
    }
}

