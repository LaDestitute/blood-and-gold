package com.ladestitute.bloodandgold.items.spells;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.client.BaGKeybinds;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.items.ItemHandlerHelper;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.Random;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class TransmuteSpellItem extends Item implements ICurioItem {

    public TransmuteSpellItem(Properties properties) {
        super(properties);
    }

    public static final ITag.INamedTag<Item> transmute_itempool =
            ItemTags.bind("bloodandgold:transmute_itempool");

    @SubscribeEvent
    public static void castspell(TickEvent.PlayerTickEvent event)
    {
        event.player.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
            ItemStack spell =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.TRANSMUTE_SPELL.get(), event.player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            //Failsafe to prevent underflowing
            //There may be a small chance it will underflow to -1 but
            //It will always usually self-correct to 0 next update if whenever

            if(!spell.isEmpty() && BaGKeybinds.castheldspell.consumeClick() && h.checktransmutecharge() == 0) {
                ItemStack ring_of_mana =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_MANA.get(), event.player).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                ItemStack handstack = event.player.getItemBySlot(EquipmentSlotType.MAINHAND);
                ItemStack offhandstack = event.player.getItemBySlot(EquipmentSlotType.OFFHAND);
                Random rand = new Random();

                if(ring_of_mana.isEmpty()) {
                    if (handstack.getItem() == ItemInit.RING_OF_WAR.get() ||
                            handstack.getItem() == ItemInit.RING_OF_PROTECTION.get() ||
                            handstack.getItem() == ItemInit.RING_OF_MANA.get() ||
                            handstack.getItem() == ItemInit.GLASS_SHARD.get() ||
                            handstack.getItem() == ItemInit.DAGGER_OF_FROST.get() ||
                            handstack.getItem() == ItemInit.DAGGER.get() ||
                            handstack.getItem() == ItemInit.FIREBALL_SPELL.get() ||
                            handstack.getItem() == ItemInit.INFERNAL_TORCH.get() ||
                            handstack.getItem() == ItemInit.GLASS_TORCH.get() ||
                            handstack.getItem() == ItemInit.FROST_CHARM.get() ||
                            handstack.getItem() == ItemInit.RISK_CHARM.get() ||
                            handstack.getItem() == ItemInit.PROTECTION_CHARM.get() ||
                            handstack.getItem() == ItemInit.JEWELED_DAGGER.get() ||
                            handstack.getItem() == ItemInit.RING_OF_SHIELDING.get() ||
                            handstack.getItem() == ItemInit.RING_OF_REGENERATION.get() ||
                            handstack.getItem() == ItemInit.RING_OF_PEACE.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_ARMOR.get() ||
                            handstack.getItem() == ItemInit.GLASS_ARMOR.get() ||
                            handstack.getItem() == ItemInit.TORCH_OF_STRENGTH.get() ||
                            handstack.getItem() == ItemInit.STRENGTH_CHARM.get() ||
                            handstack.getItem() == ItemInit.SHIELD_SPELL.get() ||
                            handstack.getItem() == ItemInit.RING_OF_MIGHT.get() ||
                            handstack.getItem() == ItemInit.RING_OF_GOLD.get() ||
                            handstack.getItem() == ItemInit.HEAL_SPELL.get() ||
                            handstack.getItem() == ItemInit.FREEZE_SPELL.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_SHOVEL.get() ||
                            handstack.getItem() == ItemInit.GLASS_SHOVEL.get() ||
                            handstack.getItem() == ItemInit.BLOOD_SHOVEL.get() ||
                            handstack.getItem() == ItemInit.BLOOD_DAGGER.get() ||
                            handstack.getItem() == ItemInit.BLOOD_SWORD.get() ||
                            handstack.getItem() == ItemInit.BLOOD_RAPIER.get() ||
                            handstack.getItem() == ItemInit.BLOOD_SPEAR.get() ||
                            handstack.getItem() == ItemInit.BLOOD_FLAIL.get() ||
                            handstack.getItem() == ItemInit.BLOOD_BOW.get() ||
                            handstack.getItem() == ItemInit.BLOOD_CROSSBOW.get() ||
                            handstack.getItem() == ItemInit.GLASS_DAGGER.get() ||
                            handstack.getItem() == ItemInit.GLASS_SWORD.get() ||
                            handstack.getItem() == ItemInit.GLASS_RAPIER.get() ||
                            handstack.getItem() == ItemInit.GLASS_SPEAR.get() ||
                            handstack.getItem() == ItemInit.GLASS_FLAIL.get() ||
                            handstack.getItem() == ItemInit.GLASS_BOW.get() ||
                            handstack.getItem() == ItemInit.GLASS_CROSSBOW.get() ||
                            handstack.getItem() == ItemInit.GOLD_DAGGER.get() ||
                            handstack.getItem() == Items.GOLDEN_SWORD ||
                            handstack.getItem() == ItemInit.GOLD_RAPIER.get() ||
                            handstack.getItem() == ItemInit.GOLD_SPEAR.get() ||
                            handstack.getItem() == ItemInit.GOLD_FLAIL.get() ||
                            handstack.getItem() == ItemInit.GOLD_BOW.get() ||
                            handstack.getItem() == ItemInit.GOLD_CROSSBOW.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_DAGGER.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_SWORD.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_SPEAR.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_BOW.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_DAGGER.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_SWORD.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_RAPIER.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_SPEAR.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_FLAIL.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_BOW.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_CROSSBOW.get()||
                            handstack.getItem() == ItemInit.KARATE_GI.get()||
                            handstack.getItem() == ItemInit.MAUSOLEUM_MASH_DISC.get()||
                            handstack.getItem() == ItemInit.PORTABELLOHEAD_DISC.get()||
                            handstack.getItem() == ItemInit.DANCE_OF_THE_DECOROUS_DISC.get()||
                            handstack.getItem() == ItemInit.A_HOT_MESS_DISC.get()||
                            handstack.getItem() == ItemInit.STYX_AND_STONES_DISC.get()||
                            handstack.getItem() == ItemInit.POWER_CORDS_DISC.get()||
                            handstack.getItem() == Items.IRON_PICKAXE||
                            handstack.getItem() == Items.BOW||
                            handstack.getItem() == Items.CROSSBOW||
                            handstack.getItem() == Items.LEATHER_CHESTPLATE||
                            handstack.getItem() == Items.CHAINMAIL_CHESTPLATE||
                            handstack.getItem() == Items.APPLE||
                            handstack.getItem() == Items.CARROT||
                            handstack.getItem() == Items.COOKIE||
                            handstack.getItem() == ItemInit.TITANIUM_SHOVEL.get()||
                            handstack.getItem() == ItemInit.CRYSTAL_SHOVEL.get()
                    )
                    {
                        handstack.shrink(1);
                            ItemHandlerHelper.giveItemToPlayer(event.player, transmute_itempool.getRandomElement(rand).getDefaultInstance());
                    }
                }
                if(!ring_of_mana.isEmpty()) {
                    if (offhandstack.getItem() == ItemInit.RING_OF_WAR.get() ||
                            offhandstack.getItem() == ItemInit.RING_OF_PROTECTION.get() ||
                            offhandstack.getItem() == ItemInit.RING_OF_MANA.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_SHARD.get() ||
                            offhandstack.getItem() == ItemInit.DAGGER_OF_FROST.get() ||
                            offhandstack.getItem() == ItemInit.DAGGER.get() ||
                            offhandstack.getItem() == ItemInit.FIREBALL_SPELL.get() ||
                            offhandstack.getItem() == ItemInit.INFERNAL_TORCH.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_TORCH.get() ||
                            offhandstack.getItem() == ItemInit.FROST_CHARM.get() ||
                            offhandstack.getItem() == ItemInit.RISK_CHARM.get() ||
                            offhandstack.getItem() == ItemInit.PROTECTION_CHARM.get() ||
                            offhandstack.getItem() == ItemInit.JEWELED_DAGGER.get() ||
                            offhandstack.getItem() == ItemInit.RING_OF_SHIELDING.get() ||
                            offhandstack.getItem() == ItemInit.RING_OF_REGENERATION.get() ||
                            offhandstack.getItem() == ItemInit.RING_OF_PEACE.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_ARMOR.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_ARMOR.get() ||
                            offhandstack.getItem() == ItemInit.TORCH_OF_STRENGTH.get() ||
                            offhandstack.getItem() == ItemInit.STRENGTH_CHARM.get() ||
                            offhandstack.getItem() == ItemInit.SHIELD_SPELL.get() ||
                            offhandstack.getItem() == ItemInit.RING_OF_MIGHT.get() ||
                            offhandstack.getItem() == ItemInit.RING_OF_GOLD.get() ||
                            offhandstack.getItem() == ItemInit.HEAL_SPELL.get() ||
                            offhandstack.getItem() == ItemInit.FREEZE_SPELL.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_SHOVEL.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_SHOVEL.get() ||
                            offhandstack.getItem() == ItemInit.BLOOD_SHOVEL.get() ||
                            offhandstack.getItem() == ItemInit.BLOOD_DAGGER.get() ||
                            offhandstack.getItem() == ItemInit.BLOOD_SWORD.get() ||
                            offhandstack.getItem() == ItemInit.BLOOD_RAPIER.get() ||
                            offhandstack.getItem() == ItemInit.BLOOD_SPEAR.get() ||
                            offhandstack.getItem() == ItemInit.BLOOD_FLAIL.get() ||
                            offhandstack.getItem() == ItemInit.BLOOD_BOW.get() ||
                            offhandstack.getItem() == ItemInit.BLOOD_CROSSBOW.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_DAGGER.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_SWORD.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_RAPIER.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_SPEAR.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_FLAIL.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_BOW.get() ||
                            offhandstack.getItem() == ItemInit.GLASS_CROSSBOW.get() ||
                            offhandstack.getItem() == ItemInit.GOLD_DAGGER.get() ||
                            offhandstack.getItem() == Items.GOLDEN_SWORD ||
                            offhandstack.getItem() == ItemInit.GOLD_RAPIER.get() ||
                            offhandstack.getItem() == ItemInit.GOLD_SPEAR.get() ||
                            offhandstack.getItem() == ItemInit.GOLD_FLAIL.get() ||
                            offhandstack.getItem() == ItemInit.GOLD_BOW.get() ||
                            offhandstack.getItem() == ItemInit.GOLD_CROSSBOW.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_DAGGER.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_SWORD.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_SPEAR.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_BOW.get() ||
                            offhandstack.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get() ||
                            offhandstack.getItem() == ItemInit.TITANIUM_DAGGER.get() ||
                            offhandstack.getItem() == ItemInit.TITANIUM_SWORD.get() ||
                            offhandstack.getItem() == ItemInit.TITANIUM_RAPIER.get() ||
                            offhandstack.getItem() == ItemInit.TITANIUM_SPEAR.get() ||
                            offhandstack.getItem() == ItemInit.TITANIUM_FLAIL.get() ||
                            offhandstack.getItem() == ItemInit.TITANIUM_BOW.get() ||
                            offhandstack.getItem() == ItemInit.TITANIUM_CROSSBOW.get()||
                            offhandstack.getItem() == ItemInit.KARATE_GI.get())

                    {
                        offhandstack.shrink(1);
                        ItemHandlerHelper.giveItemToPlayer(event.player, transmute_itempool.getRandomElement(rand).getDefaultInstance());

                    }
                    if (handstack.getItem() == ItemInit.RING_OF_WAR.get() ||
                            handstack.getItem() == ItemInit.RING_OF_PROTECTION.get() ||
                            handstack.getItem() == ItemInit.RING_OF_MANA.get() ||
                            handstack.getItem() == ItemInit.GLASS_SHARD.get() ||
                            handstack.getItem() == ItemInit.DAGGER_OF_FROST.get() ||
                            handstack.getItem() == ItemInit.DAGGER.get() ||
                            handstack.getItem() == ItemInit.FIREBALL_SPELL.get() ||
                            handstack.getItem() == ItemInit.INFERNAL_TORCH.get() ||
                            handstack.getItem() == ItemInit.GLASS_TORCH.get() ||
                            handstack.getItem() == ItemInit.FROST_CHARM.get() ||
                            handstack.getItem() == ItemInit.RISK_CHARM.get() ||
                            handstack.getItem() == ItemInit.PROTECTION_CHARM.get() ||
                            handstack.getItem() == ItemInit.JEWELED_DAGGER.get() ||
                            handstack.getItem() == ItemInit.RING_OF_SHIELDING.get() ||
                            handstack.getItem() == ItemInit.RING_OF_REGENERATION.get() ||
                            handstack.getItem() == ItemInit.RING_OF_PEACE.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_ARMOR.get() ||
                            handstack.getItem() == ItemInit.GLASS_ARMOR.get() ||
                            handstack.getItem() == ItemInit.TORCH_OF_STRENGTH.get() ||
                            handstack.getItem() == ItemInit.STRENGTH_CHARM.get() ||
                            handstack.getItem() == ItemInit.SHIELD_SPELL.get() ||
                            handstack.getItem() == ItemInit.RING_OF_MIGHT.get() ||
                            handstack.getItem() == ItemInit.RING_OF_GOLD.get() ||
                            handstack.getItem() == ItemInit.HEAL_SPELL.get() ||
                            handstack.getItem() == ItemInit.FREEZE_SPELL.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_SHOVEL.get() ||
                            handstack.getItem() == ItemInit.GLASS_SHOVEL.get() ||
                            handstack.getItem() == ItemInit.BLOOD_SHOVEL.get() ||
                            handstack.getItem() == ItemInit.BLOOD_DAGGER.get() ||
                            handstack.getItem() == ItemInit.BLOOD_SWORD.get() ||
                            handstack.getItem() == ItemInit.BLOOD_RAPIER.get() ||
                            handstack.getItem() == ItemInit.BLOOD_SPEAR.get() ||
                            handstack.getItem() == ItemInit.BLOOD_FLAIL.get() ||
                            handstack.getItem() == ItemInit.BLOOD_BOW.get() ||
                            handstack.getItem() == ItemInit.BLOOD_CROSSBOW.get() ||
                            handstack.getItem() == ItemInit.GLASS_DAGGER.get() ||
                            handstack.getItem() == ItemInit.GLASS_SWORD.get() ||
                            handstack.getItem() == ItemInit.GLASS_RAPIER.get() ||
                            handstack.getItem() == ItemInit.GLASS_SPEAR.get() ||
                            handstack.getItem() == ItemInit.GLASS_FLAIL.get() ||
                            handstack.getItem() == ItemInit.GLASS_BOW.get() ||
                            handstack.getItem() == ItemInit.GLASS_CROSSBOW.get() ||
                            handstack.getItem() == ItemInit.GOLD_DAGGER.get() ||
                            handstack.getItem() == Items.GOLDEN_SWORD ||
                            handstack.getItem() == ItemInit.GOLD_RAPIER.get() ||
                            handstack.getItem() == ItemInit.GOLD_SPEAR.get() ||
                            handstack.getItem() == ItemInit.GOLD_FLAIL.get() ||
                            handstack.getItem() == ItemInit.GOLD_BOW.get() ||
                            handstack.getItem() == ItemInit.GOLD_CROSSBOW.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_DAGGER.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_SWORD.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_RAPIER.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_SPEAR.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_FLAIL.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_BOW.get() ||
                            handstack.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_DAGGER.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_SWORD.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_RAPIER.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_SPEAR.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_FLAIL.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_BOW.get() ||
                            handstack.getItem() == ItemInit.TITANIUM_CROSSBOW.get()||
                            handstack.getItem() == ItemInit.KARATE_GI.get()||
                            handstack.getItem() == ItemInit.MAUSOLEUM_MASH_DISC.get()||
                            handstack.getItem() == ItemInit.PORTABELLOHEAD_DISC.get()||
                            handstack.getItem() == ItemInit.DANCE_OF_THE_DECOROUS_DISC.get()||
                            handstack.getItem() == ItemInit.A_HOT_MESS_DISC.get()||
                            handstack.getItem() == ItemInit.STYX_AND_STONES_DISC.get()||
                            handstack.getItem() == ItemInit.POWER_CORDS_DISC.get()||
                            handstack.getItem() == Items.IRON_PICKAXE||
                            handstack.getItem() == Items.BOW||
                            handstack.getItem() == Items.CROSSBOW||
                            handstack.getItem() == Items.LEATHER_CHESTPLATE||
                            handstack.getItem() == Items.CHAINMAIL_CHESTPLATE||
                            handstack.getItem() == Items.APPLE||
                            handstack.getItem() == Items.CARROT||
                            handstack.getItem() == Items.COOKIE
                    )
                    {
                        handstack.shrink(1);
                        ItemHandlerHelper.giveItemToPlayer(event.player, transmute_itempool.getRandomElement(rand).getDefaultInstance());
                    }
                }

                h.settransmutecharge(10);

            }
        });
    }

    @SubscribeEvent
    public static void killlistener(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                ItemStack spell =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.TRANSMUTE_SPELL.get(), event.getEntityLiving()).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                if(!spell.isEmpty() && h.checktransmutecharge() > 0)
                {
                    ItemStack ring_of_mana =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_MANA.get(), event.getEntityLiving()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(!ring_of_mana.isEmpty())
                    {
                        h.decreasetransmutecooldown(2);

                    }
                    else h.decreasetransmutecooldown(1);

                }
            });
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Override
    public ActionResult<ItemStack> use(World p_77659_1_, PlayerEntity p_77659_2_, Hand p_77659_3_) {
        p_77659_2_.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
            ItemStack spell =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.TRANSMUTE_SPELL.get(), p_77659_2_).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if(!spell.isEmpty())
            {
                h.settransmutecharge(0);
                p_77659_2_.getMainHandItem().shrink(1);
            }
        });
        return super.use(p_77659_1_, p_77659_2_, p_77659_3_);
    }
}


