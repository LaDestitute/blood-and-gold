package com.ladestitute.bloodandgold.items.spells;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.client.BaGKeybinds;
import com.ladestitute.bloodandgold.entities.spells.EntityFireball;
import com.ladestitute.bloodandgold.entities.spells.EntityFireballManaBuffed;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.util.bagcaps.effects.BaGEffectsCapabilityProvider;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID)
public class FireballSpellItem extends Item implements ICurioItem {

    public FireballSpellItem(Properties properties) {
        super(properties);
    }

    @SubscribeEvent
    public static void castspell(TickEvent.PlayerTickEvent event)
    {
        event.player.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
            if(h.checkfireballcharge() < 0)
            {
                h.setfireballcharge(0);
            }
            ItemStack spell =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.FIREBALL_SPELL.get(), event.player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            //Failsafe to prevent underflowing
            //There may be a small chance it will underflow to -1 but
            //It will always usually self-correct to 0 next update if whenever

            if(!spell.isEmpty() && BaGKeybinds.castheldspell.consumeClick() && h.checkfireballcharge() == 0) {
                ItemStack ring_of_mana =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_MANA.get(), event.player).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                if(!ring_of_mana.isEmpty())
                {
                    EntityFireballManaBuffed fireball_buffed = new EntityFireballManaBuffed(event.player, event.player.level);
                    //This is the second most important method here, it sets how the entity shall shoot.
                    // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                    fireball_buffed.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                    //This is the most important, it actually adds it to the world!
                    event.player.level.addFreshEntity(fireball_buffed);
                    h.setfireballcharge(5);
                }
                if(ring_of_mana.isEmpty())
                {
                    EntityFireball fireball = new EntityFireball(event.player, event.player.level);
                    //This is the second most important method here, it sets how the entity shall shoot.
                    // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                    fireball.shootFromRotation(event.player, event.player.xRot, event.player.yRot, 0.0F, 1.5F, 1F);
                    //This is the most important, it actually adds it to the world!
                    event.player.level.addFreshEntity(fireball);
                    h.setfireballcharge(5);
                }
            }
        });
    }

    @SubscribeEvent
    public static void killlistener(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof PlayerEntity)
            event.getSource().getEntity().getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
            {
                ItemStack spell =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.FIREBALL_SPELL.get(), event.getEntityLiving()).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                if(!spell.isEmpty() && h.checkfireballcharge() > 0)
                {
                    ItemStack ring_of_mana =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.BOMB_SPELL.get(), event.getEntityLiving()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(!ring_of_mana.isEmpty())
                    {
                        h.decreasefireballcooldown(2);
                    }
                    else h.decreasefireballcooldown(1);
                }
            });
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        return new ICurio.SoundInfo(SoundEvents.FIRE_AMBIENT, 1.0f, 1.0f);
    }

    @Override
    public ActionResult<ItemStack> use(World p_77659_1_, PlayerEntity p_77659_2_, Hand p_77659_3_) {
        p_77659_2_.getCapability(BaGEffectsCapabilityProvider.BAGEFFECTS).ifPresent(h ->
        {
            ItemStack spell =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.FIREBALL_SPELL.get(), p_77659_2_).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if(!spell.isEmpty())
            {
                h.setfireballcharge(0);
                p_77659_2_.getMainHandItem().shrink(1);
            }
        });
        return super.use(p_77659_1_, p_77659_2_, p_77659_3_);
    }
}

