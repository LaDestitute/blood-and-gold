package com.ladestitute.bloodandgold.items.weapons.titanium;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.*;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class TitaniumCrossbowItem extends CrossbowItem {

    public float bowdamagebonus = 2F;
    private static final Ingredient REPAIR = Ingredient.of(Items.AIR);

    public  TitaniumCrossbowItem(Properties p_i48522_1_) {
        super(p_i48522_1_);
    }

    //Remove the "public static final Ingredient" line and this if you don't want a repairable bow
    @Override
    public boolean isValidRepairItem(ItemStack toRepair, ItemStack repair) {
        return REPAIR.test(repair);
    }

    //We pull some of the code from BowArrow and modify the arrow so we can do stuff such as having a bow fire arrows straight or bows applying damage bonuses
    //You can also set the arrow on fire, make it glow, etc
    //This is a smarter way of doing custom bow-damage properties than copying the entire right-click mehtod
    public AbstractArrowEntity getArrow(World p_220024_0_, LivingEntity p_220024_1_, ItemStack p_220024_2_, ItemStack p_220024_3_) {
        ArrowItem arrowitem = (ArrowItem)(p_220024_3_.getItem() instanceof ArrowItem ? p_220024_3_.getItem() : Items.ARROW);
        AbstractArrowEntity abstractarrowentity = arrowitem.createArrow(p_220024_0_, p_220024_3_, p_220024_1_);
        if (p_220024_1_ instanceof PlayerEntity) {
            abstractarrowentity.setBaseDamage(abstractarrowentity.getBaseDamage()+bowdamagebonus);
            abstractarrowentity.setPierceLevel((byte) 1);
        }

        abstractarrowentity.setSoundEvent(SoundEvents.CROSSBOW_HIT);
        abstractarrowentity.setShotFromCrossbow(true);


        return abstractarrowentity;
    }

    //The bow's enchantability, we return 0 cause we don't want it enchantable but you can return 5-ish or higher if you want enchantments for them
    @Override
    public int getEnchantmentValue() {
        return 12;
    }

    //The bow's book-enchantability, we return false cause we don't want it enchantable but you can return 5-ish or higher if you want enchantments for them
    @Override
    public boolean isBookEnchantable(ItemStack stack, ItemStack book)
    {
        return true;
    }

    //The distance that hostile entities holding the ranged weapon begin shooting the player at
    @Override
    public int getDefaultProjectileRange() {
        return 15;
    }

    //Bows always have arbitrarily long usedurations
    @Override
    public int getUseDuration(ItemStack p_77626_1_) {
        return 72000;
    }

    @Override
    public UseAction getUseAnimation(ItemStack p_77661_1_) {
        return UseAction.BOW;
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Not as durable as iron but a strong crossbow."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}


