package com.ladestitute.bloodandgold.blocks.shrines;

import com.ladestitute.bloodandgold.blocks.helper.BaseHorizontalBlock;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.Difficulty;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class ShrineOfBloodUnactivatedBlock extends BaseHorizontalBlock {

    private static final VoxelShape SHAPE = Stream
            .of(Block.box(4, 6, 4, 12, 10, 12),
                    Block.box(3, 0, 3, 13, 6, 13))
            .reduce((v1, v2) -> {
                return VoxelShapes.join(v1, v2, IBooleanFunction.OR);
            }).get();


    public ShrineOfBloodUnactivatedBlock(int i, Properties properties) {
        super(properties);
        runCalculation(SHAPE);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(this).get(state.getValue(HORIZONTAL_FACING));
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This shrine will give you something in return if you fill it with your blood..."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    public static final ITag.INamedTag<Item> bloodshrine_itempool =
            ItemTags.bind("bloodandgold:bloodshrine_itempool");

    @Override
    public ActionResultType use(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand p_225533_5_, BlockRayTraceResult p_225533_6_) {
        if (world.getDifficulty() != Difficulty.PEACEFUL) {
        Random rand1 = new Random();
        ItemHandlerHelper.giveItemToPlayer(player, bloodshrine_itempool.getRandomElement(rand1).getDefaultInstance());
        player.setHealth(2F);
            world.setBlock(pos, SpecialBlocksInit.SHRINE_OF_BLOOD_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                    state.getValue(HORIZONTAL_FACING)), 2);
            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BLOOD_SHOVEL.get().getDefaultInstance());
    }
            return super.use(state, world, pos, player, p_225533_5_, p_225533_6_);
        }
    }

