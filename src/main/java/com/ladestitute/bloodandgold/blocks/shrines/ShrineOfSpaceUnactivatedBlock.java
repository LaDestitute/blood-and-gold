package com.ladestitute.bloodandgold.blocks.shrines;

import com.ladestitute.bloodandgold.blocks.helper.BaseHorizontalBlock;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.Difficulty;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class ShrineOfSpaceUnactivatedBlock extends BaseHorizontalBlock {

    private static final VoxelShape SHAPE = Stream
            .of(Block.box(4, 6, 4, 12, 10, 12),
                    Block.box(3, 0, 3, 13, 6, 13))
            .reduce((v1, v2) -> {
                return VoxelShapes.join(v1, v2, IBooleanFunction.OR);
            }).get();


    public ShrineOfSpaceUnactivatedBlock(int i, Properties properties) {
        super(properties);
        runCalculation(SHAPE);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(this).get(state.getValue(HORIZONTAL_FACING));
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This shrine will give you a tool to make space but will also clear space around but not give you anything from the blocks."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    public static final ITag.INamedTag<Item> spaceshrine_itempool =
            ItemTags.bind("bloodandgold:spaceshrine_itempool");

    @Override
    public ActionResultType use(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand p_225533_5_, BlockRayTraceResult p_225533_6_) {
        if (world.getDifficulty() != Difficulty.PEACEFUL) {
            Random rand1 = new Random();
            ItemHandlerHelper.giveItemToPlayer(player, spaceshrine_itempool.getRandomElement(rand1).getDefaultInstance());
            BlockPos blockPos = pos;
            int searchRadius = 2;
            int radius = (int) (Math.floor(1.5) + searchRadius);

            for (int x = -radius; x <= radius; ++x) {
                for (int y = -1; y <= 1; ++y) {
                    for (int z = -radius; z <= radius; ++z) {
                        BlockPos shrinePos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                        BlockState blockState = player.level.getBlockState(shrinePos);
                        Block block = blockState.getBlock();
                        //  BlockPos sacriPos = blockState.getBlock().defaultBlockState().;
                        // BlockPos sacrificePos = new BlockPos(block.defaultBlockState().getBlock().);
                        if (block != Blocks.BARRIER || block != Blocks.BEDROCK ||
                                block != Blocks.COMMAND_BLOCK || block != Blocks.END_GATEWAY ||
                                block != Blocks.END_PORTAL || block != Blocks.END_PORTAL_FRAME ||
                                block != Blocks.JIGSAW || block != Blocks.NETHER_PORTAL ||
                                block != Blocks.STRUCTURE_BLOCK || block != Blocks.STRUCTURE_VOID ||
                                block != Blocks.LAVA || block != Blocks.WATER ||
                        block != SpecialBlocksInit.SHRINE_OF_BLOOD_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_DARKNESS_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_GLASS_UNACTIVATED.get()||
                                block != SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get()||
                                block != SpecialBlocksInit.SHRINE_OF_PEACE_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_RHYTHM_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_SPACE_UNACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_BLOOD_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_DARKNESS_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_GLASS_ACTIVATED.get()||
                                block != SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get()||
                                block != SpecialBlocksInit.SHRINE_OF_PEACE_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_RHYTHM_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get() ||
                                block != SpecialBlocksInit.SHRINE_OF_SPACE_ACTIVATED.get()) {
                            if (block == Blocks.STONE || block == Blocks.DIRT || block == Blocks.COBBLESTONE ||
                                    block == Blocks.GRASS_BLOCK || block == Blocks.SNOW || block == Blocks.SNOW_BLOCK ||
                                    block == Blocks.ANDESITE || block == Blocks.BRICKS
                                    || block == Blocks.CLAY || block == Blocks.COAL_ORE ||
                                    block == Blocks.COARSE_DIRT || block == Blocks.DIAMOND_ORE || block == Blocks.DIORITE ||
                                    block == Blocks.EMERALD_ORE || block == Blocks.GOLD_ORE || block == Blocks.GRANITE ||
                                    block == Blocks.GRAVEL || block == Blocks.ICE || block == Blocks.IRON_ORE ||
                                    block == Blocks.PODZOL || block == Blocks.SANDSTONE || block == Blocks.SAND ||
                                    block == Blocks.LAPIS_ORE || block == Blocks.STONE_BRICKS || block == Blocks.SMOOTH_STONE ||
                                    block == Blocks.SMOOTH_SANDSTONE || block == Blocks.SMOOTH_RED_SANDSTONE || block == Blocks.RED_SAND ||
                                    block == Blocks.RED_SANDSTONE || block == Blocks.REDSTONE_ORE ||
                                    block == Blocks.MOSSY_COBBLESTONE || block == Blocks.CHISELED_STONE_BRICKS || block == Blocks.MOSSY_STONE_BRICKS ||
                                    block == Blocks.CRACKED_STONE_BRICKS || block == Blocks.MYCELIUM ||
                                    block == Blocks.ACACIA_LOG || block == Blocks.ACACIA_PLANKS || block == Blocks.ACACIA_WOOD ||
                                    block == Blocks.BIRCH_LOG || block == Blocks.BIRCH_PLANKS || block == Blocks.BIRCH_WOOD ||
                                    block == Blocks.DARK_OAK_LOG || block == Blocks.DARK_OAK_PLANKS || block == Blocks.DARK_OAK_WOOD ||
                                    block == Blocks.JUNGLE_LOG || block == Blocks.JUNGLE_PLANKS || block == Blocks.JUNGLE_WOOD ||
                                    block == Blocks.OAK_LOG || block == Blocks.OAK_PLANKS || block == Blocks.OAK_WOOD ||
                                    block == Blocks.SPRUCE_LOG || block == Blocks.SPRUCE_PLANKS || block == Blocks.SPRUCE_WOOD ||
                                    block == Blocks.BLUE_ICE || block == Blocks.CHISELED_RED_SANDSTONE || block == Blocks.CHISELED_SANDSTONE ||
                                    block == Blocks.FROSTED_ICE || block == Blocks.GRASS_PATH || block == Blocks.TERRACOTTA ||
                                    block == Blocks.RED_TERRACOTTA || block == Blocks.ORANGE_TERRACOTTA || block == Blocks.YELLOW_TERRACOTTA ||
                                    block == Blocks.BROWN_TERRACOTTA || block == Blocks.WHITE_TERRACOTTA ||
                                    block == Blocks.LIGHT_GRAY_TERRACOTTA || block == Blocks.LIME_TERRACOTTA || block == Blocks.BLUE_TERRACOTTA) {
                                player.level.removeBlock(pos.above(), true);
                                player.level.removeBlock(pos.east(), true);
                                player.level.removeBlock(pos.west(), true);
                                player.level.removeBlock(pos.south(), true);
                                player.level.removeBlock(pos.north(), true);
                                player.level.removeBlock(pos.above().above(), true);
                                player.level.removeBlock(shrinePos.above(), true);
                                player.level.removeBlock(shrinePos.above().above(), true);
                                player.level.removeBlock(shrinePos.above().above().above(), true);

                            }
                        }
                    }
                }
            }
            world.setBlock(pos, SpecialBlocksInit.SHRINE_OF_SPACE_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                    state.getValue(HORIZONTAL_FACING)), 2);
        }
        return super.use(state, world, pos, player, p_225533_5_, p_225533_6_);
    }
}

