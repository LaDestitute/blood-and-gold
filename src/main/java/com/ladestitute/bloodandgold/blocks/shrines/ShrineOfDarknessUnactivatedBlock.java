package com.ladestitute.bloodandgold.blocks.shrines;

import com.ladestitute.bloodandgold.blocks.helper.BaseHorizontalBlock;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import com.ladestitute.bloodandgold.util.tags.BaGTags;
import net.minecraft.block.*;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.Difficulty;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.Tags;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.stream.Stream;

public class ShrineOfDarknessUnactivatedBlock extends BaseHorizontalBlock {

    private static final VoxelShape SHAPE = Stream
            .of(Block.box(4, 6, 4, 12, 10, 12),
                    Block.box(3, 0, 3, 13, 6, 13))
            .reduce((v1, v2) -> {
                return VoxelShapes.join(v1, v2, IBooleanFunction.OR);
            }).get();


    public ShrineOfDarknessUnactivatedBlock(int i, Properties properties) {
        super(properties.lightLevel((state) -> {
            return 10;
        }));
        runCalculation(SHAPE);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(this).get(state.getValue(HORIZONTAL_FACING));
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This shrine will give you insight but also ironically blind you."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public ActionResultType use(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand p_225533_5_, BlockRayTraceResult p_225533_6_) {
        if (world.getDifficulty() != Difficulty.PEACEFUL) {
            //Todo: removing placed torches in a wide radius
            ItemHandlerHelper.giveItemToPlayer(player, Items.MAP.getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(player, Items.COMPASS.getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(player, Items.TNT.getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(player, Items.FLINT_AND_STEEL.getDefaultInstance());
            //ItemHandlerHelper.giveItemToPlayer(player, disallowed_darkness_shrine_items.getRandomElement(random).getDefaultInstance());
            player.addEffect(new EffectInstance(Effects.BLINDNESS, 300, 0));

            BlockPos blockPos = player.blockPosition();
            int torchsearchRadius = 10;
            int radius = (int) (Math.floor(2.0) + torchsearchRadius);

            for (int x = -radius; x <= radius; ++x) {
                for (int y = -1; y <= 1; ++y) {
                    for (int z = -radius; z <= radius; ++z) {
                        BlockPos shrinePos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                        BlockState blockState = world.getBlockState(shrinePos);
                        Block block = blockState.getBlock();
                        if (block instanceof TorchBlock || block instanceof WallTorchBlock ||
                                block instanceof LanternBlock) {
                                world.destroyBlock(shrinePos, true);
                        }
                    }

                }
            }

            for (ItemStack disalloweditem : player.inventory.items) {
                if (disalloweditem.getItem() == Items.SOUL_TORCH) {
                    disalloweditem.setCount(0);
                }
                if (disalloweditem.getItem() == Items.TORCH) {
                    disalloweditem.setCount(0);
                }
                if (disalloweditem.getItem() == Items.COAL) {
                    disalloweditem.setCount(0);
                }
                if (disalloweditem.getItem() == Items.CHARCOAL) {
                    disalloweditem.setCount(0);
                }
                if (disalloweditem.getItem() == Items.SOUL_SAND) {
                    disalloweditem.setCount(0);
                }
                if (disalloweditem.getItem() == Items.SOUL_SOIL) {
                    disalloweditem.setCount(0);
                }
                if (disalloweditem.getItem() == Items.SOUL_LANTERN) {
                    disalloweditem.setCount(0);
                }
                if (disalloweditem.getItem() == Items.LANTERN) {
                    disalloweditem.setCount(0);
                }
            }

            world.setBlock(pos, SpecialBlocksInit.SHRINE_OF_DARKNESS_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                    state.getValue(HORIZONTAL_FACING)), 2);
        }
        return super.use(state, world, pos, player, p_225533_5_, p_225533_6_);
    }
}
