package com.ladestitute.bloodandgold.blocks.shrines;

import com.ladestitute.bloodandgold.blocks.helper.BaseHorizontalBlock;
import com.ladestitute.bloodandgold.registries.ItemInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import net.minecraft.block.*;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.Difficulty;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.stream.Stream;

public class ShrineOfGlassUnactivatedBlock extends BaseHorizontalBlock {

    private static final VoxelShape SHAPE = Stream
            .of(Block.box(4, 6, 4, 12, 10, 12),
                    Block.box(3, 0, 3, 13, 6, 13))
            .reduce((v1, v2) -> {
                return VoxelShapes.join(v1, v2, IBooleanFunction.OR);
            }).get();


    public ShrineOfGlassUnactivatedBlock(int i, Properties properties) {
        super(properties);
        runCalculation(SHAPE);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(this).get(state.getValue(HORIZONTAL_FACING));
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This shrine will give you great strength but also make you very fragile."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public ActionResultType use(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand p_225533_5_, BlockRayTraceResult p_225533_6_) {
        if (world.getDifficulty() != Difficulty.PEACEFUL) {
            for (ItemStack disalloweditem : player.inventory.items) {
                if (disalloweditem.getItem() == ItemInit.BLOOD_RAPIER.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_RAPIER.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.BLOOD_DAGGER.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_DAGGER.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.BLOOD_SWORD.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.BLOOD_SPEAR.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SPEAR.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.BLOOD_BOW.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_BOW.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.BLOOD_FLAIL.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_FLAIL.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.BLOOD_CROSSBOW.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_CROSSBOW.get().getDefaultInstance());
                }
                //
                if (disalloweditem.getItem() == ItemInit.TITANIUM_RAPIER.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_RAPIER.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.TITANIUM_DAGGER.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_DAGGER.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.TITANIUM_SWORD.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.TITANIUM_SPEAR.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SPEAR.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.TITANIUM_BOW.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_BOW.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.TITANIUM_FLAIL.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_FLAIL.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.TITANIUM_CROSSBOW.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_CROSSBOW.get().getDefaultInstance());
                }
                //
                if (disalloweditem.getItem() == ItemInit.OBSIDIAN_RAPIER.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_RAPIER.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.OBSIDIAN_DAGGER.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_DAGGER.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.OBSIDIAN_SWORD.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.OBSIDIAN_SPEAR.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SPEAR.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.OBSIDIAN_BOW.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_BOW.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.OBSIDIAN_FLAIL.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_FLAIL.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.OBSIDIAN_CROSSBOW.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_CROSSBOW.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.BOW) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_BOW.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.CROSSBOW) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_CROSSBOW.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.TRIDENT) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SPEAR.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.WOODEN_SWORD) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.STONE_SWORD) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.IRON_SWORD) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.GOLDEN_SWORD) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.DIAMOND_SWORD) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == Items.NETHERITE_SWORD) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SWORD.get().getDefaultInstance());
                }
                //
                if (disalloweditem.getItem() == ItemInit.GOLD_RAPIER.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_RAPIER.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.GOLD_DAGGER.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_DAGGER.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.GOLD_SPEAR.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SPEAR.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.GOLD_BOW.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_BOW.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.GOLD_FLAIL.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_FLAIL.get().getDefaultInstance());
                }
                if (disalloweditem.getItem() == ItemInit.GOLD_CROSSBOW.get()) {
                    disalloweditem.setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_CROSSBOW.get().getDefaultInstance());
                }
            }

            if(player.hasItemInSlot(EquipmentSlotType.HEAD))
            {
                player.inventory.armor.set(EquipmentSlotType.HEAD.getIndex(), ItemStack.EMPTY);
            }
            if(player.hasItemInSlot(EquipmentSlotType.CHEST))
            {
                player.inventory.armor.set(EquipmentSlotType.CHEST.getIndex(), ItemInit.GLASS_ARMOR.get().getDefaultInstance());
            }
            if(player.hasItemInSlot(EquipmentSlotType.LEGS))
            {
                player.inventory.armor.set(EquipmentSlotType.LEGS.getIndex(), ItemStack.EMPTY);
            }
            if(player.hasItemInSlot(EquipmentSlotType.FEET))
            {
                player.inventory.armor.set(EquipmentSlotType.FEET.getIndex(), ItemStack.EMPTY);
            }
            world.setBlock(pos, SpecialBlocksInit.SHRINE_OF_GLASS_ACTIVATED.get().defaultBlockState().setValue(HORIZONTAL_FACING,
                    state.getValue(HORIZONTAL_FACING)), 2);
            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.GLASS_SHOVEL.get().getDefaultInstance());

        }
        return super.use(state, world, pos, player, p_225533_5_, p_225533_6_);
    }
}

