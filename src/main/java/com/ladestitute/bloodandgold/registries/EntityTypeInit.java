package com.ladestitute.bloodandgold.registries;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.entities.EntityBomb;
import com.ladestitute.bloodandgold.entities.spells.EntityFireball;
import com.ladestitute.bloodandgold.entities.spells.EntityFireballManaBuffed;
import com.ladestitute.bloodandgold.entities.thrown.*;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class EntityTypeInit {
        public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, BaGMain.MOD_ID);
        //Projectiles
        public static final RegistryObject<EntityType<EntityThrownGoldDagger>> THROWN_GOLD_DAGGER = ENTITIES.register("thrown_gold_dagger",
                () -> EntityType.Builder.<EntityThrownGoldDagger>of(EntityThrownGoldDagger::new, EntityClassification.MISC)
                        .sized(0.25F, 0.25F).build("thrown_gold_dagger"));
        public static final RegistryObject<EntityType<EntityThrownGoldSpear>> THROWN_GOLD_SPEAR = ENTITIES.register("thrown_gold_spear",
                () -> EntityType.Builder.<EntityThrownGoldSpear>of(EntityThrownGoldSpear::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_gold_spear"));

        public static final RegistryObject<EntityType<EntityThrownBloodDagger>> THROWN_BLOOD_DAGGER = ENTITIES.register("thrown_blood_dagger",
                () -> EntityType.Builder.<EntityThrownBloodDagger>of(EntityThrownBloodDagger::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_blood_dagger"));
        public static final RegistryObject<EntityType<EntityThrownBloodSpear>> THROWN_BLOOD_SPEAR = ENTITIES.register("thrown_blood_spear",
                () -> EntityType.Builder.<EntityThrownBloodSpear>of(EntityThrownBloodSpear::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_blood_spear"));

        public static final RegistryObject<EntityType<EntityThrownTitaniumDagger>> THROWN_TITANIUM_DAGGER = ENTITIES.register("thrown_titanium_dagger",
                () -> EntityType.Builder.<EntityThrownTitaniumDagger>of(EntityThrownTitaniumDagger::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_titanium_dagger"));
        public static final RegistryObject<EntityType<EntityThrownTitaniumSpear>> THROWN_TITANIUM_SPEAR = ENTITIES.register("thrown_titanium_spear",
                () -> EntityType.Builder.<EntityThrownTitaniumSpear>of(EntityThrownTitaniumSpear::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_titanium_spear"));

        public static final RegistryObject<EntityType<EntityThrownGlassDagger>> THROWN_GLASS_DAGGER = ENTITIES.register("thrown_glass_dagger",
                () -> EntityType.Builder.<EntityThrownGlassDagger>of(EntityThrownGlassDagger::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_glass_dagger"));
        public static final RegistryObject<EntityType<EntityThrownGlassSpear>> THROWN_GLASS_SPEAR = ENTITIES.register("thrown_glass_spear",
                () -> EntityType.Builder.<EntityThrownGlassSpear>of(EntityThrownGlassSpear::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_glass_spear"));

        public static final RegistryObject<EntityType<EntityThrownObsidianDagger>> THROWN_OBSIDIAN_DAGGER = ENTITIES.register("thrown_obsidian_dagger",
                () -> EntityType.Builder.<EntityThrownObsidianDagger>of(EntityThrownObsidianDagger::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_obsidian_dagger"));
        public static final RegistryObject<EntityType<EntityThrownObsidianSpear>> THROWN_OBSIDIAN_SPEAR = ENTITIES.register("thrown_obsidian_spear",
                () -> EntityType.Builder.<EntityThrownObsidianSpear>of(EntityThrownObsidianSpear::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_obsidian_spear"));

        public static final RegistryObject<EntityType<EntityThrownJeweledDagger>> THROWN_JEWELED_DAGGER = ENTITIES.register("thrown_jeweled_dagger",
                () -> EntityType.Builder.<EntityThrownJeweledDagger>of(EntityThrownJeweledDagger::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_jeweled_dagger"));

        public static final RegistryObject<EntityType<EntityThrownFrostDagger>> THROWN_FROST_DAGGER = ENTITIES.register("thrown_frost_dagger",
                () -> EntityType.Builder.<EntityThrownFrostDagger>of(EntityThrownFrostDagger::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_frost_dagger"));

        public static final RegistryObject<EntityType<EntityThrownDagger>> THROWN_DAGGER = ENTITIES.register("thrown_dagger",
                () -> EntityType.Builder.<EntityThrownDagger>of(EntityThrownDagger::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_dagger"));

        public static final RegistryObject<EntityType<EntityThrownGlassShard>> THROWN_GLASS_SHARD = ENTITIES.register("thrown_glass_shard",
                () -> EntityType.Builder.<EntityThrownGlassShard>of(EntityThrownGlassShard::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("thrown_glass_shard"));

        public static final RegistryObject<EntityType<EntityBomb>> BOMB = ENTITIES.register("bomb",
                () -> EntityType.Builder.<EntityBomb>of(EntityBomb::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("bomb"));

        //spells
        public static final RegistryObject<EntityType<EntityFireball>> FIREBALL = ENTITIES.register("fireball",
                () -> EntityType.Builder.<EntityFireball>of(EntityFireball::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("fireball"));
        public static final RegistryObject<EntityType<EntityFireballManaBuffed>> FIREBALL_MANA_BUFFED = ENTITIES.register("fireball_mana_buffed",
                () -> EntityType.Builder.<EntityFireballManaBuffed>of(EntityFireballManaBuffed::new,
                        EntityClassification.MISC).sized(0.25F, 0.25F).build("fireball_mana_buffed"));
}
