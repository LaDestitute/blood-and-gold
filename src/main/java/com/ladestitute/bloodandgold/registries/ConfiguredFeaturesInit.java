package com.ladestitute.bloodandgold.registries;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;

import java.rmi.registry.RegistryHandler;
import java.util.function.Supplier;

public class ConfiguredFeaturesInit {
    public static final BlockClusterFeatureConfig SHRINE_OF_BLOOD = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(SpecialBlocksInit.SHRINE_OF_BLOOD_UNACTIVATED.get().defaultBlockState()),
            new SimpleBlockPlacer())).whitelist(ImmutableSet.of(Blocks.STONE))
            .tries(5)
            .xspread(96)
            .yspread(96)
            .zspread(96)
            .build();
    public static final BlockClusterFeatureConfig SHRINE_OF_DARKNESS = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(SpecialBlocksInit.SHRINE_OF_DARKNESS_UNACTIVATED.get().defaultBlockState()),
            new SimpleBlockPlacer())).whitelist(ImmutableSet.of(Blocks.STONE))
            .tries(5)
            .xspread(96)
            .yspread(96)
            .zspread(96)
            .build();
    public static final BlockClusterFeatureConfig SHRINE_OF_PEACE = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(SpecialBlocksInit.SHRINE_OF_PEACE_UNACTIVATED.get().defaultBlockState()),
            new SimpleBlockPlacer())).whitelist(ImmutableSet.of(Blocks.STONE))
            .tries(5)
            .xspread(96)
            .yspread(96)
            .zspread(96)
            .build();
    public static final BlockClusterFeatureConfig SHRINE_OF_GLASS = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(SpecialBlocksInit.SHRINE_OF_GLASS_UNACTIVATED.get().defaultBlockState()),
            new SimpleBlockPlacer())).whitelist((ImmutableSet.of(Blocks.STONE)))
            .tries(5)
            .xspread(96)
            .yspread(96)
            .zspread(96)
            .build();
    public static final BlockClusterFeatureConfig SHRINE_OF_RHYTHM = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(SpecialBlocksInit.SHRINE_OF_RHYTHM_UNACTIVATED.get().defaultBlockState()),
            new SimpleBlockPlacer())).whitelist((ImmutableSet.of(Blocks.STONE)))
            .tries(5)
            .xspread(96)
            .yspread(96)
            .zspread(96)
            .build();
    public static final BlockClusterFeatureConfig SHRINE_OF_SACRIFICE = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get().defaultBlockState()),
            new SimpleBlockPlacer())).whitelist((ImmutableSet.of(Blocks.STONE)))
            .tries(5)
            .xspread(96)
            .yspread(96)
            .zspread(96)
            .build();
    public static final BlockClusterFeatureConfig SHRINE_OF_SPACE = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(SpecialBlocksInit.SHRINE_OF_SPACE_UNACTIVATED.get().defaultBlockState()),
            new SimpleBlockPlacer())).whitelist(ImmutableSet.of(Blocks.STONE))
            .tries(5)
            .xspread(96)
            .yspread(96)
            .zspread(96)
            .build();
    public static final BlockClusterFeatureConfig SHRINE_OF_PAIN = (new BlockClusterFeatureConfig.Builder(
            new SimpleBlockStateProvider(SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get().defaultBlockState()),
            new SimpleBlockPlacer())).whitelist(ImmutableSet.of(Blocks.STONE))
            .tries(5)
            .xspread(96)
            .yspread(96)
            .zspread(96)
            .build();

    public static ConfiguredFeature SHRINE_OF_BLOOD_CONFIGURED = new ConfiguredFeature(
            Feature.RANDOM_PATCH,
            SHRINE_OF_BLOOD);
    public static ConfiguredFeature SHRINE_OF_DARKNESS_CONFIGURED = new ConfiguredFeature(
            Feature.RANDOM_PATCH,
            SHRINE_OF_DARKNESS);
    public static ConfiguredFeature SHRINE_OF_PEACE_CONFIGURED = new ConfiguredFeature(
            Feature.RANDOM_PATCH,
            SHRINE_OF_PEACE);
    public static ConfiguredFeature SHRINE_OF_GLASS_CONFIGURED = new ConfiguredFeature(
            Feature.RANDOM_PATCH,
            SHRINE_OF_GLASS);
    public static ConfiguredFeature SHRINE_OF_RHYTHM_CONFIGURED = new ConfiguredFeature(
            Feature.RANDOM_PATCH,
            SHRINE_OF_RHYTHM);
    public static ConfiguredFeature SHRINE_OF_SACRIFICE_CONFIGURED = new ConfiguredFeature(
            Feature.RANDOM_PATCH,
            SHRINE_OF_SACRIFICE);
    public static ConfiguredFeature SHRINE_OF_SPACE_CONFIGURED = new ConfiguredFeature(
            Feature.RANDOM_PATCH,
            SHRINE_OF_SPACE);
    public static ConfiguredFeature SHRINE_OF_PAIN_CONFIGURED = new ConfiguredFeature(
            Feature.RANDOM_PATCH,
            SHRINE_OF_PAIN);

}