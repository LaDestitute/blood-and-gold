package com.ladestitute.bloodandgold.registries;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.blocks.helper.FrozenEffectBlock;
import com.ladestitute.bloodandgold.blocks.shrines.*;
import com.ladestitute.bloodandgold.blocks.shrines.activated.*;
import net.minecraft.block.Block;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class SpecialBlocksInit {

    //An alt block registering class for blocks we want to separate into other tabs, manually set itemblocks, etc

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            BaGMain.MOD_ID);

    //Helper blocks

    //Shrines
    public static final RegistryObject<Block> SHRINE_OF_BLOOD_UNACTIVATED = BLOCKS.register("shrine_of_blood_unactivated",
            () -> new ShrineOfBloodUnactivatedBlock(-0x878787, PropertiesInit.SHRINE));
    public static final RegistryObject<Block> SHRINE_OF_BLOOD_ACTIVATED = BLOCKS.register("shrine_of_blood_activated",
            () -> new ShrineOfBloodActivatedBlock(-0x878787, PropertiesInit.SHRINE));

    public static final RegistryObject<Block> SHRINE_OF_DARKNESS_UNACTIVATED = BLOCKS.register("shrine_of_darkness_unactivated",
            () -> new ShrineOfDarknessUnactivatedBlock(-0x878787, PropertiesInit.SHRINE));
    public static final RegistryObject<Block> SHRINE_OF_DARKNESS_ACTIVATED = BLOCKS.register("shrine_of_darkness_activated",
            () -> new ShrineOfDarknessActivatedBlock(-0x878787, PropertiesInit.SHRINE));

    public static final RegistryObject<Block> SHRINE_OF_PEACE_UNACTIVATED = BLOCKS.register("shrine_of_peace_unactivated",
            () -> new ShrineOfPeaceUnactivatedBlock(-0x878787, PropertiesInit.SHRINE));
    public static final RegistryObject<Block> SHRINE_OF_PEACE_ACTIVATED = BLOCKS.register("shrine_of_peace_activated",
            () -> new ShrineOfPeaceActivatedBlock(-0x878787, PropertiesInit.SHRINE));

    public static final RegistryObject<Block> SHRINE_OF_GLASS_UNACTIVATED = BLOCKS.register("shrine_of_glass_unactivated",
            () -> new ShrineOfGlassUnactivatedBlock(-0x878787, PropertiesInit.SHRINE));
    public static final RegistryObject<Block> SHRINE_OF_GLASS_ACTIVATED = BLOCKS.register("shrine_of_glass_activated",
            () -> new ShrineOfGlassActivatedBlock(-0x878787, PropertiesInit.SHRINE));

    public static final RegistryObject<Block> SHRINE_OF_RHYTHM_UNACTIVATED = BLOCKS.register("shrine_of_rhythm_unactivated",
            () -> new ShrineOfRhythmUnactivatedBlock(-0x878787, PropertiesInit.SHRINE));
    public static final RegistryObject<Block> SHRINE_OF_RHYTHM_ACTIVATED = BLOCKS.register("shrine_of_rhythm_activated",
            () -> new ShrineOfRhythmActivatedBlock(-0x878787, PropertiesInit.SHRINE));

    public static final RegistryObject<Block> SHRINE_OF_SACRIFICE_UNACTIVATED = BLOCKS.register("shrine_of_sacrifice_unactivated",
            () -> new ShrineOfSacrificeUnactivatedBlock(-0x878787, PropertiesInit.SHRINE));
    public static final RegistryObject<Block> SHRINE_OF_SACRIFICE_ACTIVATED = BLOCKS.register("shrine_of_sacrifice_activated",
            () -> new ShrineOfSacrificeActivatedBlock(-0x878787, PropertiesInit.SHRINE));

    public static final RegistryObject<Block> SHRINE_OF_SPACE_UNACTIVATED = BLOCKS.register("shrine_of_space_unactivated",
            () -> new ShrineOfSpaceUnactivatedBlock(-0x878787, PropertiesInit.SHRINE));
    public static final RegistryObject<Block> SHRINE_OF_SPACE_ACTIVATED = BLOCKS.register("shrine_of_space_activated",
            () -> new ShrineOfSpaceActivatedBlock(-0x878787, PropertiesInit.SHRINE));

    public static final RegistryObject<Block> SHRINE_OF_PAIN_UNACTIVATED = BLOCKS.register("shrine_of_pain_unactivated",
            () -> new ShrineOfPainUnactivatedBlock(-0x878787, PropertiesInit.SHRINE));
    public static final RegistryObject<Block> SHRINE_OF_PAIN_ACTIVATED = BLOCKS.register("shrine_of_pain_activated",
            () -> new ShrineOfPainActivatedBlock(-0x878787, PropertiesInit.SHRINE));

    public static final RegistryObject<Block> FROZEN_EFFECT = BLOCKS.register("frozen_effect",
            () -> new FrozenEffectBlock(-0x07A8F4, PropertiesInit.FROZENBLOCK));


}

