package com.ladestitute.bloodandgold.registries;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.items.armor.GlassArmorItem;
import com.ladestitute.bloodandgold.items.armor.KarateGiItem;
import com.ladestitute.bloodandgold.items.armor.ObsidianArmorItem;
import com.ladestitute.bloodandgold.items.charm.FrostCharmItem;
import com.ladestitute.bloodandgold.items.charm.ProtectionCharmItem;
import com.ladestitute.bloodandgold.items.charm.RiskCharmItem;
import com.ladestitute.bloodandgold.items.charm.StrengthCharmItem;
import com.ladestitute.bloodandgold.items.packs.BackpackItem;
import com.ladestitute.bloodandgold.items.packs.HolsterItem;
import com.ladestitute.bloodandgold.items.rings.*;
import com.ladestitute.bloodandgold.items.shovels.*;
import com.ladestitute.bloodandgold.items.spells.*;
import com.ladestitute.bloodandgold.items.torches.GlassTorchItem;
import com.ladestitute.bloodandgold.items.torches.InfernalTorchItem;
import com.ladestitute.bloodandgold.items.torches.TorchOfStrengthItem;
import com.ladestitute.bloodandgold.items.weapons.DaggerItem;
import com.ladestitute.bloodandgold.items.weapons.glass.*;
import com.ladestitute.bloodandgold.items.weapons.gold.*;
import com.ladestitute.bloodandgold.items.weapons.obsidian.*;
import com.ladestitute.bloodandgold.items.weapons.special.DaggerOfFrostItem;
import com.ladestitute.bloodandgold.items.weapons.special.JeweledDaggerItem;
import com.ladestitute.bloodandgold.items.weapons.titanium.*;
import com.ladestitute.bloodandgold.items.weapons.blood.*;
import com.ladestitute.bloodandgold.util.BaGArmorTypes;
import com.ladestitute.bloodandgold.util.BaGMaterials;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ItemInit {

        //Registry initialization
        public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
                BaGMain.MOD_ID);

        //
        public static final RegistryObject<SwordItem> THROWN_GOLD_DAGGER = ITEMS.register("thrown_gold_dagger",
                () -> new DaggerItem(ItemTier.STONE, 2, -2.4f, new Item.Properties()));
    public static final RegistryObject<SwordItem> THROWN_BLOOD_WEAPON = ITEMS.register("thrown_blood_weapon",
            () -> new DaggerItem(ItemTier.STONE, 2, -2.4f, new Item.Properties()));
    public static final RegistryObject<SwordItem> THROWN_GLASS_WEAPON = ITEMS.register("thrown_glass_weapon",
            () -> new DaggerItem(ItemTier.STONE, 2, -2.4f, new Item.Properties()));
    public static final RegistryObject<SwordItem> THROWN_OBSIDIAN_WEAPON = ITEMS.register("thrown_obsidian_weapon",
            () -> new DaggerItem(ItemTier.STONE, 2, -2.4f, new Item.Properties()));
    public static final RegistryObject<SwordItem> THROWN_JEWELED_WEAPON = ITEMS.register("thrown_jeweled_weapon",
            () -> new DaggerItem(ItemTier.STONE, 2, -2.4f, new Item.Properties()));
    public static final RegistryObject<SwordItem> THROWN_FROST_WEAPON = ITEMS.register("thrown_frost_weapon",
            () -> new DaggerItem(ItemTier.STONE, 2, -2.4f, new Item.Properties()));

    public static final RegistryObject<ShovelItem> BLOOD_SHOVEL = ITEMS.register("blood_shovel",
            () -> new BloodShovelItem(BaGMaterials.BLOOD, 3, -2.4f, new Item.Properties().tab(BaGMain.SHOVELS_TAB)));
    public static final RegistryObject<CrystalShovelItem> CRYSTAL_SHOVEL = ITEMS.register("crystal_shovel",
            () -> new CrystalShovelItem(BaGMaterials.CRYSTAL, 2, -2.4f, new Item.Properties().tab(BaGMain.SHOVELS_TAB)));
    public static final RegistryObject<ShovelItem> GLASS_SHOVEL = ITEMS.register("glass_shovel",
            () -> new GlassShovelItem(BaGMaterials.GLASS, 2, -2.4f, new Item.Properties().tab(BaGMain.SHOVELS_TAB)));
    public static final RegistryObject<ObsidianShovelItem> OBSIDIAN_SHOVEL = ITEMS.register("obsidian_shovel",
            () -> new ObsidianShovelItem(BaGMaterials.OBSIDIAN, 3, -2.4f,
                    new Item.Properties().tab(BaGMain.SHOVELS_TAB)));
    public static final RegistryObject<TitaniumShovelItem> TITANIUM_SHOVEL = ITEMS.register("titanium_shovel",
            () -> new TitaniumShovelItem(BaGMaterials.TITANIUM, 0, -2.4f, new Item.Properties().tab(BaGMain.SHOVELS_TAB)));

        //Blood
        public static final RegistryObject<SwordItem> BLOOD_DAGGER = ITEMS.register("blood_dagger",
                () -> new BloodDaggerItem(BaGMaterials.BLOOD, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> BLOOD_SWORD = ITEMS.register("blood_sword",
            () -> new BloodSwordItem(BaGMaterials.BLOOD, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> BLOOD_RAPIER = ITEMS.register("blood_rapier",
            () -> new BloodRapierItem(BaGMaterials.BLOOD, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> BLOOD_SPEAR = ITEMS.register("blood_spear",
            () -> new BloodSpearItem(BaGMaterials.BLOOD, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> BLOOD_FLAIL = ITEMS.register("blood_flail",
            () -> new BloodFlailItem(BaGMaterials.BLOOD, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<BowItem> BLOOD_BOW = ITEMS.register("blood_bow",
            () -> new BloodBowItem(new Item.Properties().stacksTo(1).durability(190).tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<CrossbowItem> BLOOD_CROSSBOW = ITEMS.register("blood_crossbow",
            () -> new BloodCrossbowItem(new Item.Properties().stacksTo(1).durability(190).tab(BaGMain.WEAPONS_TAB)));

        //Glass
        public static final RegistryObject<SwordItem> GLASS_DAGGER = ITEMS.register("glass_dagger",
            () -> new GlassDaggerItem(BaGMaterials.GLASS, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
        public static final RegistryObject<SwordItem> GLASS_SWORD = ITEMS.register("glass_sword",
            () -> new GlassSwordItem(BaGMaterials.GLASS, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
        public static final RegistryObject<SwordItem> GLASS_RAPIER = ITEMS.register("glass_rapier",
            () -> new GlassRapierItem(BaGMaterials.GLASS, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
        public static final RegistryObject<SwordItem> GLASS_SPEAR = ITEMS.register("glass_spear",
            () -> new GlassSpearItem(BaGMaterials.GLASS, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
        public static final RegistryObject<SwordItem> GLASS_FLAIL = ITEMS.register("glass_flail",
            () -> new GlassFlailItem(BaGMaterials.GLASS, (int) 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
        public static final RegistryObject<BowItem> GLASS_BOW = ITEMS.register("glass_bow",
            () -> new GlassBowItem(new Item.Properties().stacksTo(1).durability(16).tab(BaGMain.WEAPONS_TAB)));
        public static final RegistryObject<CrossbowItem> GLASS_CROSSBOW = ITEMS.register("glass_crossbow",
            () -> new GlassCrossbowItem(new Item.Properties().stacksTo(1).durability(16).tab(BaGMain.WEAPONS_TAB)));

        //Gold
    public static final RegistryObject<SwordItem> GOLD_DAGGER = ITEMS.register("gold_dagger",
            () -> new GoldDaggerItem(ItemTier.GOLD, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> GOLD_RAPIER = ITEMS.register("gold_rapier",
            () -> new GoldRapierItem(ItemTier.GOLD, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> GOLD_SPEAR = ITEMS.register("gold_spear",
            () -> new GoldSpearItem(ItemTier.GOLD,  2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> GOLD_FLAIL = ITEMS.register("gold_flail",
            () -> new GoldFlailItem(ItemTier.GOLD, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<BowItem> GOLD_BOW = ITEMS.register("gold_bow",
            () -> new GoldBowItem(new Item.Properties().stacksTo(1).durability(32).tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<CrossbowItem> GOLD_CROSSBOW = ITEMS.register("gold_crossbow",
            () -> new GoldCrossbowItem(new Item.Properties().stacksTo(1).durability(32).tab(BaGMain.WEAPONS_TAB)));

        //Obsidian
        public static final RegistryObject<SwordItem> OBSIDIAN_DAGGER = ITEMS.register("obsidian_dagger",
                () -> new ObsidianDaggerItem(BaGMaterials.OBSIDIAN, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> OBSIDIAN_SWORD = ITEMS.register("obsidian_sword",
            () -> new ObsidianSwordItem(BaGMaterials.OBSIDIAN, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> OBSIDIAN_RAPIER = ITEMS.register("obsidian_rapier",
            () -> new ObsidianRapierItem(BaGMaterials.OBSIDIAN, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> OBSIDIAN_SPEAR = ITEMS.register("obsidian_spear",
            () -> new ObsidianSpearItem(BaGMaterials.OBSIDIAN, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> OBSIDIAN_FLAIL = ITEMS.register("obsidian_flail",
            () -> new ObsidianFlailItem(BaGMaterials.OBSIDIAN, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<BowItem> OBSIDIAN_BOW = ITEMS.register("obsidian_bow",
            () -> new ObsidianBowItem(new Item.Properties().stacksTo(1).durability(1796).tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<CrossbowItem> OBSIDIAN_CROSSBOW = ITEMS.register("obsidian_crossbow",
            () -> new ObsidianCrossbowItem(new Item.Properties().stacksTo(1).durability(1796).tab(BaGMain.WEAPONS_TAB)));


    //Titanium
        public static final RegistryObject<SwordItem> TITANIUM_DAGGER = ITEMS.register("titanium_dagger",
                () -> new TitaniumDaggerItem(BaGMaterials.TITANIUM, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> TITANIUM_SWORD = ITEMS.register("titanium_sword",
            () -> new TitaniumSwordItem(BaGMaterials.TITANIUM, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> TITANIUM_RAPIER = ITEMS.register("titanium_rapier",
            () -> new TitaniumRapierItem(BaGMaterials.TITANIUM, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> TITANIUM_SPEAR = ITEMS.register("titanium_spear",
            () -> new TitaniumSpearItem(BaGMaterials.TITANIUM,  2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> TITANIUM_FLAIL = ITEMS.register("titanium_flail",
            () -> new TitaniumFlailItem(BaGMaterials.TITANIUM, 2, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<BowItem> TITANIUM_BOW = ITEMS.register("titanium_bow",
            () -> new TitaniumBowItem(new Item.Properties().stacksTo(1).durability(515).tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<CrossbowItem> TITANIUM_CROSSBOW = ITEMS.register("titanium_crossbow",
            () -> new TitaniumCrossbowItem(new Item.Properties().stacksTo(1).durability(515).tab(BaGMain.WEAPONS_TAB)));

    public static final RegistryObject<ArmorItem> GLASS_ARMOR = ITEMS.register("glass_armor",
            () -> new GlassArmorItem(BaGArmorTypes.GLASS, EquipmentSlotType.CHEST,
                    new Item.Properties().tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<ArmorItem> OBSIDIAN_ARMOR = ITEMS.register("obsidian_armor",
            () -> new ObsidianArmorItem(BaGArmorTypes.OBSIDIAN, EquipmentSlotType.CHEST,
                    new Item.Properties().tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<ArmorItem> KARATE_GI = ITEMS.register("karate_gi",
            () -> new KarateGiItem(BaGArmorTypes.GI, EquipmentSlotType.CHEST,
                    new Item.Properties().tab(BaGMain.ARMOR_TAB)));

    public static final RegistryObject<SwordItem> DAGGER = ITEMS.register("dagger",
            () -> new ObsidianDaggerItem(BaGMaterials.DAGGER, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> JEWELED_DAGGER = ITEMS.register("jeweled_dagger",
            () -> new JeweledDaggerItem(BaGMaterials.JEWELED, 3, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));
    public static final RegistryObject<SwordItem> DAGGER_OF_FROST = ITEMS.register("dagger_of_frost",
            () -> new DaggerOfFrostItem(BaGMaterials.FROST, 0, -2.4f, new Item.Properties().tab(BaGMain.WEAPONS_TAB)));

    public static final RegistryObject<Item> SHRINE_OF_BLOOD_UNACTIVATED = ITEMS.register("shrine_of_blood_unactivated",
            () -> new BlockItem(SpecialBlocksInit.SHRINE_OF_BLOOD_UNACTIVATED.get(), new Item.Properties().tab(BaGMain.SHRINES_TAB)));
    public static final RegistryObject<Item> SHRINE_OF_DARKNESS_UNACTIVATED = ITEMS.register("shrine_of_darkness_unactivated",
            () -> new BlockItem(SpecialBlocksInit.SHRINE_OF_DARKNESS_UNACTIVATED.get(), new Item.Properties().tab(BaGMain.SHRINES_TAB)));
    public static final RegistryObject<Item> SHRINE_OF_PEACE_UNACTIVATED = ITEMS.register("shrine_of_peace_unactivated",
            () -> new BlockItem(SpecialBlocksInit.SHRINE_OF_PEACE_UNACTIVATED.get(), new Item.Properties().tab(BaGMain.SHRINES_TAB)));
    public static final RegistryObject<Item> SHRINE_OF_RHYTHM_UNACTIVATED = ITEMS.register("shrine_of_rhythm_unactivated",
            () -> new BlockItem(SpecialBlocksInit.SHRINE_OF_RHYTHM_UNACTIVATED.get(), new Item.Properties().tab(BaGMain.SHRINES_TAB)));
    public static final RegistryObject<Item> SHRINE_OF_SACRIFICE_UNACTIVATED = ITEMS.register("shrine_of_sacrifice_unactivated",
            () -> new BlockItem(SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get(), new Item.Properties().tab(BaGMain.SHRINES_TAB)));
    public static final RegistryObject<Item> SHRINE_OF_SPACE_UNACTIVATED = ITEMS.register("shrine_of_space_unactivated",
            () -> new BlockItem(SpecialBlocksInit.SHRINE_OF_SPACE_UNACTIVATED.get(), new Item.Properties().tab(BaGMain.SHRINES_TAB)));
    public static final RegistryObject<Item> SHRINE_OF_PAIN_UNACTIVATED = ITEMS.register("shrine_of_pain_unactivated",
            () -> new BlockItem(SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get(), new Item.Properties().tab(BaGMain.SHRINES_TAB)));
    public static final RegistryObject<Item> SHRINE_OF_GLASS_UNACTIVATED = ITEMS.register("shrine_of_glass_unactivated",
            () -> new BlockItem(SpecialBlocksInit.SHRINE_OF_GLASS_UNACTIVATED.get(), new Item.Properties().tab(BaGMain.SHRINES_TAB)));

    public static final RegistryObject<Item> MAUSOLEUM_MASH_DISC = ITEMS.register("mausoleum_mash_disc", () -> new MusicDiscItem(1,
            SoundInit.MAUSOLEUM_MASH_DISC_LAZY.get(), new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB).rarity(Rarity.RARE)));
    public static final RegistryObject<Item> PORTABELLOHEAD_DISC = ITEMS.register("portabellohead_disc", () -> new MusicDiscItem(1,
            SoundInit.PORTABELLOHEAD_DISC_LAZY.get(), new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB).rarity(Rarity.RARE)));
    public static final RegistryObject<Item> DANCE_OF_THE_DECOROUS_DISC = ITEMS.register("dance_of_the_decorous_disc", () -> new MusicDiscItem(1,
            SoundInit.DANCE_OF_THE_DECOROUS_DISC_LAZY.get(), new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB).rarity(Rarity.RARE)));
    public static final RegistryObject<Item> A_HOT_MESS_DISC = ITEMS.register("a_hot_mess_disc", () -> new MusicDiscItem(1,
            SoundInit.A_HOT_MESS_DISC_LAZY.get(), new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB).rarity(Rarity.RARE)));
    public static final RegistryObject<Item> STYX_AND_STONES_DISC = ITEMS.register("styx_and_stones_disc", () -> new MusicDiscItem(1,
            SoundInit.STYX_AND_STONES_DISC_LAZY.get(), new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB).rarity(Rarity.RARE)));
    public static final RegistryObject<Item> POWER_CORDS_DISC = ITEMS.register("power_cords_disc", () -> new MusicDiscItem(1,
            SoundInit.POWER_CORDS_DISC_LAZY.get(), new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB).rarity(Rarity.RARE)));
    public static final RegistryObject<SwordItem> GLASS_SHARD = ITEMS.register("glass_shard",
            () -> new GlassShardItem(BaGMaterials.GLASS_SHARD, 0, -2.4f, new Item.Properties().tab(BaGMain.MISC_TAB)));

    //Rings
    public static final RegistryObject<Item> RING_OF_COURAGE = ITEMS.register("ring_of_courage",
            () -> new RingOfCourageItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> RING_OF_GOLD = ITEMS.register("ring_of_gold",
            () -> new RingOfGoldItem(new Item.Properties().stacksTo(1).tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<Item> RING_OF_MIGHT = ITEMS.register("ring_of_might",
            () -> new RingOfMightItem(new Item.Properties().stacksTo(1).tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<Item> RING_OF_PEACE = ITEMS.register("ring_of_peace",
            () -> new RingOfPeaceItem(new Item.Properties().stacksTo(1).tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<Item> RING_OF_PROTECTION = ITEMS.register("ring_of_protection",
            () -> new RingOfProtectionItem(new Item.Properties().stacksTo(1).tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<Item> RING_OF_REGENERATION = ITEMS.register("ring_of_regeneration",
            () -> new RingOfRegenerationItem(new Item.Properties().stacksTo(1).tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<Item> RING_OF_SHIELDING = ITEMS.register("ring_of_shielding",
            () -> new RingOfShieldingItem(new Item.Properties().stacksTo(1).tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<Item> RING_OF_WAR = ITEMS.register("ring_of_war",
            () -> new RingOfWarItem(new Item.Properties().stacksTo(1).tab(BaGMain.ARMOR_TAB)));
    public static final RegistryObject<Item> RING_OF_MANA = ITEMS.register("ring_of_mana",
            () -> new RingOfManaItem(new Item.Properties().stacksTo(1).tab(BaGMain.ARMOR_TAB)));

    //Bags
    public static final RegistryObject<Item> HOLSTER = ITEMS.register("holster",
            () -> new HolsterItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BACKPACK = ITEMS.register("backpack",
            () -> new BackpackItem(new Item.Properties().stacksTo(1)));

    //Torches
    public static final RegistryObject<Item> TORCH_OF_STRENGTH = ITEMS.register("torch_of_strength",
            () -> new TorchOfStrengthItem(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> GLASS_TORCH = ITEMS.register("glass_torch",
            () -> new GlassTorchItem(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> INFERNAL_TORCH = ITEMS.register("infernal_torch",
            () -> new InfernalTorchItem(new Item.Properties().stacksTo(64)));

    //Charms
    public static final RegistryObject<Item> PROTECTION_CHARM = ITEMS.register("protection_charm",
            () -> new ProtectionCharmItem(new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB)));
    public static final RegistryObject<Item> STRENGTH_CHARM = ITEMS.register("strength_charm",
            () -> new StrengthCharmItem(new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB)));
    public static final RegistryObject<Item> RISK_CHARM = ITEMS.register("risk_charm",
            () -> new RiskCharmItem(new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB)));
    public static final RegistryObject<Item> FROST_CHARM = ITEMS.register("frost_charm",
            () -> new FrostCharmItem(new Item.Properties().stacksTo(1).tab(BaGMain.MISC_TAB)));

    //Spells
    public static final RegistryObject<Item> FIREBALL_SPELL = ITEMS.register("fireball_spell",
            () -> new FireballSpellItem(new Item.Properties().stacksTo(1).tab(BaGMain.SPELLS_TAB)));
    public static final RegistryObject<Item> HEAL_SPELL = ITEMS.register("heal_spell",
            () -> new HealSpellItem(new Item.Properties().stacksTo(1).tab(BaGMain.SPELLS_TAB)));
    public static final RegistryObject<Item> SHIELD_SPELL = ITEMS.register("shield_spell",
            () -> new ShieldSpellItem(new Item.Properties().stacksTo(1).tab(BaGMain.SPELLS_TAB)));
    public static final RegistryObject<Item> FREEZE_SPELL = ITEMS.register("freeze_spell",
            () -> new FreezeSpellItem(new Item.Properties().stacksTo(1).tab(BaGMain.SPELLS_TAB)));
    public static final RegistryObject<Item> TRANSMUTE_SPELL = ITEMS.register("transmute_spell",
            () -> new TransmuteSpellItem(new Item.Properties().stacksTo(1).tab(BaGMain.SPELLS_TAB)));
    public static final RegistryObject<Item> BOMB_SPELL = ITEMS.register("bomb_spell",
            () -> new BombSpellItem(new Item.Properties().stacksTo(1)));

    //Spell entities
    public static final RegistryObject<Item> FIREBALL = ITEMS.register("fireball",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BOMB = ITEMS.register("bomb",
            () -> new Item(new Item.Properties().stacksTo(64)));

}
