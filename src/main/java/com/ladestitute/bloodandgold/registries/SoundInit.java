package com.ladestitute.bloodandgold.registries;

import com.ladestitute.bloodandgold.BaGMain;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class SoundInit {
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, BaGMain.MOD_ID);

    public static final Lazy<SoundEvent> MAUSOLEUM_MASH_DISC_LAZY = Lazy
            .of(() -> new SoundEvent(new ResourceLocation(BaGMain.MOD_ID, "item.mausoleum_mash_disc")));
    public static final RegistryObject<SoundEvent> MAUSOLEUM_MASH_DISC = SOUNDS.register("item.mausoleum_mash_disc.disc",
            MAUSOLEUM_MASH_DISC_LAZY);

    public static final Lazy<SoundEvent> PORTABELLOHEAD_DISC_LAZY = Lazy
            .of(() -> new SoundEvent(new ResourceLocation(BaGMain.MOD_ID, "item.portabellohead_disc")));
    public static final RegistryObject<SoundEvent> PORTABELLOHEAD_DISC = SOUNDS.register("item.portabellohead_disc.disc",
            PORTABELLOHEAD_DISC_LAZY);

    public static final Lazy<SoundEvent> DANCE_OF_THE_DECOROUS_DISC_LAZY = Lazy
            .of(() -> new SoundEvent(new ResourceLocation(BaGMain.MOD_ID, "item.dance_of_the_decorous_disc")));
    public static final RegistryObject<SoundEvent> DANCE_OF_THE_DECOROUS_DISC = SOUNDS.register("item.dance_of_the_decorous_disc.disc",
            DANCE_OF_THE_DECOROUS_DISC_LAZY);

    public static final Lazy<SoundEvent> A_HOT_MESS_DISC_LAZY = Lazy
            .of(() -> new SoundEvent(new ResourceLocation(BaGMain.MOD_ID, "item.a_hot_mess_disc")));
    public static final RegistryObject<SoundEvent> A_HOT_MESS_DISC = SOUNDS.register("item.a_hot_mess_disc.disc",
            A_HOT_MESS_DISC_LAZY);

    public static final Lazy<SoundEvent> STYX_AND_STONES_DISC_LAZY = Lazy
            .of(() -> new SoundEvent(new ResourceLocation(BaGMain.MOD_ID, "item.styx_and_stones_disc")));
    public static final RegistryObject<SoundEvent> STYX_AND_STONES_DISC = SOUNDS.register("item.styx_and_stones_disc.disc",
            STYX_AND_STONES_DISC_LAZY);

    public static final Lazy<SoundEvent> POWER_CORDS_DISC_LAZY = Lazy
            .of(() -> new SoundEvent(new ResourceLocation(BaGMain.MOD_ID, "item.power_cords_disc")));
    public static final RegistryObject<SoundEvent> POWER_CORDS_DISC = SOUNDS.register("item.power_cords_disc.disc",
            POWER_CORDS_DISC_LAZY);


}
