package com.ladestitute.bloodandgold.registries;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;

public class PropertiesInit {
    public static final AbstractBlock.Properties SHRINE = AbstractBlock.Properties.of(Material.STONE)
            .strength(100f, 0f)
            .sound(SoundType.STONE)
            .harvestLevel(3)
            .harvestTool(ToolType.PICKAXE)
            .requiresCorrectToolForDrops();

    public static final AbstractBlock.Properties SHRINE_STONE = AbstractBlock.Properties.of(Material.STONE)
            .strength(1.5f, 6f)
            .sound(SoundType.STONE)
            .harvestLevel(0)
            .harvestTool(ToolType.PICKAXE)
            .requiresCorrectToolForDrops();

    public static final AbstractBlock.Properties FROZENBLOCK = AbstractBlock.Properties.of(Material.ICE)
            .strength(0f, 0f)
            .sound(SoundType.GLASS)
            .harvestLevel(0)
            .harvestTool(ToolType.PICKAXE)
            .air()
            .noCollission()
            .requiresCorrectToolForDrops();

}
