package com.ladestitute.bloodandgold.registries;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.container.BackpackContainer;
import com.ladestitute.bloodandgold.container.BagContainer;
import com.ladestitute.bloodandgold.container.HolsterContainer;
import net.minecraft.inventory.container.ContainerType;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ContainerInit {
    public static final DeferredRegister<ContainerType<?>> CONTAINERS = DeferredRegister.create(ForgeRegistries.CONTAINERS, BaGMain.MOD_ID);

    public static final RegistryObject<ContainerType<BagContainer>> BAG = CONTAINERS.register("bag",
            () -> IForgeContainerType.create(BagContainer::new));
    public static final RegistryObject<ContainerType<HolsterContainer>> HOLSTER = CONTAINERS.register("holster",
            () -> IForgeContainerType.create(HolsterContainer::new));
    public static final RegistryObject<ContainerType<BackpackContainer>> BACKPACK = CONTAINERS.register("backpack",
            () -> IForgeContainerType.create(BackpackContainer::new));
}
