package com.ladestitute.bloodandgold.registries;

import com.ladestitute.bloodandgold.BaGMain;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.EffectType;
import net.minecraft.potion.Potion;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class EffectInit {
    public static final DeferredRegister<Effect> EFFECTS = DeferredRegister.create(ForgeRegistries.POTIONS,
            BaGMain.MOD_ID);

    //Arrhythmia
    public static final RegistryObject<Effect> ARRHYTHMIA_EFFECT =
            EFFECTS.register("arrhythmia", () -> new ArrhythmiaEffect(EffectType.NEUTRAL, 0xBE2205));

    //Unused, for a planned 1/10 'critical brew' chance which doubles the duration
    //public static final RegistryObject<Potion> COLD_RESIST_POTION_DOUBLE = POTIONS.register("cold_resist_double", () ->
    //new Potion(new EffectInstance(COLD_RESIST_EFFECT.get(), 18000)));
    //public static final RegistryObject<Potion> HEAT_RESIST_POTION_DOUBLE = POTIONS.register("heat_resist_double", () ->
    //new Potion(new EffectInstance(HEAT_RESIST_EFFECT.get(), 18000)));

    //
    public static class ArrhythmiaEffect extends Effect {

        public ArrhythmiaEffect(EffectType typeIn, int liquidColorIn) {
            super(typeIn, liquidColorIn);
        }

    }

}
