package com.ladestitute.bloodandgold.client;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.client.render.BombRenderer;
import com.ladestitute.bloodandgold.client.screen.BackpackScreen;
import com.ladestitute.bloodandgold.client.screen.HolsterScreen;
import com.ladestitute.bloodandgold.entities.EntityBomb;
import com.ladestitute.bloodandgold.registries.ContainerInit;
import com.ladestitute.bloodandgold.registries.EntityTypeInit;
import com.ladestitute.bloodandgold.registries.SpecialBlocksInit;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.entity.SpriteRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

import java.util.function.Supplier;

@Mod.EventBusSubscriber(modid = BaGMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticClientSetup(FMLClientSetupEvent event) {
        event.setPhase(EventPriority.HIGH);
        //MinecraftForge.EVENT_BUS.register(new ZStatsGUIHandler());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_BLOOD_UNACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_DARKNESS_UNACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_PEACE_UNACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_PAIN_UNACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_GLASS_UNACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_SACRIFICE_UNACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_SPACE_UNACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_RHYTHM_UNACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_BLOOD_ACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_DARKNESS_ACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_PEACE_ACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_PAIN_ACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_GLASS_ACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_SACRIFICE_ACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_SPACE_ACTIVATED.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.SHRINE_OF_RHYTHM_ACTIVATED.get(), RenderType.translucent());
        ScreenManager.register(ContainerInit.HOLSTER.get(), HolsterScreen::new);
        ScreenManager.register(ContainerInit.BACKPACK.get(), BackpackScreen::new);

        ItemRenderer renderer = Minecraft.getInstance().getItemRenderer();

        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_GOLD_SPEAR.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_BLOOD_DAGGER.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_BLOOD_SPEAR.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_TITANIUM_DAGGER.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_TITANIUM_SPEAR.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_GLASS_DAGGER.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_GLASS_SPEAR.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_OBSIDIAN_DAGGER.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_OBSIDIAN_SPEAR.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_JEWELED_DAGGER.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_FROST_DAGGER.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_DAGGER.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.THROWN_GLASS_SHARD.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.FIREBALL.get(),
                (renderManager) -> new SpriteRenderer<>(renderManager, renderer));
        registerEntityModels(event.getMinecraftSupplier());
    }

    public static void init(final FMLClientSetupEvent event) {
        BaGKeybinds.register();
    }

    private static void registerEntityModels(Supplier<Minecraft> minecraft) {
        //Just a variable I have set incase I want to add more entites, which will make the code more efficient
        ItemRenderer renderer = minecraft.get().getItemRenderer();

        /*
         * We now need to render the entity on the client using the Rendering Registry
         * We take in the Entity then the RenderType. I use a lambda function here for ease.
         * Most projectiles will use SpriteRenderers for their rendering, using the same texture as the item
         * It taKes in the manager from the lambda and the variable above for the item renderer.
         */
        RenderingRegistry.registerEntityRenderingHandler(EntityTypeInit.BOMB.get(), (renderManager) -> new BombRenderer<EntityBomb>(renderManager, renderer));
    }

}
