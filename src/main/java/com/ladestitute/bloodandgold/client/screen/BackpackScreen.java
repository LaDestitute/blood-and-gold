package com.ladestitute.bloodandgold.client.screen;

import com.ladestitute.bloodandgold.BaGMain;
import com.ladestitute.bloodandgold.container.BackpackContainer;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class BackpackScreen extends SimpleContainerScreen<BackpackContainer>
{
    public BackpackScreen(BackpackContainer container, PlayerInventory playerInventory, ITextComponent title)
    {
        super(container, playerInventory, title);
    }

    @Override
    protected ResourceLocation getBackgroundTexture()
    {
        return new ResourceLocation(BaGMain.MOD_ID, "textures/gui/single_slot.png");
    }
}

