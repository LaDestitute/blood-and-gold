package com.ladestitute.bloodandgold.client;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import org.lwjgl.glfw.GLFW;

public class BaGKeybinds {
    public static KeyBinding throwweapon;
    public static KeyBinding castheldspell;

    public static void register()
    {
        throwweapon = new KeyBinding("throwweapon", GLFW.GLFW_KEY_R, "key.categories.bloodandgold");
        castheldspell = new KeyBinding("castheldspell", GLFW.GLFW_KEY_Y, "key.categories.bloodandgold");

        ClientRegistry.registerKeyBinding(throwweapon);
        ClientRegistry.registerKeyBinding(castheldspell);
    }

}
